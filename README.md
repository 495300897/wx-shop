# wx-shop

#### 背景介绍
{**微信小程序简易商城**
2018年，基于微信客户端搭建的一个 **运动服专卖** 小程序，没做任何推广，纯粹个人爱好。
目前开源前后端，无论是个人、团队、或是企业，均可学习使用或商用。项目请看 [https://gitee.com/495300897/wx-shop.git](https://gitee.com/495300897/wx-shop.git)}

#### 软件架构
JAVA，MAVEN，SPRING，SPRING MVC，MYSQL，REDIS


#### 软件版本

1.  MAVEN 3.6
2.  JDK 1.8
3.  MYSQL 5.6
4.  REDIS 3.0

#### 使用说明

- wise-net-web   JAVA管理后端
![输入图片说明](wise-net-web/src/main/webapp/images/wx/wise-net-web.png)

1.  修改配置文件：wise-net-web/src/main/resources/jdbc.properties
2.  创建数据库sport_wear，导入wise-net-web/src/main/resources/sport_wear.sql目录下的 **sport_wear.sql** 
3.  访问web页面：http://localhost:port/wisenet
4.  登录账号: admin admin
5.  配置小程序账号信息：公众号管理菜单 > 微信账号列表，配置完成，重启服务端

- sport_wear     微信小程序前端

![首页](sports-wear/images/image.png)

![详情](sports-wear/info.png)

![输入图片说明](sports-wear/images/addr.png)

![用户](sports-wear/images/user.png)

1.  修改配置文件sports-wear/project.config.json中的 **appid** 
2.  修改配置文件sports-wear/utils/util.js中的 **host** 为后端部署路径
3.  修改配置文件sports-wear/pages/index/index.js中的 **appid** 

#### 联系方式

1. WX：fzhihui
2. QQ：495300897

( **备注：代码开源几年以来，承蒙各位的下载与部署，但由于太多人加V，可能忙不过来；特别是很多没有编程基础的朋友，如果急需使用，可V我100，保证把项目用到的各种技术毫无保留地教会大家，谢谢！** )
