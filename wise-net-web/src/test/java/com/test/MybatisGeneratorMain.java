package com.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.internal.DefaultShellCallback;

public class MybatisGeneratorMain {
	
	public static void main(String[] args) {
		Configuration config = null;
		boolean overwrite = true;
		String genCfg = "/mybatis-generator.xml";
		List<String> warnings = new ArrayList<String>();
		File configFile = new File(MybatisGeneratorMain.class.getResource(genCfg).getFile());
		ConfigurationParser cp = new ConfigurationParser(warnings);
		try {
			config = cp.parseConfiguration(configFile);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("读取配置文件出错...");
		}
		MyBatisGenerator myBatisGenerator = null;
		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		long beginTs = System.currentTimeMillis();
		try {
			System.out.println("生成文件中...");
			myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
			System.out.println("生成文件出错...");
		}
		try {
			myBatisGenerator.generate(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("生成文件完成，请刷新项目工程...");
		long endTs = System.currentTimeMillis();
		System.out.println("耗时(毫秒)：" + (endTs - beginTs));
	}
}