package com.test;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wisenet.cache.RedisCacheProvider;
import com.wisenet.dao.GeneratorDAO;
import com.wisenet.model.WxPayResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class JunitTest {
	@Autowired
	private GeneratorDAO genDAO;
	@Autowired
	private RedisCacheProvider redis;
	
	/**
	 * 生成代码测试
	 * @throws Exception
	 */
	@Test
	public void genCodeTest() {
		// generator.properties 修改配置文件
		String fileDir = "D://code/"; // 生成代码目录
		String[] tableNames = new String[] { "text_info", "user" };
		// 生成代码文件
		generatorCode(tableNames, fileDir);
	}
	
	/**
	 * defaultRollback = true 事务回滚
	 * @throws Exception
	 */
	@Test
	public void dbTest() throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("openid", "fzh");
		map.put("total_fee", "20");
		WxPayResult result = JSONObject.parseObject(JSON.toJSON(map).toString(), WxPayResult.class);
		System.out.println(result.getOpenid());
		System.out.println(result.getTotalFee());
		/*System.out.println("测试中文");
		User user = new User();
		user.setUsername("冯志辉");
		user.setPassword("123456");
		userService.saveUser(user);
		System.out.println("user: " + userMapper.findByUserName("冯志辉"));*/
//		System.out.println("password: " + userService.findPwd("冯志辉"));
	}
	
	@Test
	public void redisTest() throws Exception {
		System.out.println(redis.get("ACT_VISIT_1"));
	}
	
	/**
	 * 生成代码文件
	 * @param tableNames
	 * @param fileDir
	 */
	public void generatorCode(String[] tableNames, String fileDir) {
//		for(String tableName : tableNames) {
//			// 查询表信息
//			Map<String, Object> table = genDAO.getTable(tableName);
//			// 查询列信息
//			List<Map<String, Object>> columns = genDAO.listColumns(tableName);
//			// 生成代码
//			GenUtils.generatorCode(table, columns, fileDir);
//		}
	}
	
	/**
	 * 生成代码zip包
	 * @param tableNames
	 * @return
	 * @throws Exception
	 */
	public byte[] generatorCodeZip(String[] tableNames) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);
		for(String tableName : tableNames) {
			// 查询表信息
			Map<String, Object> table = genDAO.getTable(tableName);
			// 查询列信息
			List<Map<String, Object>> columns = genDAO.listColumns(tableName);
			// 生成代码
//			GenUtils.generatorCode(table, columns, zip);
		}
		IOUtils.closeQuietly(zip);
		return outputStream.toByteArray();
	}
	
	public static void main(String[] args) {
		System.out.println(RandomUtils.nextInt(0, 3));
	}
	
}
