package com.wisenet.wx.util;

import java.math.BigInteger;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

import com.wisenet.common.Const;
import com.wisenet.common.WxConst;
import com.wisenet.service.wxpubno.ApiService;
import com.wisenet.wx.api.GuessImage;
import com.wisenet.wx.api.GuessNumber;

public class StringUtil {

	/**
	 * 追加字符串(消息菜单)
	 * @return
	 */
	public static String strContent() {
		StringBuffer sb = new StringBuffer();

		sb.append("/::)亲，怎么现在才来，赶快让我为你服务吧~\n\n");
		return sb.toString();
	}

	/**
	 * 根据输入获取回复内容
	 * @param requestMap
	 * @return
	 */
	public static void replyContent(Map<String, String> requestMap) {
		String type = Const.MENU_TYPE_DEFAULT, respContent = null;
		
		// 获取用户输入内容
		String inputTxt = requestMap.get(WxConst.CONTENT).replaceAll("，", ",").trim();

		if (StringUtils.isEmpty(inputTxt)) return;

		// QQ表情
		if (QQFace.isQqFace(inputTxt)) {
			respContent = inputTxt;
		}
		// 获取猜数帮助
		else if (Const.GUESS_NUMBER_HELP.equals(inputTxt)) {
			respContent = GuessNumber.GAME_DESC;
		}
		// 获取天气预报
		else if ((inputTxt.startsWith(Const.WEATHER_INPUT) || inputTxt.endsWith(Const.WEATHER_INPUT))) {
			respContent = ApiService.getWeather(inputTxt);
		}
		// 获取猜图答案
		else if (inputTxt.startsWith(Const.GUESS_IMAGE_INPUT) || inputTxt.startsWith(Const.GUESS_IDIOM_INPUT)) {
			respContent = GuessImage.getAnswer(inputTxt);
		} else {
			// 关键词或记录功能消息
			type = Const.MENU_TYPE_RECORD;
		}
		
		requestMap.put(WxConst.TYPE, type);
		requestMap.put("respContent", respContent);
	}

	public static String getTitle() {
		if (getRandNum(3) / 2 == 0) {
			return "我的最新照片，帅哥要看看吗？";
		}
		return "嘻嘻，你控制不住自己点进来看！";
	}
	
	/**
	 * 获取随机数(返回0到num-1之间的数，不含自身)
	 * @param num
	 * @return
	 */
	public static int getRandNum(int num) {
		Random rand = new Random();
		return rand.nextInt(num);
	}
	
	/**
	 * 获取指定范围随机数，包含自身
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandNum(int min, int max) {
	    Random random = new Random();
	    return random.nextInt(max) % (max - min + 1) + min;
	}
	
	public static String getShortStr(String fromUserName) {
		BigInteger bigInteger = new BigInteger(Math.abs(fromUserName.hashCode()) + "");
		return bigInteger.toString(36).toUpperCase();
	}
	
	/**
	 * 计算采用utf-8编码方式时字符串所占字节数
	 * @param content
	 * @return
	 */
	public static int getByteSize(String content) {
		try {
			// 汉字采用utf-8编码时占3个字节
			return content != null ? content.getBytes("utf-8").length : 0;
		} catch (Exception ex) {
			return 0;
		}
	}
	
}
