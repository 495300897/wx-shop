package com.wisenet.wx.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QQFace {

	/**
	 * 判断是否是QQ表情
	 * @param content
	 * @return
	 */
	public static boolean isQqFace(String content) {
		// 判断QQ表情的正则表达式
		String qqfaceRegex = "/::\\)|/::~|/::B|/::\\||/:8-\\)|/::<|/::$|/::X|/::Z|/::'\\(|/::-\\||/::@|/::P|/::D|/::O|/::\\(|/::\\+|/:--b|/::Q|/::T|/:,@P|/:,@-D|/::d|/:,@o|/::g|/:\\|-\\)|/::!|/::L|/::>|/::,@|/:,@f|/::-S|/:\\?|/:,@x|/:,@@|/::8|/:,@!|/:!!!|/:xx|/:bye|/:wipe|/:dig|/:handclap|/:&-\\(|/:B-\\)|/:<@|/:@>|/::-O|/:>-\\||/:P-\\(|/::'\\||/:X-\\)|/::\\*|/:@x|/:8\\*|/:pd|/:<W>|/:beer|/:basketb|/:oo|/:coffee|/:eat|/:pig|/:rose|/:fade|/:showlove|/:heart|/:break|/:cake|/:li|/:bome|/:kn|/:footb|/:ladybug|/:shit|/:moon|/:sun|/:gift|/:hug|/:strong|/:weak|/:share|/:v|/:@\\)|/:jj|/:@@|/:bad|/:lvu|/:no|/:ok|/:love|/:<L>|/:jump|/:shake|/:<O>|/:circle|/:kotow|/:turn|/:skip|/:oY|/:#-0|/:hiphot|/:kiss|/:<&|/:&>";
		Pattern p = Pattern.compile(qqfaceRegex);
		Matcher m = p.matcher(content);
		if (m.matches()) {
			return true;
		}
		return false;
	}

	/**
	 * emoji表情转换(hex -> utf-16) QQFace.emoji(0x2600))
	 * @param hexEmoji
	 * @return
	 */
	public static String emoji(int hexEmoji) {
		return String.valueOf(Character.toChars(hexEmoji));
	}
	
	/**
	 * 过滤emoji表情
	 * @param source
	 * @return
	 */
	public static String filterEmoji(String source) {
		System.out.println("sourceStr = " + source);
		int len = source.length();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			sb.append(!notisEmojiCharacter(codePoint) ? "*" : source.charAt(i));
		}
		System.out.println("replaceStr = " + sb.toString());
		return sb.toString();
	}

	/**
	 * 非emoji表情字符判断
	 * @param codePoint
	 * @return
	 */
	public static boolean notisEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) ||
			   (codePoint == 0x9) || 
			   (codePoint == 0xA) || 
			   (codePoint == 0xD) || 
			   ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || 
			   ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || 
			   ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}

}
