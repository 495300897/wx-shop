package com.wisenet.controller.web.wx;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.annotation.LoginValidate;
import com.wisenet.common.Const;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.ImageTextInfo;
import com.wisenet.entity.User;
import com.wisenet.service.wx.ImageTextInfoService;
import com.wisenet.util.VeDate;

/**
 * 单图文操作Controller
 * @author fzh
 *
 */
@Controller
@RequestMapping("/imagetextinfo")
public class ImageTextInfoController extends PlatformController {
	@Autowired
	private ImageTextInfoService imageTextInfoService;
//	@Autowired
//	private ImageTextMoreService imageTextMoreService;

	/**
	 * 新增单图文页面
	 * @return
	 */
	@RequestMapping("/addUI")
	public ModelAndView addImageTextInfoUI() {
		ModelAndView mav = new ModelAndView(getBasePath() + "article_detail");
		mav.addObject("currDate", VeDate.getDateYMD(new Date()));
		return mav;
	}

	/**
	 * 修改单图文页面
	 * @param rid
	 * @return
	 */
	@RequestMapping("/updateUI/{imageTextNo}")
	public ModelAndView updateImageTextInfoUI(@PathVariable("imageTextNo") Long imageTextNo) {
		ModelAndView mav = new ModelAndView(getBasePath() + "article_detail_edit");
		mav.addObject("imageTextInfo", imageTextInfoService.findByPK(imageTextNo));
		return mav;
	}

	/**
	 * 保存或修改单图文
	 * @param request
	 * @param imageTextInfo
	 * @return
	 */
	@RequestMapping("/saveOrUpdate")
	public @ResponseBody boolean saveOrUpdateImageTextInfo(HttpServletRequest request, ImageTextInfo imageTextInfo) {
		String flag = request.getParameter("flag");
		if (flag == null || imageTextInfo == null)
			return false;
		if ("add".equals(flag)) {
			User user = getSessionUser(request);
			imageTextInfo.setCreateTime(new Date());
//			imageTextInfo.setOperator(user.getUsername());
			// 暂用分享次数
			imageTextInfo.setOperator(Const.MESSAGE_TYPE_TEXT);
			// 暂用点赞数量
			imageTextInfo.setClickUrl(Const.MESSAGE_TYPE_TEXT);
			imageTextInfo.setWeixinPublicNo(user.getWxpublicno());
			imageTextInfo.setImageTextType(Const.MESSAGE_TYPE_TEXT);
			int suc = imageTextInfoService.save(imageTextInfo);
			if (suc > 0)
				return true;
		}
		if ("update".equals(flag)) {
			int suc = imageTextInfoService.update(imageTextInfo);
			if (suc > 0)
				return true;
		}
		return false;
	}

	/**
	 * 删除图文
	 * @param imageTextNo
	 * @return
	 */
	@RequestMapping("/remove/{imageTextNo}")
	public @ResponseBody boolean removeImageText(@PathVariable("imageTextNo") Long imageTextNo) {
		JSONObject json = getJsonResult(imageTextNo);
		if (json.getBoolean(SUCCESS)) {
			ImageTextInfo imageTextInfo = imageTextInfoService.findByPK(imageTextNo);
			if (imageTextInfo != null) {
				imageTextInfoService.delete(imageTextNo);
				return true;
			}
		}
		return false;
	}

	/**
	 * 图文分页
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView imageTextList(HttpServletRequest request) throws Exception {
		int page = getPage(request.getParameter(PAGE));
		String wxno = getWeiXinPublicNo(request);
		ModelAndView mav = new ModelAndView(getBasePath() + "article");
		mav.addObject(PAGE, page);
		mav.addObject("totalCount", imageTextInfoService.findImageTextCount(wxno));
		mav.addObject(LIST, imageTextInfoService.findImageTextList(getPageStart(page), PAGE_SIZE, wxno));
		return mav;
	}

	/**
	 * 查看详细页面
	 * @param id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@LoginValidate(value = false)
	@RequestMapping("/detail/{type}/{id}")
	public ModelAndView findDetailArticle(@PathVariable("id") Long id, @PathVariable("type") String type) {
		ModelAndView mav = new ModelAndView(getBasePath() + "detail");
		if (id != null && StringUtils.isNotEmpty(type)) {
			if (Const.MESSAGE_TYPE_TEXT.equals(type)) {
				mav.addObject("imageText", imageTextInfoService.findByPK(id));
			} else {
//				mav.addObject("imageText", imageTextMoreService.findByPK(id));
			}
		}
		return mav;
	}

	@Override
	protected String getBasePath() {
		return "/material/imagetextinfo/";
	}

}
