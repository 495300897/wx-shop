package com.wisenet.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.wisenet.common.Const;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.User;
import com.wisenet.service.wx.UserService;

@Controller
public class LoginController extends PlatformController {
	@Autowired
	private UserService userService;
	
	/**
	 * 登录
	 * @param request
	 * @param username
	 * @param pwd
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/login")
	public ModelAndView login(HttpServletRequest request, String username, String pwd) throws Exception {
		ModelAndView mav = new ModelAndView(getBasePath());
		if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(pwd)) {
			User user = userService.findByUsernameAndPwd(username, pwd);
			if (user != null) {
				setSessionUser(request, user);
				mav.setViewName("redirect:/orderManage/list");
				return mav;
			}
		}
		mav.addObject(MESSAGE, "用户名或密码有误！");
		return mav;
	}
	
	/**
	 * 退出
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.removeAttribute(Const.LOGIN_USER_ATTRIBUTE);
		return getBasePath();
	}

	@Override
	protected String getBasePath() {
		return "/login";
	}
	
    
}
