package com.wisenet.controller.web.sportwear;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.SportWearInfo;
import com.wisenet.service.sportwear.SportWearInfoService;

/**
 * 商品详情管理控制类
 * @author fzh
 */
@RestController
@RequestMapping("/sportWear")
public class SportWearInfoController extends PlatformController {
	public static Logger logger = Logger.getLogger(SportWearInfoController.class);
	@Autowired
	private SportWearInfoService wearInfoService;
	
	/**
	 * 查询详情列表
	 * @param wearId
	 * @return
	 */
	@RequestMapping(value = "/infoList", method = RequestMethod.GET)
	public ModelAndView getInfoList(Integer wearId) {
		ModelAndView mav = new ModelAndView(getBasePath() + "infoList");
		List<SportWearInfo> list = wearInfoService.findListByWearId(wearId);
		mav.addObject(LIST, list);
		mav.addObject("wearId", wearId);
		return mav;
	}
	
	/**
	 * 商品详情
	 * @param wearInfo
	 * @return
	 */
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public ModelAndView wearInfo(SportWearInfo wearInfo) {
		ModelAndView mav = new ModelAndView(getBasePath() + "info");
		if (wearInfo.getId() != null) {
			wearInfo = wearInfoService.findById(wearInfo.getId());
		}
		mav.addObject("wearInfo", wearInfo);
		return mav;
	}
	
	/**
	 * 保存商品详情
	 * @param wearInfo
	 * @return
	 */
	@RequestMapping(value = "/info/save", method = RequestMethod.POST)
	public @ResponseBody JSONObject wearInfoSave(SportWearInfo wearInfo) {
		JSONObject json = getJsonResult(wearInfo.getWearId(), wearInfo.getPrice(), 
			wearInfo.getColor(), wearInfo.getColorUrl());
		if (!json.getBooleanValue(SUCCESS)) {
			return json;
		}
		SportWearInfo info = wearInfo.getId() == null ? null : wearInfoService.findById(wearInfo.getId());
		if (info == null) {
			wearInfoService.save(wearInfo);
		} else {
			wearInfoService.update(wearInfo);
		}
		return json;
	}
	
	@Override
	protected String getBasePath() {
		return "/sportwear/";
	}

}