package com.wisenet.controller.web.sportwear;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.annotation.LoginValidate;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.Banner;
import com.wisenet.entity.SportWear;
import com.wisenet.service.sportwear.BannerService;
import com.wisenet.service.sportwear.SportWearService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 商品管理控制类
 * @author fzh
 */
@RestController
@RequestMapping("/sportWear")
public class SportWearController extends PlatformController {
	public static Logger logger = Logger.getLogger(SportWearController.class);
	@Autowired
	private BannerService bannerService;
	@Autowired
	private SportWearService sportWearService;
	
	/**
	 * 查询商品列表
	 * @param page
	 * @param title
	 * @return
	 */
	@RequestMapping(value = "/goodsList", method = RequestMethod.GET)
	public ModelAndView getGoodsList(String page, String title) {
		int pages = getPage(page);
		ModelAndView mav = new ModelAndView(getBasePath() + "goodsList");
		List<SportWear> list = sportWearService.findList(title, getPageStart(pages), PAGE_SIZE);
		mav.addObject(LIST, list);
		mav.addObject(PAGE, pages);
		mav.addObject("title", title);
		return mav;
	}
	
	/**
	 * 列表查询，根据序号降序排序
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/bannerList", method = RequestMethod.GET)
	public ModelAndView getBannerList(Integer status) {
		ModelAndView mav = new ModelAndView(getBasePath() + "bannerList");
		List<Banner> list = bannerService.findList(status);
		mav.addObject(LIST, list);
		return mav;
	}
	
	/**
	 * 列表查询，根据序号降序排序
	 * @param status
	 * @return
	 */
	@LoginValidate(value = false)
	@RequestMapping(value = "/banner/list", method = RequestMethod.GET)
	public @ResponseBody List<Banner> findBannerList(Integer status) {
		List<Banner> list = bannerService.findList(status);
		return list;
	}
	
	/**
	 * 商品详情
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/goods/info", method = RequestMethod.GET)
	public ModelAndView goodsInfo(Integer id) {
		ModelAndView mav = new ModelAndView(getBasePath() + "goods");
		SportWear sportWear = sportWearService.findById(id);
		if (sportWear != null) {
			sportWear.setImageUrl(sportWear.getCoverUrls().split(",")[0]);
			mav.addObject("sportWear", sportWear);
		}
		return mav;
	}
	
	/**
	 * 保存商品
	 * @param sportWear
	 * @return
	 */
	@RequestMapping(value = "/goods/save", method = RequestMethod.POST)
	public @ResponseBody JSONObject goodsSave(SportWear sportWear) {
		JSONObject json = getJsonResult(sportWear.getTitle(), sportWear.getCoverUrls(),
			sportWear.getInfoUrls(), sportWear.getInfoText());
		if (!json.getBooleanValue(SUCCESS)) {
			return json;
		}
		SportWear wear = sportWear.getId() == null ? null : sportWearService.findById(sportWear.getId());
		if (wear == null) {
			sportWearService.save(sportWear);
		} else {
			sportWearService.update(sportWear);
		}
		return json;
	}
	
	/**
	 * BANNER详情
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/banner/info", method = RequestMethod.GET)
	public ModelAndView bannerInfo(Integer id) {
		ModelAndView mav = new ModelAndView(getBasePath() + "banner");
		Banner banner = bannerService.findById(id);
		mav.addObject("banner", banner);
		return mav;
	}
	
	/**
	 * 保存BANNER
	 * @param banner
	 * @return
	 */
	@RequestMapping(value = "/banner/save", method = RequestMethod.POST)
	public @ResponseBody JSONObject bannerSave(Banner banner) {
		JSONObject json = getJsonResult(banner.getImageUrl(), banner.getSortNum());
		if (!json.getBooleanValue(SUCCESS)) {
			return json;
		}
		Banner bannerInfo = banner.getId() == null ? null : bannerService.findById(banner.getId());
		if (bannerInfo == null) {
			bannerService.save(banner);
		} else {
			bannerService.update(banner);
		}
		return json;
	}
	
	@Override
	protected String getBasePath() {
		return "/sportwear/";
	}
	

}
