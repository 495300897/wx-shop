package com.wisenet.controller.web.sportwear;

import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.annotation.LoginValidate;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.SportWearOrder;
import com.wisenet.service.sportwear.SportWearOrderService;

/**
 * 订单管理控制类
 * @author fzh
 */
@RestController
@RequestMapping("/orderManage")
public class OrderManageController extends PlatformController {
	public static Logger logger = Logger.getLogger(OrderManageController.class);
	@Autowired
	private SportWearOrderService sportWearOrderService;
	
	/**
	 * 查询订单列表
	 * @param page
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView getOrderList(String page, SportWearOrder order) {
		int pages = getPage(page);
		ModelAndView mav = new ModelAndView(getBasePath() + LIST);
		List<SportWearOrder> list = sportWearOrderService.findListPage(order, getPageStart(pages), PAGE_SIZE);
		mav.addObject(LIST, list);
		mav.addObject(PAGE, pages);
		mav.addObject("order", order);
		return mav;
	}
	
	/**
	 * 修改发货、收货状态
	 * @param param
	 * @return
	 */
	@LoginValidate(value = false)
	@RequestMapping(value = "/updateDelivery", method = RequestMethod.POST)
	public @ResponseBody JSONObject updateDelivery(@RequestBody SportWearOrder param) {
		JSONObject json = getJsonResult(param.getId(), param.getUid(), param.getOutTradeNo());
		if (json.getBoolean(SUCCESS)) {
			// 更新订单状态
			int success = sportWearOrderService.updateOrderDelivery(param);
			if (success < 0) {
				json.put(SUCCESS, false);
				return json;
			}
			// 查询订单
			SportWearOrder order = sportWearOrderService.findByUidAndOutTradeNo(param.getUid(), param.getOutTradeNo());
			if (order == null || order.getPostId() == null) {
				json.put(SUCCESS, false);
				return json;
			}
		}
		return json;
	}
	
	@LoginValidate(value = false)
	@RequestMapping(value = "/queryExpress", method = RequestMethod.GET)
	public @ResponseBody JSONObject queryExpress(String postid,String type) {
		JSONObject json = getJsonResult(postid, type);
		if(json.getBoolean(SUCCESS)) {
			try {
				String url = "http://www.kuaidi100.com/query?type=%s&postid=%s";
				Document doc = Jsoup.connect(String.format(url, type, postid))
						.timeout(10000).ignoreContentType(true).get();
				Elements elements = doc.getElementsByTag("body");
				json.put("express", elements.get(0).text());
			} catch (Exception e) {
				e.printStackTrace();
				json.put(SUCCESS, false);
			}
		}
		return json;
	}
	
	@Override
	protected String getBasePath() {
		return "/sportwear/";
	}
	

}
