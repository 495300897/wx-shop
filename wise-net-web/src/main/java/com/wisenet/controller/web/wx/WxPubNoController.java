package com.wisenet.controller.web.wx;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.JssdkConfig;
import com.wisenet.entity.User;
import com.wisenet.entity.WeixinPubno;
import com.wisenet.service.wx.JssdkConfigService;
import com.wisenet.service.wx.UserService;
import com.wisenet.service.wx.WeiXinPubnoService;

@Controller
@RequestMapping("/wxpubno")
public class WxPubNoController extends PlatformController {
	@Autowired
	private UserService userService;
	@Autowired
	private JssdkConfigService jssdkConfigService;
	@Autowired
	private WeiXinPubnoService weiXinPubnoService;

	@RequestMapping("/JSSDKUI")
	public String JSSDKUI() {
		return "/jssdk/jssdkConfig";
	}

	@RequestMapping("/oauthUI")
	public String oauthUI() {
		return "/oauth/oauthConfig";
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView getList(String page, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView(getBasePath() + "/list");
		User user = getSessionUser(request);
		if (user != null) {
			int pages = getPage(page);
			List<WeixinPubno> list = weiXinPubnoService.getList(getPageStart(pages), PAGE_SIZE, user.getUsername());
			mav.addObject(LIST, list);
			mav.addObject(PAGE, pages);
		}
		return mav;
	}

	@RequestMapping("/editUI")
	public ModelAndView editUI(Integer id) {
		ModelAndView mav = new ModelAndView(getBasePath() + "/wxnoEdit");
		if (id != null) {
			WeixinPubno wxpubno = weiXinPubnoService.findById(id);
			mav.addObject("wxpubno", wxpubno);
		}
		return mav;
	}

	/**
	 * 保存公众号信息，把公众号信息保存到用户表
	 * @param wxpubno
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	public ModelAndView saveOrUpdate(WeixinPubno wxpubno, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("redirect:/wxpubno/list");
		User user = getSessionUser(request);
		// 保存公众号信息
		wxpubno.setOperator(user.getUsername());
		if (StringUtils.isEmpty(wxpubno.getId())) {
			weiXinPubnoService.save(wxpubno);
		} else {
			weiXinPubnoService.update(wxpubno);
		}
		// 把公众号信息保存到用户表
		if ("pubno".equals(wxpubno.getPubnoType())) {
			User usr = userService.findByUsername(user.getUsername());
			if (usr != null) {
				usr.setWxpublicno(wxpubno.getPubno());
				userService.updateUser(usr);
			}
		}
		return mav;
	}

	/**
	 * 保存菜单授权信息
	 * @param request
	 * @param attr
	 * @return
	 */
	public ModelAndView authorizeMenu(HttpServletRequest request, RedirectAttributes attr) {
		ModelAndView mav = new ModelAndView("redirect:/menu/wxtreeUI");
		WeixinPubno wxpnlist = weiXinPubnoService.findByWxNo(getWeiXinPublicNo(request));
		if (wxpnlist != null) {
			wxpnlist.setAppid(request.getParameter("appid"));
			wxpnlist.setAppsecret(request.getParameter("secret"));
			weiXinPubnoService.update(wxpnlist);
			attr.addFlashAttribute("ok", "保存成功，请重新发布菜单！");
		}
		return mav;
	}

	/**
	 * 保存JSSDK配置
	 * @param jssdkConfig
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveJSSDK")
	public @ResponseBody JSONObject saveJSSDK(JssdkConfig jssdkConfig) throws Exception {
		JSONObject json = getJsonResult(jssdkConfig);
		if (!json.getBooleanValue(SUCCESS)) {
			json.put("data", "no");
			return json;
		}
		int suc = jssdkConfigService.save(jssdkConfig);
		if (suc > 0) {
			json.put("data", "ok");
		}
		return json;
	}

	@Override
	protected String getBasePath() {
		return "/wxaccount/";
	}
}
