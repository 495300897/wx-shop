package com.wisenet.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.common.Const;
import com.wisenet.entity.User;

/**
 * 平台controller
 * @author fzh
 */
@Controller
public abstract class PlatformController {
	/**返回信息*/
	protected static final String RESULT = "result";
	/**提示信息*/
	protected static final String MESSAGE = "message";
	protected static final String SUCCESS = "success";
	/**分页信息*/
	protected static final String PAGE = "page";
	/**每页15条*/
	protected static final int PAGE_SIZE = 15;
	/**列表数据*/
	protected static final String LIST = "list";
	/**分页总条数*/
	protected static final String TOTAL_COUNT = "totalCount";
	/**错误标识*/
	protected static final String ERROR = "error";
	protected static final String APPID = "APPID";
	protected static final String SECRET = "SECRET";
	/**返回视图路径**/
	protected abstract String getBasePath();
	
	/** 基于@ExceptionHandler异常处理 */
    @ExceptionHandler
    public String exp(HttpServletRequest request, Exception ex) {
        request.setAttribute("ex", ex);
        ex.printStackTrace();
        // 根据不同错误转向不同页面  
        return "error_500";
    }
    
    /**
	 * 分页
	 * @param page
	 * @return
	 */
	protected int getPage(String page) {
		try {
			int PAGE = Integer.parseInt(page);
			return PAGE <= 0 ? 1 : PAGE;
		} catch (Exception e) {
			return 1;
		}
	}
	
	/**
	 * 分页起始行数
	 * @param page
	 * @return
	 */
	protected int getPageStart(int page) {
		return (page - 1) * PAGE_SIZE;
	}
	
	/**
	 * 获取工程真实路径
	 * @param request
	 * @return
	 */
	protected String getRealPath(HttpServletRequest request) {
		return request.getSession().getServletContext().getRealPath("/");
	}
	
	/**
	 * 获取用户登录信息
	 * @param request
	 * @return
	 */
	protected User getSessionUser(HttpServletRequest request) {
		return (User) request.getSession().getAttribute(Const.LOGIN_USER_ATTRIBUTE);
	}
	
	/**
	 * 保存用户登录信息
	 * @param request
	 * @param user
	 */
	protected void setSessionUser(HttpServletRequest request, User user) {
		request.getSession().setAttribute(Const.LOGIN_USER_ATTRIBUTE, user);
	}
	
	/**
	 * 获取用户公众号
	 * @param request
	 * @return
	 */
	protected String getWeiXinPublicNo(HttpServletRequest request) {
		User user = getSessionUser(request);
		return user != null ? user.getWxpublicno() : null;
	}
	
	/**
	 * 参数非空校验
	 * @param params
	 * @return
	 */
	protected JSONObject getJsonResult(Object... params) {
		JSONObject json = new JSONObject();
		try {
			String str = "传参：";
			for (Object param : params) {
				str += param + " ";
				if (param == null || "".equals(param) 
						|| "null".equals(param) || "undefined".equals(param)) {
					json.put(SUCCESS, false);
					json.put(MESSAGE, "invalid params");
					System.out.println(str);
					return json;
				}
			}
			json.put(SUCCESS, true);
//			System.out.println(str);
		} catch (Exception e) {
			json.put(SUCCESS, false);
			json.put(MESSAGE, "invalid params");
		}
		json.put(MESSAGE, "操作成功");
		return json;
	}
	
}
