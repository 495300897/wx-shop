package com.wisenet.controller.client;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 根据方法名调用的基类SERVLET
 * @author fzh
 *
 */
public class BaseServlet extends HttpServlet {
	private static final long serialVersionUID = -4432047232692829900L;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html;charset=UTF-8");
			
		String methodName = req.getParameter("method");
		if (methodName == null || methodName.isEmpty()) {
			System.out.println("亲，你没有传入方法名...");
			return;
		}
		Class clazz = this.getClass();
		try {
			Method method = clazz.getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
			String result = (String) method.invoke(this, req, resp);
			if (result != null && !result.isEmpty()) {
				req.getRequestDispatcher(result).forward(req, resp);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
}
