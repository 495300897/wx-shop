package com.wisenet.controller.client.sportwear;

import java.util.List;
import java.util.SortedMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.annotation.LoginValidate;
import com.wisenet.cache.SimpleCacheProvider;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.SportWearAddr;
import com.wisenet.entity.SportWearInfo;
import com.wisenet.entity.SportWearOrder;
import com.wisenet.entity.WeixinPubno;
import com.wisenet.service.sportwear.SportWearAddrService;
import com.wisenet.service.sportwear.SportWearInfoService;
import com.wisenet.service.sportwear.SportWearOrderService;
import com.wisenet.service.sportwear.SportWearPayService;

/**
 * 运动服订单管理控制类
 * @author fzh
 */
@RestController
@RequestMapping("/sport/order")
public class OrderController extends PlatformController {
	public static Logger logger = Logger.getLogger(OrderController.class);
	@Autowired
	private SimpleCacheProvider simpleCacheProvider;
	@Autowired
	private SportWearPayService sportWearPayService;
	@Autowired
	private SportWearInfoService sportWearInfoService;
	@Autowired
	private SportWearAddrService sportWearAddrService;
	@Autowired
	private SportWearOrderService sportWearOrderService;
	
	/**
	 * 查询订单列表
	 * @param uid
	 * @return
	 */
	@LoginValidate(value = false)
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody JSONObject getOrderList(String uid) {
		JSONObject json = getJsonResult(uid);
		if (json.getBoolean(SUCCESS)) {
			List<SportWearOrder> list = sportWearOrderService.findByUid(uid);
			for (SportWearOrder order : list) {
				// 订单列表中用来设置按钮点击状态
				order.setPayed(false);
				SportWearAddr address = sportWearAddrService.findById(order.getAddressId());
				order.setAddress(address);
			}
			json.put(LIST, list);
		}
		return json;
	}
	
	/**
	 * 用户支付
	 * @param orderParam
	 * @return
	 */
	@LoginValidate(value = false)
	@RequestMapping(value = "/toPay", method = RequestMethod.POST)
	public @ResponseBody JSONObject saveOrderAndPay(@RequestBody SportWearOrder orderParam) {
		JSONObject json = getJsonResult(orderParam.getWearId(), orderParam.getUid(), 
				orderParam.getCount(), orderParam.getPrice(), orderParam.getSize());
		if (json.getBoolean(SUCCESS)) {
			SportWearInfo wearInfo = sportWearInfoService.findById(orderParam.getWearId());
			if (wearInfo != null && wearInfo.getStock() == 1) {
				WeixinPubno weixinPubno = simpleCacheProvider.get(orderParam.getAppId());
				if (weixinPubno == null) {
					json.put(SUCCESS, false);
					json.put(MESSAGE, "APP不存在");
					return json;
				}
				// 防止传入非法金额
				orderParam.setPrice(wearInfo.getPrice());
				SortedMap<String, String> payment = sportWearPayService.saveOrderAndPay(orderParam, "save", weixinPubno);
				json.put("payment", payment);
			} else {
				json.put(SUCCESS, false);
				json.put(MESSAGE, "商品不存在或已下架");
			}
		}
		return json;
	}
	
	/**
	 * 订单列表中再次支付
	 * @param orderParam
	 * @return
	 */
	@LoginValidate(value = false)
	@RequestMapping(value = "/payAgain", method = RequestMethod.POST)
	public @ResponseBody JSONObject updateOrderAndPay(@RequestBody SportWearOrder orderParam) {
		JSONObject json = getJsonResult(orderParam.getId(), orderParam.getCount(), 
				orderParam.getUid(), orderParam.getWearId());
		if (json.getBoolean(SUCCESS)) {
			SportWearInfo wearInfo = sportWearInfoService.findById(orderParam.getWearId());
			if (wearInfo != null && wearInfo.getStock() == 1) {
				WeixinPubno weixinPubno = simpleCacheProvider.get(orderParam.getAppId());
				if (weixinPubno == null) {
					json.put(SUCCESS, false);
					json.put(MESSAGE, "APP不存在");
					return json;
				}
				// 防止传入非法金额
				orderParam.setPrice(wearInfo.getPrice());
				SortedMap<String, String> payment = sportWearPayService.saveOrderAndPay(orderParam, "update", weixinPubno);
				json.put("payment", payment);
			} else {
				json.put(SUCCESS, false);
				json.put(MESSAGE, "商品不存在或已下架");
			}
		}
		return json;
	}
	
	/**
	 * 取消订单
	 * @param orderParam
	 * @return
	 */
	@LoginValidate(value = false)
	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	public @ResponseBody JSONObject cancelOrder(@RequestBody SportWearOrder orderParam) {
		JSONObject json = getJsonResult(orderParam.getId(), orderParam.getUid());
		if (json.getBoolean(SUCCESS)) {
			long success = sportWearOrderService.cancelOrder(orderParam);
			if (success < 0) json.put(SUCCESS, false);
		}
		return json;
	}
	
	@Override
	protected String getBasePath() {
		return null;
	}
	

}
