package com.wisenet.controller.client.sportwear;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.annotation.LoginValidate;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.SportWearAddr;
import com.wisenet.service.sportwear.SportWearAddrService;

/**
 * 收货地址管理控制类
 * @author fzh
 */
@RestController
@RequestMapping("/sport/addr")
public class AddressController extends PlatformController {
	public static Logger logger = Logger.getLogger(AddressController.class);
	@Autowired
	private SportWearAddrService sportWearAddrService;
	
	@LoginValidate(value = false)
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAddressList(String uid) {
		JSONObject json = getJsonResult(uid);
		if (json.getBoolean(SUCCESS)) {
			List<SportWearAddr> list = sportWearAddrService.findByUid(uid);
			json.put(LIST, JSONObject.toJSON(list));
		}
		return json;
	}
	
	@LoginValidate(value = false)
	@RequestMapping(value = "/getAddress", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAddress(int id) {
		JSONObject json = getJsonResult(id);
		if (json.getBoolean(SUCCESS)) {
			SportWearAddr address = sportWearAddrService.findById(id);
			json.put("address", JSONObject.toJSON(address));
		}
		return json;
	}
	
	@LoginValidate(value = false)
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody JSONObject saveAddress(@RequestBody SportWearAddr sportWearAddr) {
		JSONObject json = getJsonResult(sportWearAddr.getAddress(), sportWearAddr.getPhone(),
				sportWearAddr.getUid(), sportWearAddr.getName());
		if (json.getBoolean(SUCCESS)) {
			long addressId = sportWearAddrService.save(sportWearAddr);
			if (addressId < 0) {
				json.put(SUCCESS, false);
				json.put(MESSAGE, "保存失败");
			} else {
				json.put("addressId", addressId);
			}
		}
		return json;
	}
	
	@Override
	protected String getBasePath() {
		return null;
	}
	

}
