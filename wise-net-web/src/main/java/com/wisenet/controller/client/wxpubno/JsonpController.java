package com.wisenet.controller.client.wxpubno;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.annotation.LoginValidate;
import com.wisenet.service.wxpubno.ApiService;

@RestController
public class JsonpController {
	/**
	 * JSONP跨域请求(本质是GET，查看浏览器的General->Request Method)
	 * @return 
	 */
	@LoginValidate(value=false)
	@RequestMapping(value="/jsonp", produces="application/json;charset=UTF-8")
	public Object jsonp(String callback) throws Exception {
		JSONObject json = new JSONObject();
		json.put("data", "JSONP跨域请求(本质是GET，查看浏览器的General->Request Method)");
		return callback + String.format("(%s)", json);
	}
	
	@LoginValidate(value=false)
	@RequestMapping(value="/musicList", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public Object getMusicList(String name) throws Exception {
		return ApiService.getMusicList(name);
	}
	
	@LoginValidate(value=false)
	@RequestMapping(value="/getMp3Url", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public Object getMp3Url(String songid) throws Exception {
		return ApiService.getMp3Url(songid);
	}

}
