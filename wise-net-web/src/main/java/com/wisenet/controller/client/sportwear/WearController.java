package com.wisenet.controller.client.sportwear;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.annotation.LoginValidate;
import com.wisenet.controller.PlatformController;
import com.wisenet.entity.SportWear;
import com.wisenet.entity.SportWearInfo;
import com.wisenet.service.sportwear.SportWearInfoService;
import com.wisenet.service.sportwear.SportWearService;

/**
 * 运动服商城控制类
 * 
 * @author fzh
 */
@RestController
@RequestMapping("/sport")
public class WearController extends PlatformController {
	public static Logger logger = Logger.getLogger(WearController.class);
	@Autowired
	private SportWearService sportWearService;
	@Autowired
	private SportWearInfoService sportWearInfoService;

	@LoginValidate(value = false)
	@RequestMapping(value = "/wearList", method = RequestMethod.GET)
	public @ResponseBody JSONObject getWearList(String wearType) {
		JSONObject json = getJsonResult(wearType);
		if (json.getBoolean(SUCCESS)) {
			List<SportWear> list = sportWearService.findByType(wearType);
			json.put(LIST, JSONObject.toJSON(list));
		}
		return json;
	}

	@LoginValidate(value = false)
	@RequestMapping(value = "/searchList", method = RequestMethod.GET)
	public @ResponseBody JSONObject getWearListByTitle(String title) {
		JSONObject json = getJsonResult(title);
		if (json.getBoolean(SUCCESS)) {
			List<SportWear> list = sportWearService.findByTitle(title);
			json.put(LIST, JSONObject.toJSON(list));
		}
		return json;
	}

	@LoginValidate(value = false)
	@RequestMapping(value = "/wearInfo", method = RequestMethod.GET)
	public @ResponseBody JSONObject getWearInfo(int id) {
		JSONObject json = getJsonResult(id);
		if (json.getBoolean(SUCCESS)) {
			SportWear sportWear = sportWearService.findById(id);
			if (sportWear != null) {
				List<SportWearInfo> infoList = sportWearInfoService.findListByWearId(sportWear.getId());
				json.put("wear", sportWear);
				json.put("infoList", JSONObject.toJSON(infoList));
			}
		}
		return json;
	}

	@Override
	protected String getBasePath() {
		return null;
	}

}
