package com.wisenet.interceptor;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.wisenet.annotation.LoginValidate;
import com.wisenet.common.Const;

/**
 * 登录拦截器
 * @author fzh
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = Logger.getLogger(LoginInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		// spring mvc拦截请求
		if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
			LoginValidate loginValidate = ((HandlerMethod) handler).getMethodAnnotation(LoginValidate.class);
			// 没有声明则默认需要登录
			if (loginValidate == null || loginValidate.value()) {
				// 未登录
				if (request.getSession().getAttribute(Const.LOGIN_USER_ATTRIBUTE) == null) {
					// 获得请求路径的URI
					if (!request.getRequestURI().contains("login")) {
						response.sendRedirect(request.getContextPath() + "/login");
						return false;
					}

					// AJAX请求（响应头会有x-requested-with）
					String requestType = request.getHeader("X-Requested-With");
					if ("XMLHttpRequest".equalsIgnoreCase(requestType)) {
						logger.info(" AJAX拦截...");
						ServletOutputStream out = response.getOutputStream();
						// AJAX返回： error(e)->e.responseText
						out.print("unlogin");
						out.flush();
						out.close();
						return false;
					}
				}
			}
		}
		return true;
	}

}