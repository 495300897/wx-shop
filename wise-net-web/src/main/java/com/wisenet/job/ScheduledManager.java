package com.wisenet.job;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wisenet.util.VeDate;

@Component("scheduledManager")
@Lazy(true)
public class ScheduledManager {
	// 日志
	private static final Logger logger = LoggerFactory.getLogger(ScheduledManager.class);

	// 任务一
	@Scheduled(cron = "0/10 * * * * ?")
	public void autoCardCalculate() {
		logger.info("task ------- fzh -------" + VeDate.dateToString(new Date()));
	}

	// 任务二
/*	@Scheduled(cron = "0/5 * * * * ?")
	public void autoCardCalculate2() {
		System.out.println(simpleDateFormat.format(new Date()) + " ：执行中任务2……");
	}*/
}