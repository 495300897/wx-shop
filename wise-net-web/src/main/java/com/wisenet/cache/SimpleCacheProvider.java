package com.wisenet.cache;

import java.util.Map;

import com.google.common.collect.Maps;
import com.wisenet.entity.WeixinPubno;

/**
 * simple cache provider use map
 * @author fzh
 */
public class SimpleCacheProvider {
    public SimpleCacheProvider() {
    }
    
    private static Map<String, String> cache = Maps.newHashMap();
    private static SimpleCacheProvider instance = new SimpleCacheProvider();
    private static Map<String, WeixinPubno> cacheContainer = Maps.newHashMap();

    public SimpleCacheProvider getInstance() {
        return instance;
    }

    public void put(String key, String value) {
    	cache.put(key, value);
    }

    public String getString(String key) {
        return cache.get(key);
    }
    
    public void removeCache(String key) {
    	cache.remove(key);
    }
    
    public void put(String key, WeixinPubno weixinPubno) {
        cacheContainer.put(key, weixinPubno);
    }

    public WeixinPubno get(String key) {
        return cacheContainer.get(key);
    }

    public void remove(String key) {
        cacheContainer.remove(key);
    }

    public void clear() {
        cacheContainer.clear();
    }

}
