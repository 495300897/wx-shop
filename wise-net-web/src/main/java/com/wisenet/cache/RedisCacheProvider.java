package com.wisenet.cache;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * redis cache provider
 * @author fzh
 */
public class RedisCacheProvider implements CacheProvider {
    private RedisTemplate<String, Serializable> redisTemplate;

    public RedisTemplate<String, Serializable> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Serializable> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void put(final String key, final Serializable cacheObject) {
        redisTemplate.execute(new RedisCallback<Serializable>() {
            @Override
            public Serializable doInRedis(RedisConnection connection) throws DataAccessException {
                @SuppressWarnings("unchecked")
				RedisSerializer<Serializable> value = (RedisSerializer<Serializable>) redisTemplate.getValueSerializer();
                connection.set(redisTemplate.getStringSerializer().serialize(key), value.serialize(cacheObject));
                return null;
            }
        });
    }

    @Override
    public Serializable get(final String key) {
        return redisTemplate.execute(new RedisCallback<Serializable>() {
            @Override
            public Serializable doInRedis(RedisConnection connection)
                    throws DataAccessException {
                byte[] redisKey = redisTemplate.getStringSerializer().serialize(key);
                if (connection.exists(redisKey)) {
                    byte[] value = connection.get(redisKey);
                    Serializable valueSerial = (Serializable)redisTemplate.getValueSerializer()
                            .deserialize(value);
                    return valueSerial;
                }
                return null;
            }
        });
    }

    /**
     * 设置key过期时间
     * @param key
     * @param timeout
     * @param unit
     */
	public void setex(String key, long timeout, TimeUnit unit) {
		redisTemplate.expire(key, timeout, unit);
	}
	
	/**
	 * 设置key的值和过期时间
	 * @param key
	 * @param value
	 * @param timeout
	 * @param unit
	 */
	public void setex(String key, String value, long timeout, TimeUnit unit) {
		redisTemplate.opsForValue().set(key, value, timeout, unit);
	}
	
	 /**
     * HashSet
     * @param key 键
     * @param map 对应多个键值
     */
    public void hmset(String key, Map<String, Object> map) {
    	redisTemplate.opsForHash().putAll(key, map);
    }
    
    /**
     * 获取hashKey对应的所有键值
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }
	
	/**
	 * 设置key自增
	 * @param key
	 * @param delta
	 * @return
	 */
	public long incr(String key, long delta) {
    	return redisTemplate.opsForValue().increment(key, delta);
	}
	
	/**
     * 左起添加list元素
     * @param key
     * @param value
     * @return
     */
	public Long lPush(String key, Serializable value) {
		return redisTemplate.opsForList().leftPush(key, value);
	}
	
	/**
     * 右起添加list元素
     * @param key
     * @param value
     * @return
     */
	public Long rPush(String key, Serializable value) {
		return redisTemplate.opsForList().rightPush(key, value);
	}
	
	/**
     * 获取list
     * @param key
     * @param start
     * @param end
     * @return
     */
	public List<Serializable> lRange(String key, long start, long end) {
		return redisTemplate.opsForList().range(key, start, end);
	}
	
	 /**
     * 根据索引获取list元素
     * @param key
     * @param value
     * @return
     */
	public Serializable lIndex(String key, long index) {
		return redisTemplate.opsForList().index(key, index);
	}
	
	/**
	 * hash模糊扫描
	 * @param hashKey
	 * @param rowKey 此处使用表达式和键名进行匹配
	 * @return
	 */
	public Cursor<Entry<Object, Object>> hashscan(String hashKey, String rowKey) {
		Cursor<Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(hashKey,
				ScanOptions.scanOptions().match("*" + rowKey + "*").count(1000).build());
		return cursor;
	}
	
    @Override
    public void remove(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public void clear() {

    }

	@Override
	public ListOperations<String, Serializable> opsForList() {
		return redisTemplate.opsForList();
	}
	
}
