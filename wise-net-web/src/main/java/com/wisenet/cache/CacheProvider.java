package com.wisenet.cache;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.redis.core.ListOperations;

/**
 * cache provider
 * @author fzh
 */
public interface CacheProvider {
    /**
     * 放入cache中
     * @param key
     * @param cacheObject
     */
    void put(String key, Serializable cacheObject);

    /**
     * 获取放在cache中的内容
     * @param key
     * @return
     */
    Serializable get(String key);
    
    ListOperations<String, Serializable> opsForList();
    
    /**
     * 左起添加list元素
     * @param key
     * @param value
     * @return
     */
    Long lPush(String key, Serializable value);
    
    /**
     * 右起添加list元素
     * @param key
     * @param value
     * @return
     */
    Long rPush(String key, Serializable value);
    
    /**
     * 根据索引获取list元素
     * @param key
     * @param value
     * @return
     */
    Serializable lIndex(String key, long value);
    
    /**
     * 根据范围获取list
     * @param key
     * @param start
     * @param end
     * @return
     */
    List<Serializable> lRange(String key, long start, long end);
    
    /**
     * 清除cache中对应的值
     * @param key
     */
    void remove(String key);

    /**
     * 清除所有的cache
     */
    void clear();
}
