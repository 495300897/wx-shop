package com.wisenet.cache;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wisenet.dao.wx.WeiXinPubNoDAO;
import com.wisenet.entity.WeixinPubno;

@Component
public class CacheLoader {
	@Autowired
	private WeiXinPubNoDAO WeiXinPubNoDAO;
	@Autowired
	public SimpleCacheProvider simpleCacheProvider;
	
	public static Logger logger = Logger.getLogger(CacheLoader.class);

	@PostConstruct
    public void init() {
		logger.info("初始化公众号小程序常量...");
		
		List<WeixinPubno> list = WeiXinPubNoDAO.getList("admin");
		for (WeixinPubno weixinPubno : list) {
			simpleCacheProvider.put(weixinPubno.getAppid(), weixinPubno);
		}
	}
	
}
