package com.wisenet.cache;

import java.util.Collections;
import java.util.UUID;

import redis.clients.jedis.Jedis;

/**
 * RedisLock分布式锁
 * 加锁： 通过setnx 向特定的key写入一个随机字符串，并设置失效时间，写入成功即加锁成功
 * 注意点：
 *  必须给锁设置一个失效时间，避免死锁
 *  加锁时，每个节点产生一个随机字符串，避免锁误删
 *  写入随机字符串与设置失效时间必须是同时，保证加锁的原子性
 * 解锁：
 *  匹配随机字符串，删除redis特定的key数据
 *  要保证获取数据，判断一致以及删除数据三个操作是原子性
 *  执行如下lua脚本：LUA_SCRIPT
 */
public class RedisLock {
    
    /**SET key value NX PX 3000 成功返回值*/
    private static final String LOCK_SUCCESS = "OK";
    /**表示 NX 模式*/
    private static final String SET_IF_NOT_EXIST = "NX";
    /**单位 毫秒**/
    private static final String SET_WITH_EXPIRE_TIME_PX = "PX";
    /**线程安全存储**/
    private static final ThreadLocal<String> local = new ThreadLocal<>();
    /**lua脚本**/
    private static final String LUA_SCRIPT = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
    
    /**
     * 加锁
     * @param jedis
     * @param key
     * @param expireTime
     * @return
     */
    public static boolean lock(Jedis jedis, String key, int expireTime) {
        // 产生随机字符串
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");

        String result = jedis.set(key, uuid, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME_PX, expireTime);
        if (LOCK_SUCCESS.equals(result)) {
            // 随机字符串绑定线程
            local.set(uuid);
            return true;
        }
        return false;
    }

    /**
     * 释放锁
     * @param jedis
     * @param key
     * @return
     */
    public static boolean unLock(Jedis jedis, String key) {
        String uuid = local.get();
	    // 当前线程没有绑定uuid 直接返回
        if (uuid == null || "".equals(uuid)) {
            return false;
        }
        Object result = jedis.eval(LUA_SCRIPT, Collections.singletonList(key), Collections.singletonList(uuid));
        if (Long.valueOf(1).equals(result)) {
            // 解除绑定线程的随机字符串
            local.remove();
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost", 6379);
        jedis.select(0); // 选择第一个库
        
        RedisLock.lock(jedis, "LOCK_KEY", 3000);

//      RedisLock.unLock(jedis, LOCK_KEY);
    }

}
