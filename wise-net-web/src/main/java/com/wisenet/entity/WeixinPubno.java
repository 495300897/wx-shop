package com.wisenet.entity;

import java.util.Date;

import com.wisenet.annotation.Column;

public class WeixinPubno {
	private Integer id;
	private String pubno;
	private String pubnoType;
    private String appid;
    private String appsecret;
    private String interfaceUrl;
    private String token;
    private String operator;
    private Date createTime;
    private String pubnoName;
    private String mchId;
    private String mchKey;
    private String spbillIp;
    private String notifyUrl;
    @Column(value=false)
    private String type;
    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPubno() {
		return pubno;
	}
	public void setPubno(String pubno) {
		this.pubno = pubno;
	}
	public String getPubnoType() {
		return pubnoType;
	}
	public void setPubnoType(String pubnoType) {
		this.pubnoType = pubnoType;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getAppsecret() {
		return appsecret;
	}
	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}
	public String getInterfaceUrl() {
		return interfaceUrl;
	}
	public void setInterfaceUrl(String interfaceUrl) {
		this.interfaceUrl = interfaceUrl;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getPubnoName() {
		return pubnoName;
	}
	public void setPubnoName(String pubnoName) {
		this.pubnoName = pubnoName;
	}
	public String getMchId() {
		return mchId;
	}
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	public String getMchKey() {
		return mchKey;
	}
	public void setMchKey(String mchKey) {
		this.mchKey = mchKey;
	}
	public String getSpbillIp() {
		return spbillIp;
	}
	public void setSpbillIp(String spbillIp) {
		this.spbillIp = spbillIp;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
    
}
