package com.wisenet.entity;

import com.wisenet.annotation.Column;

public class SportWear {
	private Integer id;
	private String title;
	private String sizes;
	private String price;
	private String origPrice;
	private String infoUrls;
	private String wearType;
	private String infoText;
	private String coverUrls;
	// 封面图片第一个
	@Column(value = false)
	private String imageUrl;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSizes() {
		return sizes;
	}
	public void setSizes(String sizes) {
		this.sizes = sizes;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getOrigPrice() {
		return origPrice;
	}
	public void setOrigPrice(String origPrice) {
		this.origPrice = origPrice;
	}
	public String getInfoUrls() {
		return infoUrls;
	}
	public void setInfoUrls(String infoUrls) {
		this.infoUrls = infoUrls;
	}
	public String getWearType() {
		return wearType;
	}
	public void setWearType(String wearType) {
		this.wearType = wearType;
	}
	public String getInfoText() {
		return infoText;
	}
	public void setInfoText(String infoText) {
		this.infoText = infoText;
	}
	public String getCoverUrls() {
		return coverUrls;
	}
	public void setCoverUrls(String coverUrls) {
		this.coverUrls = coverUrls;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
}
