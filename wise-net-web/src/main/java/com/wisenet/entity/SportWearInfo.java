package com.wisenet.entity;

public class SportWearInfo {
	private Integer id;
	private Integer stock;
	private Integer wearId;
	private String color;
	private Double price;
	private String colorUrl;
	private Double origPrice;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public Integer getWearId() {
		return wearId;
	}
	public void setWearId(Integer wearId) {
		this.wearId = wearId;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getColorUrl() {
		return colorUrl;
	}
	public void setColorUrl(String colorUrl) {
		this.colorUrl = colorUrl;
	}
	public Double getOrigPrice() {
		return origPrice;
	}
	public void setOrigPrice(Double origPrice) {
		this.origPrice = origPrice;
	}
	
}
