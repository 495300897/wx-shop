package com.wisenet.entity;

import com.wisenet.annotation.Column;

public class SportWearOrder {
	private Integer id;
	private String uid;
	private Integer wearId;
	private String size;
	private String title;
	private String color;
	private String comment;
	private String formId;
	private String prepayId;
	private String outTradeNo;
	private Double price;
	private Integer count;
	// 0支付中，1已支付，2支付失败，3退款中，4已退款，-1删除订单
	private Integer payStatus;
	// 0未发货，1发货中，2已收货
	private Integer delivery;
	// 支付状态描述
	private String paymsg;
	private Integer addressId;
	private String postId;
	private String postType;
	private String imageUrl;
	private String createTime;
	private Double totalPrice;
	@Column(value = false)
	private String appId;
	@Column(value = false)
	private boolean payed;
	@Column(value = false)
	private SportWearAddr address;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Integer getWearId() {
		return wearId;
	}
	public void setWearId(Integer wearId) {
		this.wearId = wearId;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getPrepayId() {
		return prepayId;
	}
	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}
	public String getOutTradeNo() {
		return outTradeNo;
	}
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}
	public Integer getDelivery() {
		return delivery;
	}
	public void setDelivery(Integer delivery) {
		this.delivery = delivery;
	}
	public String getPaymsg() {
		return paymsg;
	}
	public void setPaymsg(String paymsg) {
		this.paymsg = paymsg;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getPostType() {
		return postType;
	}
	public void setPostType(String postType) {
		this.postType = postType;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public boolean isPayed() {
		return payed;
	}
	public void setPayed(boolean payed) {
		this.payed = payed;
	}
	public SportWearAddr getAddress() {
		return address;
	}
	public void setAddress(SportWearAddr address) {
		this.address = address;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
}
