package com.wisenet.entity;

import java.util.Date;

public class UserInfo {
	private Long id;
	private String userid;
	private Date time;
	private String type;
	// 猜图关数、猜数答案
	private String ids;
	private String location;
	private int count;
	// 猜数答案
	private String content;
	private Integer isend;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getIsend() {
		return isend;
	}
	public void setIsend(Integer isend) {
		this.isend = isend;
	}
	
}
