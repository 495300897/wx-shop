package com.wisenet.entity;

/**
 * 自定义菜单参数实体类
 * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html
 * @author fzh
 *
 */
public class MenuInfo {
	private Integer id;
	private Integer menuId;
	
	private String url;      // 页面路径，view、miniprogram类型必须
	private String mkey;     // 菜单KEY值，click等点击类型必须
	private String name;     // 菜单名称
	private String type;     // 菜单类型
	private String appid;    // 小程序appid
	private String pagepath; // 小程序路径
	
	private Long orderNo;
	private Long parentMenuId;
	private Long refImageTextNo;
    private String refText;
    private String createTime;
    private String weixinPublicNo;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMkey() {
		return mkey;
	}
	public void setMkey(String mkey) {
		this.mkey = mkey;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getPagepath() {
		return pagepath;
	}
	public void setPagepath(String pagepath) {
		this.pagepath = pagepath;
	}
	public Long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}
	public Long getParentMenuId() {
		return parentMenuId;
	}
	public void setParentMenuId(Long parentMenuId) {
		this.parentMenuId = parentMenuId;
	}
	public String getWeixinPublicNo() {
		return weixinPublicNo;
	}
	public void setWeixinPublicNo(String weixinPublicNo) {
		this.weixinPublicNo = weixinPublicNo;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getRefText() {
		return refText;
	}
	public void setRefText(String refText) {
		this.refText = refText;
	}
	public Long getRefImageTextNo() {
		return refImageTextNo;
	}
	public void setRefImageTextNo(Long refImageTextNo) {
		this.refImageTextNo = refImageTextNo;
	}
    
    
}
