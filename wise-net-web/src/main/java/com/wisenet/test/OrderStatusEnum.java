package com.wisenet.test;

public class OrderStatusEnum {
	public enum ResultCode {
		FILE_NULL_OR_ZERO(10001, "文件流为空或大小为0"),
		GET_FILE_FAILURE(10002, "获取文件失败"),
		FILE_NOT_FOUND(10003, "文件未找到"),
		FILE_UPLOAD_FAILURE(10004, "文件上传失败"),
		FILE_SIZE_EXCEEDS_LIMIT(10005, "文件大小超过限制");

		private int code;
		private String message;

		public static String getMessage(int code) {
			ResultCode[] codes = ResultCode.values();
			for (int i = 0; i < codes.length; i++) {
				if (code == codes[i].getCode()) {
					return codes[i].getMessage();
				}
			}
			return null;
		}

		private ResultCode(int code, String message) {
			this.code = code;
			this.message = message;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}

	public static void main(String[] args) {
		System.out.println(ResultCode.getMessage(10001));
	}

}