package com.wisenet.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.cache.RedisCacheProvider;
import com.wisenet.dao.mapper.IUserMapper;
import com.wisenet.entity.User;
import com.wisenet.service.wxpubno.WeixinService;
import com.wisenet.wx.util.WeChatApiUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TransactionConfiguration(defaultRollback = false)
@Transactional
public class JunitTest {
	@Autowired
	private RedisCacheProvider redis;
	@Autowired
	private IUserMapper userMapper;
	@Autowired
	private WeixinService weixinService;
	@Autowired
	private SqlSessionTemplate sqlSession;

	@Test
	public void checkImageTest() {
		String token = weixinService.getToken("", "");
		if (token == null) {
			return;
		}
		try {
			InputStream in = new FileInputStream("D://1.jpg");
			JSONObject json = WeChatApiUtil.checkImage(in, token);
			System.out.println(json);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void dbTest() {
		String iv = "xTudhySv2F7DxoE4Ih9TBQ==";
		String sessionKey = "tiihtNczf5v6AKRyjwEUhQ==";

		String encryptedData = "6HWOnft+llF62vPMTvIw3JTTueXnP9c17HErlx/pSJKfmHsdjKs1XlmWlpOSgMlXW2IGbJOXwPzGRGLMWD5QeBl3d/hbs2XQX+ov2ugGYK8LwxfyBEiPEYaJyGMqxtRQeeYoeCE3rxjo9U3e+UUyTIICIefdvIMTm1RVkdEoLNER5VnVvTVDC6shWxuAXNdsomy+k1NaD5f28pbrSXjKBzV+etwovkA11PvrMpQkZV+CowELuvyuh+F3N3wdLogzOlxv/XDz9JDlhzzNVSxPDylTZSCoJuNtfIuI8zWa8yNuY8AF9Wlvei0IC6dPxgT7yAdgtPV/oO5wvCPrj6ymlgD6xVe9SnYmz+l60VfzLeirybSD4ui+wzR4RfXaz86NJIQENQwVZYcSk1nNInJHi0ezpn/JBAG5M3p7kGOwGe9eg/NqfDeSSW9voIx++Na0U17ssrD5JRjSgnQT4nTmrg==";

		JSONObject jsonResult = weixinService.decryptData(encryptedData, iv, sessionKey);
		
		System.out.println(jsonResult);
		
		User user = userMapper.findById(1);
		System.out.println(user.getUsername());
		user = sqlSession.selectOne("com.wisenet.dao.mapper.IUserMapper.findById", 1);
		System.out.println(user.getUsername());
	}

	@Test
	public void testScan() {
		Cursor<Entry<Object, Object>> cursor = redis.hashscan("audio_HEART_2020", "06-");
		while (cursor.hasNext()) {
			Entry<Object, Object> entry = cursor.next();
			String key = (String) entry.getKey();
			Object value = entry.getValue();

			System.out.printf("%s %s\n", key, value);
		}
	}

	@Test
	public void audioTest() {
		String key = "%s-%s@%s_duration";
		key = String.format(key, "05-09", "鎴戣繖鏍峰鑻辫", "MzIwMTg2MTM4OF8yMjQ3NDg2OTc3");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(key, "40:14");
		redis.hmset("audio_HEART_2020_05", map);
	}

	@Test
	public void redisTest() {
		String a = (String) redis.get("USE_COUNT_oTv570r5gJO5lneikXbw8PLP3msA");
		System.out.println(a);
		/*
		 * redis.lPush("articles", "5Fq9qWauXnWBUiE0nQzT6w"); redis.lPush("articles",
		 * "IKWbL9gFK5rYZfEt7KjetQ"); redis.lPush("articles", "Jswfv19LsnGOGmrVA7U7-w");
		 * redis.lPush("articles", "OIJN9MisxSWmkdM4hzCxTw");
		 */

		// redis.put("english_category", EnglishTest.category());
		// redis.hmset("english_paper", EnglishTest.paper());

		/*
		 * String path = "C:\\Users\\49530\\Desktop\\tmp"; ArrayList<File> files =
		 * EnglishTest.getListFiles(path); for (int i = 0; i < files.size(); i++) {
		 * Map<String, Object> map; try { map =
		 * EnglishTest.getAudioInfo(files.get(i).toString()); // audio_CET-6_5
		 * audio_CET_6_NEW audio_CET_6_REAL audio_CET_6_REAL_10 // audio_YASI_4
		 * audio_YASI_5 audio_YASI_6 audio_YASI_7 audio_YASI_8 audio_YASI_9 //
		 * audio_YASI_10 audio_YASI_11 audio_YASI_12 audio_YASI_13 audio_YASI_14
		 * audio_YASI_15 // audio_YASI_OG audio_YASI_EXAM audio_YASI_LIFE //
		 * audio_TEM_NEW audio_TEM_400 audio_TEM_100 audio_TEM_300 audio_TEM_BASIC
		 * audio_TEM_BASIC_8 audio_TEM_BASIC_10 // audio_TOEFL_75 audio_TOEFL_SECRET
		 * redis.hmset("audio_TOEFL_SECRET", map); } catch (IOException e) {
		 * e.printStackTrace(); } }
		 */

		// Map<Object, Object> map = redis.hmget("audio_TOEFL_SECRET");
		// System.out.println(map);

		/*
		 * User user = new User(); user.setId(1); user.setUsername("鍐織杈�");
		 * user.setPassword("123456");
		 * redis.getRedisTemplate().opsForList().leftPush("list",
		 * JSON.toJSONString(user)); user = new User(); user.setId(2);
		 * user.setUsername("鍐織杈�2"); user.setPassword("456123");
		 * redis.getRedisTemplate().opsForList().leftPush("list",
		 * JSON.toJSONString(user)); user = new User(); user.setId(3);
		 * user.setUsername("鍐織杈�3"); user.setPassword("789456");
		 * redis.getRedisTemplate().opsForList().leftPush("list",
		 * JSON.toJSONString(user));
		 */

		// System.out.println(redis.getRedisTemplate().opsForList().index("list", 0));
	}

}
