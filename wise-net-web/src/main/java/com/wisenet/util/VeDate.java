package com.wisenet.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class VeDate {

	public static Date stringToDate(String strDate) {
		  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  ParsePosition pos = new ParsePosition(0);
		  return formatter.parse(strDate, pos);
	}
	
	public static String stringDate(String strDate, String format) {
		  SimpleDateFormat formatter = new SimpleDateFormat(format);
		  ParsePosition pos = new ParsePosition(0);
		  Date strtodate = formatter.parse(strDate, pos);
		  return formatter.format(strtodate);
	}
	
	/**
	 * yyyy-MM-dd格式的日期
	 * @param date
	 * @return
	 */
	public static String getDateYMD(Date date) {
		  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		  return formatter.format(date);
	}
	
	public static String getDateMDHM(Date date) {
		  SimpleDateFormat formatter = new SimpleDateFormat("MM-dd HH:mm");
		  return formatter.format(date);
	}
	
	public static String dateToString(Date date) {
		  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  return formatter.format(date);
	}
	
	/**
	 * yyyyMMddHHmmss型日期
	 * @param date
	 * @return
	 */
	public static String getDateYMDHMS(Date date) {
		  SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		  return formatter.format(date);
	}
	
	public static String getDateMMddHHmmss() {
		  SimpleDateFormat formatter = new SimpleDateFormat("MMddHHmmss");
		  return formatter.format(new Date());
	}
	
	public static boolean isSameDate(Date date1, Date date2) {
		return getDateYMD(date1).equals(getDateYMD(date2));
	}
	
	/**
	 * 获取是否过期
	 * @param oldT
	 * @param expireT
	 * @return
	 */
	public static boolean getExpireSeconds(long oldT, long expireT) {
		long expireSeconds = System.currentTimeMillis() - oldT;
		return expireSeconds / 1000 >= expireT;
	}
	
	/**
	 * 获取当前时间到次天凌晨的失效时间
	 * @return
	 */
	public static long getExpireSeconds() {
		Calendar calender = getCalendar();
		long beforeSeconds = calender.getTimeInMillis();
		// 设置当天时间为23:59:59
		calender.set(calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		long afterSeconds = calender.getTimeInMillis();
		return (afterSeconds - beforeSeconds) / 1000;
	}
	
	/**
	 * 获取当前日期
	 * @return
	 */
	public static Calendar getCalendar() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return cal;
	}
	
}
