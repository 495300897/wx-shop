package com.wisenet.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UNRepeatSixCode {
	
	/**
	 * 生成一个6位不可重复的字符编码
	 * @return
	 * @throws Exception
	 */
	public static String getUnRepeatSixCode() throws Exception {
		StringBuilder sixChar = new StringBuilder();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
		String time = sdf.format(date);
		for (int i = 0; i < time.length() / 2; i++) {
			String singleChar;
			String x = time.substring(i * 2, (i + 1) * 2);
			int b = Integer.parseInt(x);
			if (b < 10) {
				singleChar = Integer.toHexString(Integer.parseInt(x));
			} else if (b < 36) {
				singleChar = String.valueOf((char) (Integer.parseInt(x) + 55));
			} else {
				singleChar = String.valueOf((char) (Integer.parseInt(x) + 61));
			}
			sixChar.append(singleChar);
		}
		// 防止并发
		Thread.sleep(1000);
		return sixChar.toString().toUpperCase();
	}
	
}

