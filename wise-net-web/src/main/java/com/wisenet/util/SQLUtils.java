package com.wisenet.util;

import com.wisenet.annotation.Column;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.util.List;

public class SQLUtils {

	/**
	 * 创建insert语句及参数
	 * @param <T>
	 * @param t
	 * @param sql
	 * @param args
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> void createInsert(T t, StringBuffer sql, List<Object> args)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = t.getClass().getDeclaredFields();
		String tableName = t.getClass().getSimpleName();
		sql.append("INSERT INTO ").append(underscoreName(tableName)).append(" (");
		StringBuffer values = new StringBuffer(") VALUES (");
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.get(t) != null) {
				// 字段上是否有注解
				if (field.isAnnotationPresent(Column.class)) {
					// 输出注解属性
					Column colunm = field.getAnnotation(Column.class);
					if (!colunm.value()) continue;
				}
				sql.append(underscoreName(field.getName())).append(",");
				values.append("?,");
				args.add(field.get(t));
			}
		}
		// 删除最后一个字符
		sql.deleteCharAt(sql.length() - 1);
		values.deleteCharAt(values.length() - 1);
		sql.append(values).append(")");
	}

	/**
	 * 创建update语句及参数
	 * @param <T>
	 * @param t
	 * @param sql
	 * @param args
	 * @param key
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> void createUpdate(T t, StringBuffer sql, List<Object> args, String key)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = t.getClass().getDeclaredFields();
		String tableName = t.getClass().getSimpleName();
		sql.append("UPDATE ").append(underscoreName(tableName)).append(" SET ");
		String keyStr = null;
		Object keyValue = null;
		for (Field field : fields) {
			field.setAccessible(true);
			// 字段是否有值
			if (field.get(t) != null) {
				// 字段上是否有注解
				if (field.isAnnotationPresent(Column.class)) {
					// 输出注解属性
					Column column = field.getAnnotation(Column.class);
					if (!column.value()) continue;
				}
				String columnName = underscoreName(field.getName());
				if (StringUtils.isBlank(key) && "id".equals(columnName)) {
					keyStr = "id";
					keyValue = field.get(t);
					continue;
				}
				if (!StringUtils.isBlank(key) && key.equals(columnName)) {
					keyStr = key;
					keyValue = field.get(t);
					continue;
				}
				sql.append(columnName).append(" = ?,");
				args.add(field.get(t));
			}
		}
		args.add(keyValue);
		sql.deleteCharAt(sql.length() - 1);
		sql.append(" WHERE ").append(keyStr).append(" = ?");
	}
	
	/**
	 * 创建查询语句
	 * @param t
	 * @param sql
	 * @param args
	 * @param sort 如：create_time DESC
	 * @param start
	 * @param queryType 如：like 或 eq
	 * @param length
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> void createSelect(T t, StringBuffer sql, List<Object> args, String sort, String queryType, int start, int length)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = t.getClass().getDeclaredFields();
		String tableName = t.getClass().getSimpleName();
		sql.append("SELECT * FROM ").append(underscoreName(tableName)).append(" WHERE ");
		for (Field field : fields) {
			field.setAccessible(true);
			Object fieldVal = field.get(t);
			// 字段是否有值
			if (fieldVal != null && !"".equals(fieldVal)) {
				// 字段上是否有注解
				if (field.isAnnotationPresent(Column.class)) {
					// 输出注解属性
					Column colunm = field.getAnnotation(Column.class);
					if (!colunm.value()) continue;
				}
				String colnummName = underscoreName(field.getName());
				
				// 打印参数
				System.out.println(colnummName + " = " + fieldVal);
				
				sql.append(colnummName).append("like".equals(queryType) ? " LIKE ? AND " : "=? AND ");
				args.add("like".equals(queryType) ? "%" + fieldVal + "%" : fieldVal);
			}
		}
		// 删除字符
		if (sql.toString().contains("=? AND ") || sql.toString().contains("LIKE ? AND ")) {
			sql.delete(sql.lastIndexOf(" AND "), sql.length());
		} else {
			sql.delete(sql.lastIndexOf(" WHERE "), sql.length());
		}
		// 排序
		if (!StringUtils.isBlank(sort)) {
			sql.append(" ORDER BY ").append(sort);
		}
		// 分页
		if (length > 0) {
			// 设置参数值
			args.add(start);
			args.add(length);
			sql.append(" LIMIT ?,?");
			// 打印分页
			System.out.println("LIMIT " + start + ", " + length);
		}
	}

	/**
	 * 创建删除语句
	 * @param <T>
	 * @param clz
	 * @param key
	 * @param sql
	 */
	public static <T> void createDelete(Class<T> clz, String key, StringBuffer sql) {
		sql.append("DELETE FROM ").append(underscoreName(clz.getSimpleName())).append(" WHERE ");
		String keyStr = StringUtils.isBlank(key) ? "id" : key;
		sql.append(keyStr).append("=?");
	}

	/**
	 * 根据指定key获取对象
	 * @param <T>
	 * @param clz
	 * @param sql
	 * @param key
	 * @param fields
	 */
	public static <T> void createGetByKey(Class<T> clz, StringBuffer sql, String key, String fields) {
		sql.append("SELECT ").append(StringUtils.isBlank(fields) ? "*" : fields);
		sql.append(" FROM ").append(underscoreName(clz.getSimpleName())).append(" WHERE ");
		String keyStr = StringUtils.isBlank(key) ? "id" : key;
		sql.append(keyStr).append("=?");
	}
	
	/**
	 * 将驼峰式命名的字符串转换为下划线小写方式。如果转换前的驼峰式命名的字符串为空，则返回空字符串。
	 * 例如：helloWorld->hello_world
	 * @param name 转换前的驼峰式命名的字符串
	 * @return 转换后下划线小写方式命名的字符串
	 */
	public static String underscoreName(String name) {
	    StringBuilder result = new StringBuilder();
	    if (!StringUtils.isBlank(name)) {
	    	// 循环处理字符
	        for (int i = 0; i < name.length(); i++) {
	            String s = name.substring(i, i + 1);
	            // 在大写字母前添加下划线
	            if (i > 0 && s.equals(s.toUpperCase()) && !Character.isDigit(s.charAt(0))) {
	                result.append("_");
	            }
	            result.append(s);
	        }
	    }
	    return result.toString().toLowerCase();
	}
}
