package com.wisenet.util;

/**
 * 文件类型枚取
 */
public enum FileType {

	JPEG("FFD8FF"),

	PNG("89504E47"),

	GIF("47494638"),

	BMP("424D"),
	
	HEIC("000000186"),
	
	WEBP("52494646");
	
	/*TIFF("49492A00"), // TIFF

	DWG("41433130"), // CAD

	PSD("38425053"), // Adobe Photoshop

	RTF("7B5C727466"), // Rich Text Format

	XML("3C3F786D6C"), // XML

	HTML("68746D6C3E"), // HTML

	EML("44656C69766572792D646174653A"), // Email [thorough only]

	DBX("CFAD12FEC5FD746F"), // Outlook Express

	PST("2142444E"), // Outlook (pst)

	XLS_DOC("D0CF11E0"), // MS Word/Excel

	MDB("5374616E64617264204A"), // MS Access

	WPD("FF575043"), // WordPerfect

	EPS("252150532D41646F6265"), // Postscript

	PDF("255044462D312E"), // Adobe Acrobat

	QDF("AC9EBD8F"), // Quicken

	PWL("E3828596"), // Windows Password

	ZIP("504B0304"), // ZIP Archive

	RAR("52617221"), // RAR Archive

	WAV("57415645"), // Wave

	AVI("41564920"), // AVI

	RAM("2E7261FD"), // Real Audio

	RM("2E524D46"), // Real Media

	MPG("000001BA"), // MPEG (mpg)

	MOV("6D6F6F76"), // Quicktime

	ASF("3026B2758E66CF11"), // Windows Media

	MID("4D546864"); // MIDI
*/
	private String value = "";

	/**
	 * Constructor.
	 * @param type
	 */
	private FileType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}