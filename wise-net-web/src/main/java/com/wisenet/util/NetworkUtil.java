package com.wisenet.util;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class NetworkUtil {
    /**
     * Logger for this class
     */
	private static final Logger logger = Logger.getLogger(NetworkUtil.class);

    /**
     * 获取请求主机IP地址，如果通过代理进来，则透过防火墙获取真实IP地址
     * @param request
     * @return
     */
    public static String getIpAddr(HttpServletRequest request) {
    	logger.info(" 获取请求主机IP地址，如果通过代理进来，则透过防火墙获取真实IP地址...");
    	
        String ip = request.getHeader("X-Forwarded-For");
        if (logger.isInfoEnabled()) {
        	System.out.println(" X-Forwarded-For - String ip = " + ip);
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
                if (logger.isInfoEnabled()) {
                	System.out.println(" Proxy-Client-IP - String ip = " + ip);
                }
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
                if (logger.isInfoEnabled()) {
                	System.out.println(" WL-Proxy-Client-IP - String ip = " + ip);
                }
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
                if (logger.isInfoEnabled()) {
                	System.out.println(" HTTP_CLIENT_IP - String ip = " + ip);
                }
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
                if (logger.isInfoEnabled()) {
                	System.out.println(" HTTP_X_FORWARDED_FOR - String ip = " + ip);
                }
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
                if (logger.isInfoEnabled()) {
                	System.out.println(" getRemoteAddr - String ip = " + ip);
                }
            }
        } else if (ip.length() > 15) {
            String[] ips = ip.split(",");
            for (String strIp : ips) {
                if (!("unknown".equalsIgnoreCase(strIp))) {
                    ip = strIp;
                    break;
                }
            }
        }
        return ip;
    }
}