package com.wisenet.util;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

/**
 * 生成头像二维码
 * @author fzh
 */
public class QrcodeUtil {
	public static Logger logger = Logger.getLogger(QrcodeUtil.class);

	public static void main(String[] args) throws Exception {
		createHeadQrcode("D:/1.jpg", "https://wcgame.ax-tec.cn/images/twsz.jpg");
		
		// createCircularImage("https://wcgame.ax-tec.cn/images/twsz.jpg", 100, 20);
	}
	
	/**
	 * 创建头像二维码
	 * @param qrcodeUrl
	 * @param headUrl
	 * @return
	 */
	public static String createHeadQrcode(String qrcodeUrl, String headUrl) {
		int headImg_X_Y;  // 头像坐标
		int headImg_W_H = 50; // 头像宽高
		int headImg_radius = 20;  // 圆角像素
		try {
			// 二维码图片
			BufferedImage qrcodeImage = ImageIO.read(new File(qrcodeUrl));
			
			// 创建圆角头像图片
			BufferedImage headImage = createCircularImage(headUrl, headImg_W_H, headImg_radius);
			
			// 头像坐标
			headImg_X_Y = (qrcodeImage.getWidth() - headImg_W_H) / 2;
			
			logger.info("headImg_X_Y = " + headImg_X_Y);
			
			// 创建图片流
			BufferedImage bufferedImage = new BufferedImage(qrcodeImage.getWidth(), qrcodeImage.getWidth(), BufferedImage.TYPE_INT_RGB);
			
			// 创建画布模板
			Graphics2D grap2D = bufferedImage.createGraphics();
			// 写入二维码
			grap2D.drawImage(qrcodeImage, 0, 0, qrcodeImage.getWidth(), qrcodeImage.getWidth(), null);
			// 写入头像
			grap2D.drawImage(headImage, headImg_X_Y, headImg_X_Y, headImg_W_H, headImg_W_H, null);
			// 完成模板修改
			grap2D.dispose();

			// 获取新文件的地址
			File outputfile = new File("D:/head_qrcode.jpg");
			// 生成合成后的图片
			ImageIO.write(bufferedImage, "jpg", outputfile);
			
			// 释放资源
			headImage.flush();
			qrcodeImage.flush();
			bufferedImage.flush();
		} catch (Exception ex) {
			logger.info("生成图片出错：" + ex.getMessage());
		}
		return null;
	}

	/**
	 * 创建一个圆角图片
	 * @param headUrl
	 * @param imgSize
	 * @param radius
	 * @return
	 * @throws IOException
	 */
    private static BufferedImage createCircularImage(String headUrl, int imgSize, int radius) throws IOException {
    	// 创建图片流
        BufferedImage bufferedImage = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_ARGB);
        // 创建一个2D画布
        Graphics2D canvas = bufferedImage.createGraphics();
        canvas.setComposite(AlphaComposite.Src);
        canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // 设置圆角
        canvas.fill(new RoundRectangle2D.Float(0, 0, imgSize, imgSize, radius, radius));
        canvas.setComposite(AlphaComposite.SrcAtop);
        // 写入图片
        canvas.drawImage(ImageIO.read(new URL(headUrl)), 0, 0, imgSize, imgSize, null);
        // 设置边框
        canvas.setColor(Color.WHITE);
        canvas.setStroke(new BasicStroke(3));
        canvas.drawRoundRect(0, 0, imgSize, imgSize, imgSize / 2, imgSize / 2);
        // 结束画图
        canvas.dispose();
        
//      ImageIO.write(bufferedImage, "png", new File("D:/cirlce.jpg"));
        return bufferedImage;
    }
    
}
