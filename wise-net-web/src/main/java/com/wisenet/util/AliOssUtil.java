package com.wisenet.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.Bucket;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;

/**
 * 阿里云存储工具类
 * @author fzh
 *
 */
public class AliOssUtil {
	// 日志
	public static Logger logger = Logger.getLogger(AliOssUtil.class);
	
	/** 阿里云API的密钥Access Key ID */
	public String accessKeyId = "";
	/** 阿里云API的密钥Access Key Secret */
	public String accessKeySecret = "";
    /** 阿里云API的内或外网域名 */
	public String endpoint = "oss-cn-shenzhen.aliyuncs.com";
    /** 阿里云API的bucket名称 */
	public String bucketName = "";
    /** 阿里云API的文件夹名称 如：folder_name/ */
	public String folder = "default/";
	/** OSS客户端 */
    private OSSClient ossClient;

    public AliOssUtil() {
    	ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
    }
    
    public AliOssUtil(String endpoint, String bucketName, String folder, String accessKeyId, String accessKeySecret) {
    	this.folder = folder;
    	this.endpoint = endpoint;
    	this.bucketName = bucketName;
    	this.accessKeyId = accessKeyId;
    	this.accessKeySecret = accessKeySecret;
    	// 实例化阿里云存储
    	ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
    	logger.info("实例化阿里云存储... ossClient init");
    }
    
    /**
     * 销毁
     */
    public void destory() {
      logger.info("destory()... ossClient shutdown");
      ossClient.shutdown();
    }
    
    /**
     * 上传网络流
     * @param instream 文件流
     * @param fileName 文件名称 包括后缀名
     * @param folder 文件夹名称 如：folder_name/
     * @return 返回文件访问链接
     */
	public String uploadFileOSS(InputStream instream, String folder, String fileName) {
		String result = checkEmpty(bucketName, fileName);
		if (!"YES".equals(result)) {
			logger.info("非空检查：" + result);
			return result;
		}
		// 设置数字命名文件名称
		String[] arr = fileName.split("[.]");
		String tempName = arr.length > 2 ? arr[arr.length - 2] : arr[0];
		fileName = Math.abs(tempName.hashCode()) + "." + arr[arr.length - 1];
		
		try {
			// 创建上传Object的Metadata
			ObjectMetadata objectMetadata = new ObjectMetadata();
			objectMetadata.setContentLength(instream.available());
			objectMetadata.setCacheControl("no-cache");
			objectMetadata.setHeader("Pragma", "no-cache");
			objectMetadata.setContentType(getContentType(fileName));
			objectMetadata.setContentDisposition("inline;filename=" + fileName);
			// 上传文件
			PutObjectResult putResult = ossClient.putObject(bucketName, folder + fileName, instream, objectMetadata);
			result = putResult.getETag();
			// 返回文件访问链接
            if (result != null && result.length() > 0) {
            	result = "https://" + bucketName + "." + endpoint.replace("http://", "") + "/" + folder + fileName;
            }
		} catch (IOException e) {
			result = "message: " + e.getMessage();
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if (instream != null) {
					instream.close();
				}
			} catch (IOException e) {
				result = "message: " + e.getMessage();
				e.printStackTrace();
			}
			destory();
		}
		return result;
	}
	
	/**
     * 上传文件
     * @param file 上传文件（文件全路径如：D:\\image\\cake.jpg）
     * @return 返回文件访问链接
     */
    public String uploadObject2OSS(File file) {
        String result = null;
        try {
            // 以输入流的形式上传文件
            InputStream is = new FileInputStream(file);
            // 文件名
            String fileName = file.getName();
            // 文件大小
            long fileSize = file.length();
            // 创建上传Object的Metadata
            ObjectMetadata metadata = new ObjectMetadata();
            // 上传的文件的长度
            metadata.setContentLength(is.available());
            // 指定该Object被下载时的网页的缓存行为
            metadata.setCacheControl("no-cache");
            // 指定该Object下设置Header
            metadata.setHeader("Pragma", "no-cache");
            // 指定该Object被下载时的内容编码格式
            metadata.setContentEncoding("utf-8");
            // 文件的MIME，定义文件的类型及网页编码，决定浏览器将以什么形式、什么编码读取文件。如果用户没有指定则根据Key或文件名的扩展名生成，
            // 如果没有扩展名则填默认值application/octet-stream
            metadata.setContentType(getContentType(fileName));
            // 指定该Object被下载时的名称（指示MINME用户代理如何显示附加的文件，打开或下载，及文件名称）
            metadata.setContentDisposition("filename/filesize=" + fileName + "/" + fileSize + "Byte.");
            // 上传文件 (上传文件流的形式)
            PutObjectResult putResult = ossClient.putObject(bucketName, folder + fileName, is, metadata);
            // 解析结果
            result = putResult.getETag();
            // 返回文件访问链接
            if (result != null && result.length() > 0) {
            	result = "https://" + bucketName + "." + endpoint.replace("http://", "") + "/" + folder + fileName;
            }
        } catch (Exception e) {
        	result = "message: " + e.getMessage();
            e.printStackTrace();
            logger.error("上传异常：" + e.getMessage(), e);
        } finally {
        	destory();
        }
        return result;
    }

    /**
     * 创建存储空间
     * @param bucketName 存储空间
     * @return
     */
    public String createBucketName(String bucketName) {
        // 存储空间
        final String bucketNames = bucketName;
        if (!ossClient.doesBucketExist(bucketName)) {
            // 创建存储空间
            Bucket bucket = ossClient.createBucket(bucketName);
            logger.info("创建存储空间成功");
            return bucket.getName();
        }
        return bucketNames;
    }

    /**
     * 删除存储空间buckName
     * @param bucketName 存储空间
     */
    public void deleteBucket(String bucketName) {
        ossClient.deleteBucket(bucketName);
        logger.info("删除" + bucketName + "Bucket成功");
    }

    /**
     * 创建模拟文件夹
     * @param bucketName 存储空间
     * @param folder 模拟文件夹名如"qj_nanjing/"
     * @return 文件夹名
     */
    public String createFolder(String bucketName, String folder) {
        // 文件夹名
        final String keySuffixWithSlash = folder;
        // 判断文件夹是否存在，不存在则创建
        if (!ossClient.doesObjectExist(bucketName, keySuffixWithSlash)) {
            // 创建文件夹
            ossClient.putObject(bucketName, keySuffixWithSlash, new ByteArrayInputStream(new byte[0]));
            logger.info("创建文件夹成功");
            // 得到文件夹名
            OSSObject object = ossClient.getObject(bucketName, keySuffixWithSlash);
            String fileDir = object.getKey();
            return fileDir;
        }
        return keySuffixWithSlash;
    }

    /**
     * 根据key删除OSS服务器上的文件
     * @param folder 模拟文件夹名 如"qj_nanjing/"
     * @param key Bucket下的文件的路径名+文件名 如："upload/cake.jpg"
     */
    public void deleteFile(String folder, String key) {
        ossClient.deleteObject(bucketName, folder + key);
        logger.info("删除" + bucketName + "下的文件" + folder + key + "成功");
    }

    /**
     * 通过文件名判断并获取OSS服务文件上传时文件的contentType
     * @param fileName 文件名
     * @return 文件的contentType
     */
    public static String getContentType(String fileName) {
        // 文件的后缀名
        String fileExtension = fileName.substring(fileName.lastIndexOf("."));
        if (".bmp".equalsIgnoreCase(fileExtension)) {
            return "image/bmp";
        }
        if (".gif".equalsIgnoreCase(fileExtension)) {
            return "image/gif";
        }
        if (".jpeg".equalsIgnoreCase(fileExtension) || ".jpg".equalsIgnoreCase(fileExtension)
                || ".png".equalsIgnoreCase(fileExtension)) {
            return "image/jpeg";
        }
        if (".png".equalsIgnoreCase(fileExtension)) {
            return "image/png";
        }
        if (".html".equalsIgnoreCase(fileExtension)) {
            return "text/html";
        }
        if (".txt".equalsIgnoreCase(fileExtension)) {
            return "text/plain";
        }
        if (".vsd".equalsIgnoreCase(fileExtension)) {
            return "application/vnd.visio";
        }
        if (".ppt".equalsIgnoreCase(fileExtension) || "pptx".equalsIgnoreCase(fileExtension)) {
            return "application/vnd.ms-powerpoint";
        }
        if (".doc".equalsIgnoreCase(fileExtension) || "docx".equalsIgnoreCase(fileExtension)) {
            return "application/msword";
        }
        if (".xml".equalsIgnoreCase(fileExtension)) {
            return "text/xml";
        }
        // 默认返回类型
        return "";
    }
    
    /**
     * 非空检查
     * @param bucketName
     * @param fileName
     * @return
     */
    public String checkEmpty(String bucketName, String fileName) {
    	if (StringUtils.isEmpty(bucketName)) {
			return "message: bucketName不能为空！";
		}
		if (StringUtils.isEmpty(fileName)) {
			return "message: fileName不能为空！";
		}
		if (fileName.lastIndexOf(".") == -1) {
			return "message: fileName必须包含后缀！";
		}
		return "YES";
    }

}
