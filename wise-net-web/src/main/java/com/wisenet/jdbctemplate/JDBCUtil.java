package com.wisenet.jdbctemplate;

import com.wisenet.util.SQLUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/***
 * JDBC工具类
 * @author fzh
 */
public abstract class JDBCUtil {
	// 日志
	private static final Logger logger = Logger.getLogger(JDBCUtil.class);
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;

	/**
	 * 插入数据 Class类名一定和数据库表名相同
	 * @param t
	 * @return <T>
	 */
	public <T> int insert(T t) {
		StringBuffer sql = new StringBuffer();
		List<Object> args = new ArrayList<>();
		try {
			SQLUtils.createInsert(t, sql, args);
			return jdbcTemplate.update(sql.toString(), args.toArray());
		} catch (Exception e) {
			logger.error(String.format("Error createInsert sql params:%s", t), e);
			return 0;
		}
	}
	
	 /**
     * 插入一个对象，并返回这个对象的自增id
     * @param t
     * @return <T>
	 * @throws Exception 
     */
    public <T> long insertAndGetId(T t) throws Exception {
    	final StringBuffer sql = new StringBuffer();
		final List<Object> args = new ArrayList<>();
		SQLUtils.createInsert(t, sql, args);
		KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(conn -> {
			PreparedStatement preparedStatement = conn.prepareStatement(sql.toString(),
					PreparedStatement.RETURN_GENERATED_KEYS);
			for (int i = 0; i < args.size(); i ++) {
				preparedStatement.setObject(i + 1, args.get(i));
			}
			return preparedStatement;
		}, keyHolder);
        return keyHolder.getKey().longValue();
    }

	/**
	 * 更新赋值的实体 key 位主键名称，如果没有 使用id作为默认的
	 * @param t
	 * @param key
	 * @return <T>
	 */
	public <T> int update(T t, String key) {
		StringBuffer sql = new StringBuffer();
		List<Object> args = new ArrayList<>();
		try {
			SQLUtils.createUpdate(t, sql, args, key);
			return jdbcTemplate.update(sql.toString(), args.toArray());
		} catch (Exception e) {
			logger.error(String.format("Error createUpdate sql params:%s", t), e);
			return 0;
		}
	}
	
	/**
	 * 创建动态条件查询语句
	 * @param t
	 * @param sort 如：create_time DESC
	 * @param start
	 * @param queryType 如：like 或 eq
	 * @param length
	 * @return <T>
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> selectListByDynamicSql(T t, String sort, String queryType, int start, int length) {
		StringBuffer sql = new StringBuffer();
		List<Object> args = new ArrayList<>();
		try {
			SQLUtils.createSelect(t, sql, args, sort, queryType, start, length);
			logger.info(sql.toString());
			Class<T> clz = (Class<T>) t.getClass();
			return this.getListObject(clz, sql.toString(), args.toArray());
		} catch (Exception ex) {
			logger.error(String.format("Error selectListByDynamicSql sql params:%s", t), ex);
			return null;
		}
	}

	/**
	 * 批量更新数据
	 * @param sql
	 * @param values
	 * @return int[]
	 */
	public int[] batchUpdate(String sql, List<Object[]> values) {
		return jdbcTemplate.batchUpdate(sql, values);
	}
	
	/**
	 * 批量更新数据
	 * @param sql
	 * @param paramMap
	 * @return int
	 */
	public int batchUpdate(String sql, Map<String, Object> paramMap) {
		logger.info(sql);
		NamedParameterJdbcTemplate namedParamJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
		return namedParamJdbcTemplate.update(sql, new MapSqlParameterSource(paramMap));
	}
	
	/**
	 * 删除数据 key 位主键名称，如果没有 使用id作为默认的
	 * @param clz
	 * @param key
	 * @param value
	 * @return <T>
	 */
	public <T> int delete(Class<T> clz, String key, Object[] value) {
		if (value == null) return 0;
		StringBuffer sql = new StringBuffer();
		SQLUtils.createDelete(clz, key, sql);
		return jdbcTemplate.update(sql.toString(), value);
	}

	/**
	 * 获取单个对象
	 * @param clz
	 * @param sql
	 * @param args
	 * @return <T>
	 */
	public <T> T getObject(Class<T> clz, String sql, Object[] args) {
		return jdbcTemplate.query(sql, rs -> {
			final List<String> columns = getResultSetColumn(rs.getMetaData());
			if (rs.next()) {
				try {
					T t = clz.newInstance();
					Field[] fields = clz.getDeclaredFields();
					for (Field f : fields) {
						// 存在当前列
						String colnumnName = SQLUtils.underscoreName(f.getName());
						if (columns.contains(colnumnName)) {
							f.setAccessible(true);
							if (rs.getString(colnumnName) != null) {
								f.set(t, rs.getObject(colnumnName));
							}
						}
					}
//					logger.info(sql);
					return t;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}, args);
	}
	
	/**
	 * 返回多个对象
	 * @param clz
	 * @param sql
	 * @param args
	 * @return <T>
	 */
	public <T> List<T> getListObject(final Class<T> clz, final String sql, final Object[] args) {
		return jdbcTemplate.query(sql, args, new BeanPropertyRowMapper<>(clz));
	}
	
	/**
	 * 获取数据数量
	 * @param sql
	 * @param args
	 * @return int
	 */
	public int getCounts(final String sql, final Object[] args) {
		return jdbcTemplate.query(sql, rs -> {
			if (rs.next()) {
				try {
//						logger.info(sql);
					return rs.getInt("count");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 0;
		}, args);
	}
	
	/**
	 * 获取结果集中返回的字段名称
	 * @param metaData
	 * @return List<String>
	 * @throws SQLException
	 */
	protected List<String> getResultSetColumn(ResultSetMetaData metaData) throws SQLException {
		int num = metaData.getColumnCount();
		List<String> columns = new ArrayList<>();
		for (int i = 1; i <= num; i ++) {
			columns.add(metaData.getColumnName(i));
		}
		return columns;
	}


}
