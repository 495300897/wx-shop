package com.wisenet.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.wisenet.entity.User;

@Repository
public interface IUserMapper {

	List<User> findUser();
	
	int insert(User user);
	
	User findByUserName(String userName);
	
	@Select("SELECT * FROM user WHERE id = #{id}")
	User findById(int id);
}
