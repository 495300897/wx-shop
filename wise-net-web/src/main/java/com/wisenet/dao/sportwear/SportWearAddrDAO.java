package com.wisenet.dao.sportwear;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.SportWearAddr;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class SportWearAddrDAO extends JDBCUtil {

	public SportWearAddr findById(int id) {
		String sql = "SELECT * FROM sport_wear_addr WHERE id = ? LIMIT 1";
		return this.getObject(SportWearAddr.class, sql, new Object[]{ id });
	}
	
	public List<SportWearAddr> findByUid(String uid) {
		try {
			String sql = "SELECT * FROM sport_wear_addr WHERE uid = ? ORDER BY create_time DESC LIMIT 10;";
			return this.getListObject(SportWearAddr.class, sql, new Object[]{ uid });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
    
}
