package com.wisenet.dao.sportwear;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.SportWear;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class SportWearDAO extends JDBCUtil {
    
//	@Cacheable(value="myCache", key="#id")
	public SportWear findById(int id) {
		try {
			String sql = "SELECT * FROM sport_wear WHERE id = ? LIMIT 1";
			return this.getObject(SportWear.class, sql, new Object[] { id });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//	@Cacheable(value="myCache", key="#wearType")
	public List<SportWear> findByType(String wearType) {
		try {
			String sql = "SELECT * FROM sport_wear WHERE wear_type = ?";
			return this.getListObject(SportWear.class, sql, new Object[] { wearType });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<SportWear> findByTitle(String title) {
		try {
			String sql = "SELECT * FROM sport_wear WHERE title LIKE ?";
			return this.getListObject(SportWear.class, sql, new Object[] { "%" + title + "%" });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<SportWear> findList(String title, int start, int length) {
		String sql = "SELECT * FROM sport_wear ORDER BY id DESC LIMIT ?, ?";
		if (title == null || title.length() == 0) {
			return this.getListObject(SportWear.class, sql, new Object[] { start, length });
		}
		sql = "SELECT * FROM sport_wear WHERE title LIKE ? ORDER BY id DESC LIMIT ?, ?";
		return this.getListObject(SportWear.class, sql, new Object[] { ("%" + title + "%"), start, length });
	}
    
}
