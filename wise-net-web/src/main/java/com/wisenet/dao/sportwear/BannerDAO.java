package com.wisenet.dao.sportwear;

import com.wisenet.entity.Banner;
import com.wisenet.jdbctemplate.JDBCUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BannerDAO extends JDBCUtil {

	public Banner findById(Integer id) {
		String sql = "SELECT * FROM banner WHERE id = ? LIMIT 1";
		return this.getObject(Banner.class, sql, new Object[] { id });
	}
	
	/**
	 * 列表查询，根据序号降序排序
	 * @param status
	 * @return
	 */
	public List<Banner> findList(Integer status) {
		String sql = "SELECT * FROM banner ORDER BY sort_num DESC";
		if (status == null) {
			return this.getListObject(Banner.class, sql, new Object[] {});
		}
		sql = "SELECT * FROM banner WHERE status = ? ORDER BY sort_num DESC LIMIT 8";
		return this.getListObject(Banner.class, sql, new Object[] { status });
	}
    
}
