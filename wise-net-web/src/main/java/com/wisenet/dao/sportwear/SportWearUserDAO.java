package com.wisenet.dao.sportwear;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.SportWearUser;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class SportWearUserDAO extends JDBCUtil {

	public List<SportWearUser> findByName(String name) {
		try {
			String sql = "SELECT * FROM sport_wear_user WHERE name LIKE ? ORDER BY create_time DESC LIMIT 10;";
			return this.getListObject(SportWearUser.class, sql, new Object[]{ "%" + name + "%" });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public SportWearUser findByUid(String uid) {
		try {
			String sql = "SELECT * FROM sport_wear_user WHERE uid = ? LIMIT 1";
			return this.getObject(SportWearUser.class, sql, new Object[]{ uid });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
    
}
