package com.wisenet.dao.sportwear;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.SportWearOrder;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class SportWearOrderDAO extends JDBCUtil {

	public List<SportWearOrder> findByUid(String uid) {
		try {
			String sql = "SELECT * FROM sport_wear_order WHERE uid = ? ORDER BY create_time DESC";
			return this.getListObject(SportWearOrder.class, sql, new Object[]{ uid });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public SportWearOrder findByOutTradeNo(String outTradeNo) {
		try {
			String sql = "SELECT * FROM sport_wear_order WHERE out_trade_no = ? LIMIT 1";
			return this.getObject(SportWearOrder.class, sql, new Object[]{ outTradeNo });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public SportWearOrder findById(int id) {
		try {
			String sql = "SELECT * FROM sport_wear_order WHERE id = ? LIMIT 1";
			return this.getObject(SportWearOrder.class, sql, new Object[]{ id });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public int cancelOrder(int id, String uid) {
		return jdbcTemplate.update("UPDATE sport_wear_order SET pay_status = -1, paymsg = '用户主动取消订单' WHERE id = ? AND uid = ?",
				new Object[]{ id, uid });
	}

	public int updateOrderStatus(int id, int status, String uid, String paymsg) {
		return jdbcTemplate.update("UPDATE sport_wear_order SET pay_status = ?, paymsg = ? WHERE id = ? AND uid = ?",
				new Object[]{ status, paymsg, id, uid });
	}
	
	public int updateOrderDelivery(SportWearOrder param) {
		// 录入订单号
		if (param.getPostId() != null && param.getPostId().length() > 0) {
			return jdbcTemplate.update("UPDATE sport_wear_order SET delivery = ?, post_id = ?, post_type = ? WHERE id = ? AND uid = ? AND out_trade_no = ?",
					new Object[]{ param.getDelivery(), param.getPostId(), param.getPostType(), param.getId(), param.getUid(), param.getOutTradeNo() });
		}
		// 修改发货状态
		return jdbcTemplate.update("UPDATE sport_wear_order SET delivery = ? WHERE id = ? AND uid = ? AND out_trade_no = ?",
				new Object[]{ param.getDelivery(), param.getId(), param.getUid(), param.getOutTradeNo() });
	}

	public SportWearOrder findByUidAndOutTradeNo(String openid, String outTradeNo) {
		try {
			String sql = "SELECT * FROM sport_wear_order WHERE uid = ? AND out_trade_no = ? LIMIT 1";
			return this.getObject(SportWearOrder.class, sql, new Object[]{ openid, outTradeNo });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
