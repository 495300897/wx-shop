package com.wisenet.dao.sportwear;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.SportWearInfo;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class SportWearInfoDAO extends JDBCUtil {
    
//	@Cacheable(value="myCache", key="#id")
	public SportWearInfo findById(int id) {
		try {
			String sql = "SELECT * FROM sport_wear_info WHERE id = ? LIMIT 1";
			return this.getObject(SportWearInfo.class, sql, new Object[]{ id });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 注意：上一个方法中wearId已用作key缓存，这里需要对key作区分
	 * @param wearId
	 * @return
	 */
//	@Cacheable(value="myCache", key="#wearId")
	public List<SportWearInfo> findListByWearId(int wearId) {
		try {
			String sql = "SELECT * FROM sport_wear_info WHERE wear_id = ? ORDER BY id DESC";
			return this.getListObject(SportWearInfo.class, sql, new Object[]{ wearId });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
    
}
