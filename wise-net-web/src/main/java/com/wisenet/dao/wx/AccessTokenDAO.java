package com.wisenet.dao.wx;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.AccessToken;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class AccessTokenDAO extends JDBCUtil {
    
    public int save(AccessToken token){
    	return insert(token);
    }

	public AccessToken findToken(String appid, String secret) {
		try {
			String sql = "SELECT * FROM access_token WHERE appid = ? AND appsecret = ? LIMIT 1";
			return this.getObject(AccessToken.class, sql, new Object[]{ appid, secret });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
    
}
