package com.wisenet.dao.wx;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.JssdkConfig;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class JssdkConfigDAO extends JDBCUtil {
    
    public int save(JssdkConfig jssdkConfig){
    	return insert(jssdkConfig);
    }
    
}
