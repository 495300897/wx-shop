package com.wisenet.dao.wx;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.FirstJoinKey;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class FirstJoinKeyDAO extends JDBCUtil {
    
	public int save(FirstJoinKey firstJoinKey) {
		return insert(firstJoinKey);
	}

	public void delete(int id) {
    	try {
    		jdbcTemplate.update("DELETE FROM first_join_key WHERE id = ?", new Object[]{id});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FirstJoinKey findByWxNo(String WxNo) {
    	try {
			String sql = "SELECT * FROM first_join_key k WHERE k.weixin_public_no = ?";
			return this.getObject(FirstJoinKey.class, sql, new Object[]{ WxNo });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
