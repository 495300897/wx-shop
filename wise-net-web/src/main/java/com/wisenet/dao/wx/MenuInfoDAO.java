package com.wisenet.dao.wx;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.MenuInfo;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class MenuInfoDAO extends JDBCUtil {
	
	public int save(MenuInfo menuInfo) {
		return insert(menuInfo);
	}
	
	public int deleteMenuInfo(int id) {
		return jdbcTemplate.update("DELETE FROM menu_info WHERE id = ?", new Object[] { id });
	}
	
	public int deleteByWxNo(String wxno) {
		String sql = "DELETE FROM menu_info WHERE weixin_public_no = ?";
		return jdbcTemplate.update(sql, new Object[] { wxno });
	}
	
	public MenuInfo findByPK(Integer id) {
		String sql = "SELECT * FROM menu_info m WHERE m.id = ?";
		return this.getObject(MenuInfo.class, sql, new Object[] { id });
	}
	
	public MenuInfo doFindByWxNo(String weixinNo) {
		String sql = "SELECT * FROM menu_info m WHERE m.weixin_public_no = ? ORDER BY m.create_time DESC LIMIT 1";
		return this.getObject(MenuInfo.class, sql, new Object[] { weixinNo });
	}
	
	public MenuInfo findMenuInfo(String weixinNo, String mkey) {
		String sql = "SELECT * FROM menu_info m WHERE m.weixin_public_no = ? AND mkey = ? ORDER BY m.create_time DESC LIMIT 1";
		return this.getObject(MenuInfo.class, sql, new Object[] { weixinNo, mkey });
	}
	
	public List<MenuInfo> findByWxNo(String weixinNo) {
		String sql = "SELECT * FROM menu_info m WHERE m.weixin_public_no = ?";
		return this.getListObject(MenuInfo.class, sql, new Object[] { weixinNo });
	}
	
	public List<MenuInfo> doFindMenuByWxNo(String weixinNo) {
		String sql = "SELECT m.menu_id,m.parent_menu_id,m.menu_name,m.menu_type,m.url,k.ref_text,k.reftext_image_id,k.ref_vedio_id"+
				 		" FROM menu_info m,menu_keyset k" +
				 		" WHERE m.weixin_public_no = k.weixin_public_no AND m.menu_id = k.menu_id AND m.weixin_public_no = ?";
		return this.getListObject(MenuInfo.class, sql, new Object[] { weixinNo });
	}
	
	public MenuInfo doFindByMenuId(String menuId, String wxno) {
		String sql = "SELECT * FROM menu_info WHERE menu_id = ? AND weixin_public_no = ?";
		return this.getObject(MenuInfo.class, sql, new Object[] { menuId, wxno });
	}
	
	public String doFindByUrl(String weixinNo,String url) {
		String sql = "SELECT m.menu_id FROM menu_info m WHERE m.weixin_public_no = ? AND m.url = ? LIMIT 1";
		MenuInfo menuInfo = this.getObject(MenuInfo.class, sql, new Object[] { weixinNo, url });
		return menuInfo.getMenuId().toString();
	}
	
	public List<String> findParentMenuList(String weixinNo) {
		String sql = "SELECT menu_id FROM menu_info m WHERE m.weixin_public_no = ? AND m.parent_menu_id = 0";
		return this.jdbcTemplate.queryForList(sql, String.class, new Object[] { weixinNo });
	}
	
}
