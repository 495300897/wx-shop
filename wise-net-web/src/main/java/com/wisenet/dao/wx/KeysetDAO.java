package com.wisenet.dao.wx;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.Keyset;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class KeysetDAO extends JDBCUtil {
    
	public int save(Keyset keySet) {
    	return this.insert(keySet);
	}

	public int update(Keyset keySet) {
		return this.update(keySet, "key_service_no");
	}

	public int delete(String keyServiceNo) {
		String sql = "DELETE FROM keyset WHERE id=?";
		return this.jdbcTemplate.update(sql, new Object[]{ keyServiceNo });
	}

	public Keyset findByPK(Long keyServiceNo) {
		String sql = "SELECT * FROM keyset k WHERE k.key_service_no = ?";
		return this.getObject(Keyset.class, sql, new Object[]{ keyServiceNo });
	}
	
	//随机10个电影名字
	public String findKeywords(String wxPublicNo) {
		String sql = "SELECT GROUP_CONCAT(t.key_word) FROM (SELECT key_word FROM keyset WHERE weixin_public_no = ? ORDER BY RAND() LIMIT 10) AS t;";
		return this.jdbcTemplate.queryForObject(sql, String.class, wxPublicNo);
	}
	
	public Keyset findKeySet(String WxNo, String keyWord) {
		String sql = "SELECT * FROM keyset k WHERE k.weixin_public_no = ? AND k.key_word = ?";
		return this.getObject(Keyset.class, sql, new Object[]{ WxNo, keyWord });
	}

	public List<Keyset> listKeySet(String wxPublicNo, int start, int length) {
		String sql = "SELECT * FROM keyset k WHERE k.weixin_public_no = ? ORDER BY k.create_time DESC LIMIT ?,?";
		return this.getListObject(Keyset.class, sql, new Object[]{ wxPublicNo, start, length });
	}
	
	public int getCounts(String wxPublicNo){
		String sql = "SELECT count(*) AS count FROM keyset WHERE weixin_public_no = ?";
		return this.getCounts(sql, new Object[]{ wxPublicNo });
	}

}
