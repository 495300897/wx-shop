package com.wisenet.dao.wx;

import java.util.Date;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.WechatUser;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class WechatUserDAO extends JDBCUtil {
    
    public int save(WechatUser user) {
    	user.setCreateTime(new Date());
    	return insert(user);
    }
    
    public int updateUser(WechatUser user) {
    	return update(user, "id");
    }
    
	public WechatUser findByOpenId(String openId) {
		String sql = "SELECT * FROM wechat_user WHERE open_id = ? LIMIT 1";
		return getObject(WechatUser.class, sql, new Object[] { openId });
	}
	
}
