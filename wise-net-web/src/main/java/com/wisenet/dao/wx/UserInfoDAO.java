package com.wisenet.dao.wx;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.UserInfo;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class UserInfoDAO extends JDBCUtil {
    
    public int save(UserInfo user){
    	try {
    		return insert(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return 0;
    }
    
    public int updateUser(UserInfo userInfo) {
    	return update(userInfo, "id");
    }
    
	public UserInfo findByUserIdAndType(String userid, String type) {
		String sql = "SELECT * FROM user_info WHERE userid = ? AND type = ? ORDER BY time DESC LIMIT 1";
		return getObject(UserInfo.class, sql, new Object[] { userid, type });
	}
	
	public UserInfo getUserByUserId(String userid) {
		String sql = "SELECT * FROM user_info WHERE userid = ? ORDER BY time DESC LIMIT 1";
		return getObject(UserInfo.class, sql, new Object[] { userid });
	}
	
}
