package com.wisenet.dao.wx;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.ImageTextMore;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class ImageTextMoreDAO extends JDBCUtil {
    
	public int save(ImageTextMore imageTextMore) {
		return this.insert(imageTextMore);
	}
	
	public int update(ImageTextMore imageTextMore) {
		return this.update(imageTextMore, "more_image_text_no");
	}
	
	public int delete(Long moreImageTextNo) {
		return jdbcTemplate.update("DELETE FROM image_text_more WHERE more_image_text_no = ?", new Object[]{ moreImageTextNo });
	}
	
	public ImageTextMore findByPK(Long moreImageTextNo) {
		String sql = "SELECT * FROM image_text_more i WHERE i.more_image_text_no = ?";
		 return this.getObject(ImageTextMore.class, sql, new Object[]{ moreImageTextNo });
	}
	
	public List<ImageTextMore> findImageTextMoreList(Long imageTextNo) {
		String sql = "SELECT * FROM image_text_more i WHERE i.image_text_no = ?";
    	return this.getListObject(ImageTextMore.class, sql, new Object[]{ imageTextNo });
    }
	
	public List<ImageTextMore> findSubImageText(Long imageTextNo) {
		String sql = "SELECT * FROM image_text_more i WHERE i.image_text_no = ?";
		return this.getListObject(ImageTextMore.class, sql, new Object[]{ imageTextNo });
    }
	
	public int[] batchUpdate(List<Object[]> values) {
		String sql = "DELETE FROM image_text_more WHERE id IN ?";
		return batchUpdate(sql, values);
	}
	
}
