package com.wisenet.dao.wx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.ImageTextInfo;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class ImageTextInfoDAO extends JDBCUtil {

	public int save(ImageTextInfo imageTextInfo) {
		return this.insert(imageTextInfo);
	}

	/**
	 * 保存对象并返回主键
	 * @param imageTextInfo
	 * @return
	 * @throws Exception
	 */
	public Long saveAndGetId(ImageTextInfo imageTextInfo) throws Exception {
		return this.insertAndGetId(imageTextInfo);
	}

	public int update(ImageTextInfo imageTextInfo) {
		return this.update(imageTextInfo, "image_text_no");
	}

	public int delete(Long imageTextNo) {
		String sql = "DELETE FROM image_text_info WHERE image_text_no = ?";
		return this.jdbcTemplate.update(sql, new Object[] { imageTextNo });
	}

	public ImageTextInfo findByPK(Long imageTextNo) {
		String sql = "SELECT * FROM image_text_info WHERE image_text_no = ?";
		return this.getObject(ImageTextInfo.class, sql, new Object[] { imageTextNo });
	}

	/**
	 * 图文分页
	 * @param page
	 * @param length
	 * @param wxPublicNo
	 * @return
	 */
	public List<ImageTextInfo> findImageTextList(int start, int length, String wxPublicNo) {
		String sql = "SELECT * FROM image_text_info WHERE weixin_public_no = ? ORDER BY create_time DESC LIMIT ?, ?";
		return this.getListObject(ImageTextInfo.class, sql, new Object[] { wxPublicNo, start, length });
	}

	public int findImageTextCount(String wxPublicNo) {
		String sql = "SELECT count(image_text_no) AS count FROM image_text_info WHERE weixin_public_no = ?";
		return this.getCounts(sql, new Object[] { wxPublicNo });
	}

	public ImageTextInfo findByPKNoContent(String imageTextNo) {
		String sql = "SELECT * FROM imagetextinfo WHERE imagetextno = ?";
		return this.getObject(ImageTextInfo.class, sql, new Object[] { imageTextNo });
	}

	public List<Map<String, Object>> doFindImageText4Select(Map<String, Object> params) throws Exception {
		String wxPublicNo = (String) params.get("wxPublicNo");
		List<Map<String, Object>> imageTextList = new ArrayList<Map<String, Object>>();
		List<ImageTextInfo> list = findByWxPublicNo(wxPublicNo);
		for (ImageTextInfo info : list) {
			Map<String, Object> tempMap = new HashMap<String, Object>();
			tempMap.put("rid", info.getImageTextNo());
			tempMap.put("title", info.getTitle());
			imageTextList.add(tempMap);
		}
		return imageTextList;
	}

	public List<ImageTextInfo> findByWxPublicNo(String wxPublicNo) {
		String sql = "SELECT * FROM image_text_info WHERE weixin_public_no = ? ORDER BY create_time DESC LIMIT 1";
		return this.getListObject(ImageTextInfo.class, sql, new Object[] { wxPublicNo });
	}

	// @Cacheable(value="myCache", key="#title")
	public ImageTextInfo findByTitle(String title) {
		String sql = "SELECT title,image_url,click_out_url FROM image_text_info WHERE title LIKE ? ORDER BY RAND() LIMIT 1";
		return this.getObject(ImageTextInfo.class, sql, new Object[] { "%" + title + "%" });
	}

	/**
	 * 随机10个电影名字
	 * @param wxPublicNo
	 * @return
	 */
	public String findTitles(String wxPublicNo) {
		String sql = "SELECT GROUP_CONCAT(t.title) FROM (SELECT title FROM image_text_info WHERE weixin_public_no = ? ORDER BY RAND() LIMIT 10) AS t";
		return this.jdbcTemplate.queryForObject(sql, String.class, wxPublicNo);
	}

	public List<ImageTextInfo> findRankList(int start, int length, String selectType) {
		String sql = "SELECT * FROM image_text_info ORDER BY RAND() LIMIT ?, ?";
		if ("0".equals(selectType)) {
			sql = "SELECT * FROM image_text_info ORDER BY create_time DESC LIMIT ?, ?";
		} else if ("1".equals(selectType)) {
			sql = "SELECT * FROM image_text_info ORDER BY click_url DESC LIMIT ?, ?";
		} else if ("2".equals(selectType)) {
			sql = "SELECT * FROM image_text_info ORDER BY operator DESC LIMIT ?, ?";
		}
		return this.getListObject(ImageTextInfo.class, sql, new Object[] { start, length });
	}

	public List<ImageTextInfo> findMovieList(String title) {
		String sql = "SELECT title,click_out_url,image_url,digest,main_text,operator FROM image_text_info WHERE title LIKE ?";
		return this.getListObject(ImageTextInfo.class, sql, new Object[] { "%" + title + "%" });
	}

	public int getAllCount() {
		return this.getCounts("SELECT count(1) AS count FROM image_text_info", new Object[] {});
	}

}
