package com.wisenet.dao.wx;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.wisenet.entity.WeixinPubno;
import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class WeiXinPubNoDAO extends JDBCUtil {
	
	public void save(WeixinPubno weiXinPubNo) {
    	try {
    		insert(weiXinPubNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void update(WeixinPubno weiXinPubNo) {
    	try {
    		this.update(weiXinPubNo, "id");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<WeixinPubno> getList(int start, int length, String uid) {
		String sql = "SELECT * FROM weixin_pubno WHERE operator = ? ORDER BY create_time DESC LIMIT ?,?";
		return this.getListObject(WeixinPubno.class, sql, new Object[] { uid, start, length });
	}
	
	public List<WeixinPubno> getList(String uid, String pubnoType) {
		String sql = "SELECT * FROM weixin_pubno WHERE operator = ? AND pubno_type = ?";
		return this.getListObject(WeixinPubno.class, sql, new Object[] { uid, pubnoType });
	}
	
	public List<WeixinPubno> getList(String uid) {
		String sql = "SELECT * FROM weixin_pubno WHERE operator = ?";
		return this.getListObject(WeixinPubno.class, sql, new Object[] { uid });
	}
	
	public WeixinPubno findById(int id) {
		String sql = "SELECT * FROM weixin_pubno WHERE id = ?";
		return this.getObject(WeixinPubno.class, sql, new Object[] { id });
	}
	
	public WeixinPubno findByAppid(String appid) {
		String sql = "SELECT * FROM weixin_pubno WHERE appid = ?";
		return this.getObject(WeixinPubno.class, sql, new Object[] { appid });
	}
	
	public WeixinPubno findByWxNo(String wxNo) {
		String sql = "SELECT * FROM weixin_pubno WHERE pubno = ?";
		return this.getObject(WeixinPubno.class, sql, new Object[] { wxNo });
	}
	
	public WeixinPubno findByOperator(String operator) {
		String sql = "SELECT * FROM weixin_pubno WHERE operator = ?";
		return this.getObject(WeixinPubno.class, sql, new Object[] { operator });
	}
	
}
