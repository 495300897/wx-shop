package com.wisenet.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.wisenet.jdbctemplate.JDBCUtil;

@Repository
public class GeneratorDAO extends JDBCUtil {

	public Map<String, Object> getTable(String tableName) {
		String sql = "SELECT table_name tableName, engine, table_comment tableComment, create_time createTime FROM information_schema.tables"
				   + " WHERE table_schema = (SELECT DATABASE()) AND table_name = ?";
		return jdbcTemplate.queryForMap(sql, new Object[] { tableName });
	}
	
	public List<Map<String, Object>> listColumns(String tableName) {
		String sql = "SELECT column_name columnName, data_type dataType, column_comment columnComment, column_key columnKey, extra FROM information_schema.columns"
				   + " WHERE table_name = ? AND table_schema = (SELECT DATABASE()) ORDER BY ordinal_position";
		return jdbcTemplate.queryForList(sql, new Object[] { tableName });
	}
	
}
