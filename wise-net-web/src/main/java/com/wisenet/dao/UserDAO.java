package com.wisenet.dao;

import com.wisenet.entity.User;
import com.wisenet.jdbctemplate.JDBCUtil;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO extends JDBCUtil {
    
    public int save(User user) {
    	try {
    		return insert(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return 0;
    }
    
    public int updateUser(User user) {
    	return update(user, "id");
    }
    
	public User findByUsernameAndPwd(String username, String password) {
    	String sql = "SELECT * FROM user u WHERE u.username = ? AND u.password = ? ORDER BY id LIMIT 1";
		try {
    		return getObject(User.class, sql, new Object[] { username, password });
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
	}
	
	public User findByUsername(String username) {
		try {
			String sql = "SELECT * FROM user u WHERE u.username = ? ORDER BY id LIMIT 1";
    		return getObject(User.class, sql, new Object[] { username });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public User findByOpenId(String openId) {
		try {
			String sql = "SELECT * FROM user WHERE openid = ? ORDER BY id LIMIT 1";
    		return getObject(User.class, sql, new Object[] { openId });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
