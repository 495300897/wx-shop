package com.wisenet.service.wx;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.common.Const;
import com.wisenet.dao.wx.ImageTextInfoDAO;
import com.wisenet.dao.wx.ImageTextMoreDAO;
import com.wisenet.entity.ImageTextInfo;

@Service
public class ImageTextInfoService {
	@Autowired
	private ImageTextInfoDAO imageTextInfoDAO;
	@Autowired
	private ImageTextMoreDAO imageTextMoreDAO;

	public int save(ImageTextInfo imageTextInfo) {
		return imageTextInfoDAO.save(imageTextInfo);
	}

	public void delete(Long imageTextNo) {
		imageTextInfoDAO.delete(imageTextNo);
	}

	public ImageTextInfo findByPK(Long imageTextNo) {
		return imageTextInfoDAO.findByPK(imageTextNo);
	}

	public ImageTextInfo findByTitle(String title) {
		return imageTextInfoDAO.findByTitle(title);
	}

	public ImageTextInfo findImageText(Long imageTextNo) throws Exception {
		ImageTextInfo mainVo = imageTextInfoDAO.findByPK(imageTextNo);
		if (mainVo == null)
			return null;
		if (Const.MESSAGE_TYPE_IMAGE.equals(mainVo.getImageTextType())) {
			mainVo.setSubVoList(imageTextMoreDAO.findImageTextMoreList(mainVo.getImageTextNo()));
		}
		return mainVo;
	}

	public List<ImageTextInfo> findImageTextList(int start, int length, String wxPublicNo) {
		List<ImageTextInfo> imageTextList = imageTextInfoDAO.findImageTextList(start, length, wxPublicNo);
		for (ImageTextInfo imgtxtInfo : imageTextList) {
			// 多图文
			if (Const.MESSAGE_TYPE_IMAGE.equals(imgtxtInfo.getImageTextType())) {
				imgtxtInfo.setSubVoList(imageTextMoreDAO.findSubImageText(imgtxtInfo.getImageTextNo()));
			}
		}
		return imageTextList;
	}

	public int findImageTextCount(String wxPublicNo) throws Exception {
		return imageTextInfoDAO.findImageTextCount(wxPublicNo);
	}

	public List<Map<String, Object>> doFindImageText4Select(Map<String, Object> params) throws Exception {
		return imageTextInfoDAO.doFindImageText4Select(params);
	}

	public int update(ImageTextInfo imageTextInfo) {
		return imageTextInfoDAO.update(imageTextInfo);
	}

	/**
	 * 更新次数
	 * @param imageTextNo
	 * @param type 1点赞，2分享
	 * @return
	 */
	public String updateCount(Long imageTextNo, String type) throws Exception {
		ImageTextInfo imageTextInfo = findByPK(imageTextNo);
		if (imageTextInfo == null) {
			return "榜单不存在";
		}
		// 只更新部分字段
		ImageTextInfo textInfo = new ImageTextInfo();
		textInfo.setImageTextNo(imageTextNo);
		// 1点赞
		if (Const.MESSAGE_TYPE_TEXT.equals(type)) {
			Integer clickCount = Integer.parseInt(imageTextInfo.getClickUrl()) + 1;
			textInfo.setClickUrl(clickCount.toString());
		}
		// 2分享
		if (Const.MESSAGE_TYPE_IMAGE.equals(type)) {
			Integer shareCount = Integer.parseInt(imageTextInfo.getOperator()) + 1;
			textInfo.setOperator(shareCount.toString());
		}
		
		// 更新次数
		update(textInfo);
		
		return "success";
	}

	public List<ImageTextInfo> findRankList(int start, int length, String selectType) {
		return imageTextInfoDAO.findRankList(start, length, selectType);
	}

	public int getMoviePage(int pageCount) throws Exception {
		int rows = imageTextInfoDAO.getAllCount();
		int page = (rows + pageCount - 1) / pageCount;
		return page;
	}
}
