package com.wisenet.service.wx;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.common.Const;
import com.wisenet.dao.wx.UserInfoDAO;
import com.wisenet.entity.UserInfo;

@Service
public class UserInfoService {
	@Autowired
	private UserInfoDAO userInfoDAO;
	public static Logger logger = Logger.getLogger(UserInfoService.class);

	public int updateUserInfo(UserInfo userInfo) {
		return userInfoDAO.update(userInfo, "id");
	}
	
	public UserInfo findByUserIdAndType(String userid, String type) {
		return userInfoDAO.findByUserIdAndType(userid, type);
	}
	
	public UserInfo findByUserId(String userid) {
		return userInfoDAO.getUserByUserId(userid);
	}
	
	/**
	 * 保存猜图游戏
	 * @param userid
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public int saveIdsByUserIdAndType(String userid, String type) {
		UserInfo userInfo = findByUserIdAndType(userid, type);
		if (userInfo == null) {
			logger.error(String.format("findByUserIdAndType: %s %s 用户不存在", userid, type));
			return 0;
		}
		try {
			String ids = userInfo.getIds() == null ? Const.GAME_NUM_DEFAULT.toString() : userInfo.getIds();
			Integer num = Integer.parseInt(ids) + 1;
			userInfo.setIds(num.toString());
			if (Const.GUESS_CY_COUNT.equals(num) || Const.GUESS_IMG_COUNT.equals(num)) {
				userInfo.setIsend(Const.GAME_NUM_DEFAULT);
			}
			return updateUserInfo(userInfo);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return 0;
	}
}
