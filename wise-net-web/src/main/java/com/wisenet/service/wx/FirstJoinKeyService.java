package com.wisenet.service.wx;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.wx.FirstJoinKeyDAO;
import com.wisenet.entity.FirstJoinKey;
import com.wisenet.entity.User;

@Service
public class FirstJoinKeyService {
	@Autowired
	private FirstJoinKeyDAO firstJoinKeyDAO;

	public int saveFirstJoinKey(FirstJoinKey firstJoinKey, User user) {
		FirstJoinKey firstjoinkey = findByWxNo(user.getWxpublicno());
		if(firstjoinkey != null) {
			if(firstJoinKey.getReText() != null) {
				firstJoinKey.setReText(firstJoinKey.getReText().trim().replace("&lt;", "<") .replace("&gt;", ">"));
			}
			return firstJoinKeyDAO.update(firstJoinKey, "id");
		}
		firstJoinKey.setCreateTime(new Date());
		firstJoinKey.setCreateUser(user.getUsername());
		firstJoinKey.setOperatorName(user.getUsername());
		firstJoinKey.setWeixinPublicNo(user.getWxpublicno());
		return firstJoinKeyDAO.save(firstJoinKey);
	}

	public void update(FirstJoinKey firstJoinKey) {
		firstJoinKeyDAO.update(firstJoinKey, "id");
	}

	public void delete(int id) {
		firstJoinKeyDAO.delete(id);
	}

	public FirstJoinKey findByWxNo(String WxNo) {
		return firstJoinKeyDAO.findByWxNo(WxNo);
	}

}
