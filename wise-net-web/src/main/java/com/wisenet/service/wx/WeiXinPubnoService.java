package com.wisenet.service.wx;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.wisenet.dao.wx.WeiXinPubNoDAO;
import com.wisenet.entity.WeixinPubno;
import com.wisenet.wx.util.SignUtil;

@Service
public class WeiXinPubnoService {
	@Value("${wx_interface_url}")
	private String wxInterfaceUrl;
	@Autowired
	private WeiXinPubNoDAO weixinPubnoDAO;

	public WeixinPubno save(WeixinPubno weixinPubno) {
		if ("pubno".equals(weixinPubno.getPubnoType())) {
			wxInterfaceUrl += SignUtil.encode(weixinPubno.getPubno() + "|" + weixinPubno.getToken());
			weixinPubno.setInterfaceUrl(wxInterfaceUrl);
		}
		weixinPubno.setCreateTime(new Date());
		weixinPubnoDAO.save(weixinPubno);
		return weixinPubno;
	}
	
	public WeixinPubno findByAppid(String appid) {
		return weixinPubnoDAO.findByAppid(appid);
	}
	
	public WeixinPubno findByOperator(String operator) {
		return weixinPubnoDAO.findByOperator(operator);
	}
	
	public void update(WeixinPubno weixinPubno) {
		weixinPubno.setCreateTime(new Date());
		weixinPubnoDAO.update(weixinPubno);
	}

	public WeixinPubno findById(int id) {
		return weixinPubnoDAO.findById(id);
	}
	
	public WeixinPubno findByWxNo(String wxpublicno) {
		return weixinPubnoDAO.findByWxNo(wxpublicno);
	}

	public List<WeixinPubno> getList(int start, int length, String uid) {
		return weixinPubnoDAO.getList(start, length, uid);
	}
	
	public List<WeixinPubno> getList(String uid, String pubnoType) {
		return weixinPubnoDAO.getList(uid, pubnoType);
	}
}
