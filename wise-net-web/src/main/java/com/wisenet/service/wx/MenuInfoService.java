package com.wisenet.service.wx;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.wx.MenuInfoDAO;
import com.wisenet.entity.MenuInfo;

@Service
public class MenuInfoService {
	@Autowired
	private MenuInfoDAO menuInfoDAO;
	
	/**
	 * 保存或修改菜单
	 * @param menuInfo
	 * @return
	 * @throws Exception 
	 */
	public int saveOrUpdateMenu(MenuInfo menuInfo) throws Exception {
		if (menuInfo.getId() == null) {
			return menuInfoDAO.save(menuInfo);
		}
		return menuInfoDAO.update(menuInfo, "id");
	}
	
	public void save(MenuInfo menuInfo) throws Exception {
		menuInfoDAO.save(menuInfo);
	}
	
	public MenuInfo findByPk(Integer id) throws Exception {
		return menuInfoDAO.findByPK(id);
	}
	
	public void update(MenuInfo menuInfo) {
		menuInfoDAO.update(menuInfo, "id");
	}
	
	public int delete(Integer id) {
		return menuInfoDAO.deleteMenuInfo(id);
	}
	
	public void deleteByWxNo(String wxno) {
		menuInfoDAO.deleteByWxNo(wxno);
	}
	
	public List<MenuInfo> doFindMenuByWxNo(String wxNo) {
		return menuInfoDAO.doFindMenuByWxNo(wxNo);
	}
	
	public MenuInfo doFindByMenuId(String menuId, String wxno) {
		return menuInfoDAO.doFindByMenuId(menuId, wxno);
	}
	
	public MenuInfo doFindByWxNo(String wxNo) throws Exception {
		return menuInfoDAO.doFindByWxNo(wxNo);
	}
	
	public List<MenuInfo> findByWxNo(String wxNo) {
		return menuInfoDAO.findByWxNo(wxNo);
	}
	
	public String doFindByUrl(String wxno, String url) {
		return menuInfoDAO.doFindByUrl(wxno, url);
	}
	
	public List<String> findParentMenuList(String wxno) {
		return menuInfoDAO.findParentMenuList(wxno);
	}
	
}
