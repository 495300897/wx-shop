package com.wisenet.service.wx;

import com.wisenet.dao.UserDAO;
import com.wisenet.entity.User;
import com.wisenet.util.VeDate;
import com.wisenet.util.encrypt.TjrAES;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserService {
	@Autowired
	private UserDAO userDAO;

	public User findByUsernameAndPwd(String username, String password) throws Exception {
		return userDAO.findByUsernameAndPwd(username, TjrAES.encrypt(password));
	}
	
	public int saveUser(User user) throws Exception {
		user.setCreateTime(VeDate.dateToString(new Date()));
		user.setPassword(TjrAES.encrypt(user.getPassword()));
		return userDAO.insert(user);
	}
	
	public int updateUser(User user) throws Exception {
		return userDAO.update(user, "id");
	}
	
	public User findByUsername(String username) {
		return userDAO.findByUsername(username);
	}
	
	
}
