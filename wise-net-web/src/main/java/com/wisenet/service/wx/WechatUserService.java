package com.wisenet.service.wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.wx.WechatUserDAO;
import com.wisenet.entity.WechatUser;
import com.wisenet.wx.util.WeixinUtil;

@Service
public class WechatUserService {
	@Autowired
	private WechatUserDAO userDAO;

	public int updateUser(WechatUser user) {
		return userDAO.update(user, "id");
	}
	
	public void saveUser(String token, String openId, String remark) {
		WechatUser user = findByOpenId(openId);
		if (user == null) {
			user = WeixinUtil.getUserInfo(token, openId);
			if (user != null) {
				user.setRemark(remark);
				saveUser(user);
			}
		}
	}
	
	public int saveUser(WechatUser user) {
		return userDAO.save(user);
	}
	
	public WechatUser findByOpenId(String openId) {
		return userDAO.findByOpenId(openId);
	}
}
