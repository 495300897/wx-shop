package com.wisenet.service.wx;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.wisenet.common.WxConst;
import com.wisenet.wx.util.WeixinUtil;

@Service
public class QrcodeService {

	/**
	 * 获取二维码
	 * @param token
	 * @param scene
	 * @param page
	 * @return
	 */
	public JSONObject getQrcode(String token, String scene, String page) {
		JSONObject param = new JSONObject();
		param.put("page", page);
		param.put("scene", scene);
		String requestUrl = String.format(WxConst.MIN_APP_QRCODE_URL, token);
		return WeixinUtil.httpRequest(requestUrl, "POST", param);
	}
}
