package com.wisenet.service.wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.wx.JssdkConfigDAO;
import com.wisenet.entity.JssdkConfig;

@Service
public class JssdkConfigService {
	@Autowired
	private JssdkConfigDAO jssdkConfigDAO;

	public int save(JssdkConfig jssdkConfig) {
		jssdkConfig.setCreateTime(System.currentTimeMillis());
		return jssdkConfigDAO.save(jssdkConfig);
	}


	
}
