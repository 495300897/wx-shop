package com.wisenet.service.sportwear;

import com.wisenet.dao.sportwear.SportWearOrderDAO;
import com.wisenet.entity.SportWearOrder;
import com.wisenet.util.VeDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SportWearOrderService {
	@Autowired
	private SportWearOrderDAO sportWearOrderDAO;

	public int save(SportWearOrder sportWearOrder) {
		sportWearOrder.setCreateTime(VeDate.dateToString(new Date()));
		return sportWearOrderDAO.insert(sportWearOrder);
	}

	public int cancelOrder(SportWearOrder orderParam) {
		// TODO 发送模板消息
		return sportWearOrderDAO.cancelOrder(orderParam.getId(), orderParam.getUid());
	}
	
	public int updateOrderStatus(int id, int status, String uid, String paymsg) {
		return sportWearOrderDAO.updateOrderStatus(id, status, uid, paymsg);
	}
	
	public int updateOrderDelivery(SportWearOrder param) {
		return sportWearOrderDAO.updateOrderDelivery(param);
	}
	
	public int update(SportWearOrder sportWearOrder) {
		sportWearOrder.setCreateTime(VeDate.dateToString(new Date()));
		return sportWearOrderDAO.update(sportWearOrder, "id");
	}
	
	public List<SportWearOrder> findByUid(String uid) {
		return sportWearOrderDAO.findByUid(uid);
	}
	
	public SportWearOrder findById(int id) {
		return sportWearOrderDAO.findById(id);
	}
	
	public SportWearOrder findByOrderId(String outTradeNo) {
		return sportWearOrderDAO.findByOutTradeNo(outTradeNo);
	}

	public SportWearOrder findByUidAndOutTradeNo(String openid, String outTradeNo) {
		return sportWearOrderDAO.findByUidAndOutTradeNo(openid, outTradeNo);
	}

	public List<SportWearOrder> findListPage(SportWearOrder order, int start, int length) {
		return sportWearOrderDAO.selectListByDynamicSql(order, "create_time DESC", "eq", start, length);
	}
	

}
