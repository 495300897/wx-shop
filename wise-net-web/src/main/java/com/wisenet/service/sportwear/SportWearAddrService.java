package com.wisenet.service.sportwear;

import java.util.Date;
import java.util.List;

import com.wisenet.util.VeDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.sportwear.SportWearAddrDAO;
import com.wisenet.entity.SportWearAddr;

@Service
public class SportWearAddrService {
	@Autowired
	private SportWearAddrDAO sportWearAddrDAO;

	public long save(SportWearAddr sportWearAddr) {
		sportWearAddr.setCreateTime(VeDate.dateToString(new Date()));
		try {
			return sportWearAddrDAO.insertAndGetId(sportWearAddr);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void update(SportWearAddr sportWearAddr) {
		sportWearAddrDAO.update(sportWearAddr, "id");
	}
	
	public SportWearAddr findById(int id) {
		return sportWearAddrDAO.findById(id);
	}
	
	public List<SportWearAddr> findByUid(String uid) {
		return sportWearAddrDAO.findByUid(uid);
	}


}
