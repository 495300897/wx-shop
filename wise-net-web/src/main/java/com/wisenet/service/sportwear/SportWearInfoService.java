package com.wisenet.service.sportwear;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.sportwear.SportWearInfoDAO;
import com.wisenet.entity.SportWearInfo;

@Service
public class SportWearInfoService {
	@Autowired
	private SportWearInfoDAO sportWearInfoDAO;

	public int save(SportWearInfo sportWearInfo) {
		return sportWearInfoDAO.insert(sportWearInfo);
	}

	public void update(SportWearInfo sportWearInfo) {
		sportWearInfoDAO.update(sportWearInfo, "id");
	}
	
	public SportWearInfo findById(int id) {
		return sportWearInfoDAO.findById(id);
	}
	
	public List<SportWearInfo> findListByWearId(int wearId) {
		return sportWearInfoDAO.findListByWearId(wearId);
	}


}
