package com.wisenet.service.sportwear;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.sportwear.SportWearDAO;
import com.wisenet.entity.SportWear;

@Service
public class SportWearService {
	@Autowired
	private SportWearDAO sportWearDAO;

	public int save(SportWear sportWear) {
		return sportWearDAO.insert(sportWear);
	}

	public void update(SportWear sportWear) {
		sportWearDAO.update(sportWear, "id");
	}
	
	public SportWear findById(int id) {
		return sportWearDAO.findById(id);
	}
	
	public List<SportWear> findByType(String wearType) {
		return sportWearDAO.findByType(wearType);
	}
	
	public List<SportWear> findByTitle(String title) {
		return sportWearDAO.findByTitle(title);
	}
	
	public List<SportWear> findList(String title, int start, int length) {
		return sportWearDAO.findList(title, start, length);
	}


}
