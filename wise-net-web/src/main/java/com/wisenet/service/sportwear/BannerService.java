package com.wisenet.service.sportwear;

import com.wisenet.dao.sportwear.BannerDAO;
import com.wisenet.entity.Banner;
import com.wisenet.util.VeDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BannerService {
	@Autowired
	private BannerDAO bannerDAO;

	public int save(Banner banner) {
		banner.setCreateTime(VeDate.dateToString(new Date()));
		return bannerDAO.insert(banner);
	}

	public void update(Banner banner) {
		banner.setUpdateTime(VeDate.dateToString(new Date()));
		bannerDAO.update(banner, "id");
	}
	
	public Banner findById(Integer id) {
		return bannerDAO.findById(id);
	}
	
	/**
	 * 列表查询，根据序号降序排序
	 * @param status
	 * @return
	 */
	public List<Banner> findList(Integer status) {
		return bannerDAO.findList(status);
	}

}
