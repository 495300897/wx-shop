package com.wisenet.service.sportwear;

import com.wisenet.dao.sportwear.SportWearUserDAO;
import com.wisenet.entity.SportWearUser;
import com.wisenet.util.VeDate;
import com.wisenet.wx.util.QQFace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SportWearUserService {
	@Autowired
	private SportWearUserDAO sportWearUserDAO;

	public long save(SportWearUser sportWearUser) {
		sportWearUser.setCreateTime(VeDate.dateToString(new Date()));
		try {
			return sportWearUserDAO.insertAndGetId(sportWearUser);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public int update(SportWearUser sportWearUser) {
		sportWearUser.setCreateTime(VeDate.dateToString(new Date()));
		sportWearUser.setNickName(QQFace.filterEmoji(sportWearUser.getNickName()));
		return sportWearUserDAO.update(sportWearUser, "id");
	}
	
	public SportWearUser findByUid(String uid) {
		return sportWearUserDAO.findByUid(uid);
	}


}
