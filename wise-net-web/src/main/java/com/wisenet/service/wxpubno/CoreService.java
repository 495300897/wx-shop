package com.wisenet.service.wxpubno;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.wx.ImageTextInfoDAO;
import com.wisenet.dao.wx.ImageTextMoreDAO;
import com.wisenet.dao.wx.KeysetDAO;
import com.wisenet.dao.wx.MessageInfoDAO;
import com.wisenet.entity.ImageTextInfo;
import com.wisenet.entity.ImageTextMore;
import com.wisenet.entity.Keyset;
import com.wisenet.entity.MessageInfo;
import com.wisenet.wx.util.MessageManager;
import com.wisenet.wx.util.MessageUtil;
import com.wisenet.wx.util.QQFace;
import com.wisenet.wx.util.StringUtil;

/**
 * 核心服务类
 * @author fzh
 */
@Service
public class CoreService {
	@Autowired
	private KeysetDAO keysetDAO;
	@Autowired
	private MessageInfoDAO messageInfoDAO;
	@Autowired
	private ImageTextInfoDAO imageTextInfoDAO;
	@Autowired
	private ImageTextMoreDAO imageTextMoreDAO;
	private String toUserName = null;
	private String fromUserName = null;
	private String respContent =  null;
	private String inputContent = null;
	
	public void init(Map<String, String> requestMap) {
		// 用户输入内容
		inputContent = requestMap.get("Content");
		// 公众帐号
		toUserName = requestMap.get("ToUserName");
		// 发送方帐号(open_id)
		fromUserName = requestMap.get("FromUserName");
	}
	
	public String operateTextMessage(Map<String, String> requestMap) throws Exception {
		// 保存用户消息记录
		saveMessageInfo(requestMap, null, null, null, "0");
		// 回复表情
		if (QQFace.isQqFace(inputContent)) {
			respContent = inputContent;
		}
		// 获取百度天气
		else if ((inputContent.startsWith("天气") || inputContent.endsWith("天气"))
				&& inputContent.length() >= 4) {
			respContent = ApiService.getWeather(inputContent);
		}
		// 回复电影图文
		else if ((inputContent.startsWith("电影")) && inputContent.length() > 2) {
			requestMap.put("name", inputContent.replace("电影", "").replace("+", ""));
			return null;
		} else {
			//查询关键字回复（优先于其它上下行记录回复-20130623）
			respContent = replyMessageByKeyword(requestMap);
			return respContent != null ? respContent : replyMessageByRand(requestMap);
		}
		return MessageUtil.replyTextMessage(fromUserName, toUserName, respContent);
	}
	
	private void saveMessageInfo(Map<String, String> requestMap, Long imageTextNo, String remsgType, String url, String keyword){
		MessageInfo messageInfo = MessageManager.createMessageInfo(requestMap, imageTextNo, remsgType, url, keyword);
		messageInfoDAO.save(messageInfo);
	}
	
	private String replyMessageByKeyword(Map<String, String> requestMap) throws Exception {
		Keyset keySet = keysetDAO.findKeySet(toUserName, inputContent);
		if(keySet != null){
			//保存消息记录
			saveMessageInfo(requestMap, null, null, null, "1");
			//文本
			if("1".equals(keySet.getReType())){
				return MessageUtil.replyTextMessage(fromUserName, toUserName, keySet.getRefText());
			}
			//图文
			if("2".equals(keySet.getReType())){
				ImageTextInfo imageTextInfo = imageTextInfoDAO.findByPK(keySet.getRefImageTextId());
				if(imageTextInfo == null) return null;
				List<ImageTextMore> imagetextMoreList = null;
				if("2".equals(imageTextInfo.getImageTextType())){
					imagetextMoreList = imageTextMoreDAO.findImageTextMoreList(imageTextInfo.getImageTextNo());
				}
				return MessageManager.getImageTextInfo(requestMap, imageTextInfo, imagetextMoreList);
			}
		}
		return null;
	}
	
	public String replyMusicMessage(String input) throws Exception {
		return null; 
	}
	
	/**
	 * 处理图片消息
	 * @param picUrl
	 * @return
	 */
	public String operateImageMessage(String picUrl){
        // 人脸检测
        return null;
	}
	
	/**
	 * 处理音频消息
	 * @param recognition
	 * @return
	 * @throws Exception 
	 */
	public String operateVoiceMessage(String recognition) throws Exception {
		respContent = ApiService.talk(recognition) + "\n[撇嘴]我暂时还识别不了您说的内容！";
		return MessageUtil.replyTextMessage(fromUserName, toUserName, respContent); 
	}
	
	/**
	 * 随机回复消息
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	public String replyMessageByRand(Map<String, String> requestMap) throws Exception {
		int num = StringUtil.getRandNum(100);

		if(num <= 33){
			return replyMusicMessage(inputContent);
		}
		if(num > 33 && num < 66){
			inputContent = ApiService.talk(inputContent);
			return MessageUtil.replyTextMessage(fromUserName, toUserName, respContent);
		}
		requestMap.put("name", inputContent);
		return null;
	}
	
}

