package com.wisenet.service.wxpubno;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.wisenet.util.VeDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisenet.dao.UserDAO;
import com.wisenet.dao.wx.ImageTextInfoDAO;
import com.wisenet.dao.wx.ImageTextMoreDAO;
import com.wisenet.dao.wx.MenuInfoDAO;
import com.wisenet.dao.wx.MessageInfoDAO;
import com.wisenet.entity.ImageTextInfo;
import com.wisenet.entity.ImageTextMore;
import com.wisenet.entity.MenuInfo;
import com.wisenet.entity.MessageInfo;
import com.wisenet.entity.User;
import com.wisenet.wx.util.MessageManager;
import com.wisenet.wx.util.MessageUtil;

/**
 * 事件服务类
 * @author fzh
 */
@Service
public class CoreEventService {
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private MenuInfoDAO menuInfoDAO;
	@Autowired
	private MessageInfoDAO messageInfoDAO;
	@Autowired
	private ImageTextInfoDAO imageTextInfoDAO;
	@Autowired
	private ImageTextMoreDAO imageTextMoreDAO;
	
	private String toUserName = null;
	private String fromUserName = null;
	private String respContent =  null;
	
	public void init(Map<String, String> requestMap) {
		toUserName = requestMap.get("ToUserName");	  // 公众帐号
		fromUserName = requestMap.get("FromUserName");// 发送方帐号(open_id)
	}
	
	public String operateEventMessage(Map<String, String> requestMap) throws Exception {
		String eventType = requestMap.get("Event").trim();
		// 自定义菜单点击事件
		if (eventType.equals(MessageUtil.EVENT_TYPE_CLICK)) {
			return replyMessageByClickMenu(requestMap);
		}
		//已关注用户扫描二维码
		if(eventType.equals(MessageUtil.EVENT_TYPE_SCAN)){
			return scanQrCodeEvent();
		}
		// 订阅
		if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {
			subscribeEvent();
		}
		// 取消订阅
		if (eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {
			// 取消订阅后用户再收不到公众号发送的消息,因此不需要回复消息
			unsubscribeEvent();
		}
		return null;
	}
	
	public String replyMessageByClickMenu(Map<String, String> requestMap) throws Exception {
		// 事件KEY值,与创建自定义菜单时指定的KEY值对应
		String eventKey = requestMap.get("EventKey").trim();
		String menuId = null;
        Logger.getAnonymousLogger().info("eventKey = " + eventKey);
        if("VIEW".equals(requestMap.get("Event"))){
        	menuId = menuInfoDAO.doFindByUrl(toUserName, eventKey);
		}
        //保存消息记录
        saveMessageInfo(requestMap, null, null, menuId, "0");
		MenuInfo menu = menuInfoDAO.doFindByMenuId(toUserName, eventKey);
		if(menu != null) {
			// 文本
			if ("click_text".equals(menu.getType())) {
				respContent = menu.getRefText();
				return MessageUtil.replyTextMessage(fromUserName, toUserName, respContent);
			}
			// 图文
			if ("click_imgtxt".equals(menu.getType())) {
				ImageTextInfo imageTextInfo = imageTextInfoDAO.findByPK(menu.getRefImageTextNo());
				if (imageTextInfo == null) return null;
				List<ImageTextMore> imagetextMoreList = null;
				if ("2".equals(imageTextInfo.getImageTextType())) {
					imagetextMoreList = imageTextMoreDAO.findImageTextMoreList(imageTextInfo.getImageTextNo());
				}
				return MessageManager.getImageTextInfo(requestMap, imageTextInfo, imagetextMoreList);
			}
		}
		return null;
	}
	
	/**
	 * 扫描二维码事件
	 * @return
	 */
	private String scanQrCodeEvent(){

//		String senceid = requestMap.get("EventKey");
//		String url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket="+requestMap.get("Ticket");
//		respContent = senceid+"\n<a href=\""+url+"\">查看二维码</a>";
		return null;
	}
	
	private void saveMessageInfo(Map<String, String> requestMap, Long imageTextNo, String remsgType, String url, String keyword){
		MessageInfo messageInfo = MessageManager.createMessageInfo(requestMap, imageTextNo, remsgType, url, keyword);
		messageInfoDAO.save(messageInfo);
	}
	
	/**
	 * 用户订阅事件
	 * @return
	 */
	private void subscribeEvent(){
		User user = userDAO.findByOpenId(fromUserName);
		if(user == null){
			user = new User();
			user.setOpenid(fromUserName);
			user.setWxpublicno(toUserName);
			user.setCreateTime(VeDate.dateToString(new Date()));
			userDAO.save(user);
		}
	}
	
	/**
	 * 用户取消订阅事件
	 * @return
	 */
	private String unsubscribeEvent(){
		return null;
	}
	
}
