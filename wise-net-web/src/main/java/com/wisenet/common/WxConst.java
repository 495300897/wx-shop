package com.wisenet.common;

/**
 * 常量公共类
 * @author fzh
 *
 */
public class WxConst {
	/**菜单创建接口地址 限100次/天(POST)*/
	public static final String MENU_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	
	/**个性化菜单创建接口地址 (POST)*/
	public static final String CONDITIONAL_MENU_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=";
	
	/**获取access_token的接口地址(GET) 限200次/天*/
	public static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
	
	/**获取jsapi_ticket的接口地址(GET)*/
	public static final String JSAPI_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi";
	
	/**创建二维码access_token接口地址(POST)*/
	public static final String CREATE_QRCODE_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN";
	
	/**显示二维码地址*/
	public static final String SHOW_QRCODE_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";
	
	/**通过code换取网页授权access_token接口(GET)*/
	public static final String OAUTH2_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";
	
	/**网页授权地址(scope为snsapi_base和snsapi_userinfo)*/
	public static final String OAUTH2_AUTHORIZE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=STATE#wechat_redirect";
	public static final String OAUTH2_SCOPE_BASE = "snsapi_base";
	public static final String OAUTH2_SCOPE_USERINFO = "snsapi_userinfo";
	
	/**获取用户信息接口*/
	public static final String USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";
	
	/**获取临时素材*/
	public static final String GET_MEDIA_URL = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";
	/**新增临时素材*/
	public static final String UPLOAD_MEDIA_URL = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";
	
	/**发送模板消息接口*/
	public static final String TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=";
	/**发送统一的服务消息接口*/
	public static final String UNIFORM_MSG_URL = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token=";
	/**发送订阅消息接口*/
	public static final String SUBSCRIBE_URL = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=";
	
	/**发送客服消息*/
	public static final String CUSTOM_MSG_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=";
	/**文本客服消息*/
	public static final String CUSTOM_MSG_TEXT = "{\"touser\":\"%s\",\"msgtype\":\"text\",\"text\":{\"content\":\"%s\"}}";
	/**音乐客服消息*/
	public static final String CUSTOM_MSG_MUSIC = "{\"touser\":\"%s\",\"msgtype\":\"music\",\"music\":{\"title\":\"%s\",\"musicurl\":\"%s\",\"hqmusicurl\":\"%s\",\"description\":\"%s\",\"thumb_media_id\":\"%s\"}}";
	
	/**图片鉴黄*/
	public static final String IMG_CHECK_URL = "https://api.weixin.qq.com/wxa/img_sec_check?access_token=";
	/**内容检查*/
	public static final String MSG_CHECK_URL = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=";
	/**腾讯身份证识别*/
	public static final String OCR_IDCARD_URL = "https://api.weixin.qq.com/cv/ocr/idcard";
	
	/**小程序带参二维码接口(5000次/分钟)*/
	public static final String MIN_APP_QRCODE_URL = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";
	/**小程序获取session_key和openid接口 */
	public static final String JSCODE2SESSION_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";
	
	/**QQ小程序获取session_key和openid接口 */
	public static final String JSCODE2SESSION_URL_QQ = "https://api.q.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";
	/**QQ图片鉴黄*/
	public static final String IMG_CHECK_URL_QQ = "https://api.q.qq.com/api/json/security/ImgSecCheck";
	/**QQ内容检测*/
	public static final String MSG_CHECK_URL_QQ = "https://api.q.qq.com/api/json/security/MsgSecCheck?access_token=";
	/**QQ调用凭证接口*/
	public static final String ACCESS_TOKEN_URL_QQ = "https://api.q.qq.com/api/getToken?grant_type=client_credential&appid=%s&secret=%s";
	
	public static final String SIGNTYPE = "MD5";
	public static final String TRADE_TYPE_JS = "JSAPI";
	public static final String TRADE_TYPE_H5 = "MWEB";
	public static final String SCENE_INFO = "{\"h5_info\":{\"type\":\"Wap\",\"wap_url\":\"https://mmnews.net.cn/photoFrame/wx/open/toPage?page=purchase/pay\",\"wap_name\":\"腾讯充值\"}}";
	
	public static final String TOKEN_OAUTH = "TOKEN_OAUTH";
	public static final String TOKEN_NORMAL = "TOKEN_NORMAL";
	public static final String JSSDK_TICKET = "JSSDK_TICKET";
	
	public static final String TYPE = "type";
	public static final String PIC_URL = "PicUrl";
	public static final String CONTENT = "Content";
	public static final String MEDIA_ID = "MediaId";
	public static final String TO_USER_NAME = "ToUserName";
	public static final String FROM_USER_NAME = "FromUserName";
	public static final String ERR_CODE = "errcode";
	public static final String WX_TOKEN = "wxToken";
	
    /**统一下单接口*/
	public static final String UNIFIEDORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	
	/**获取标签*/
	public static final String GET_TAGS_URL = "https://api.weixin.qq.com/cgi-bin/tags/get?access_token=";
	/**创建标签*/
	public static final String CREATE_TAGS_URL = "https://api.weixin.qq.com/cgi-bin/tags/create?access_token=";
	/**编辑标签*/
	public static final String UPDATE_TAGS_URL = "https://api.weixin.qq.com/cgi-bin/tags/update?access_token=";
	/**删除标签*/
	public static final String DELETE_TAGS_URL = "https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=";
	/**获取标签用户*/
	public static final String GET_TAG_USERS_URL = "https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=";
	/**批量为用户打标签*/
	public static final String BATCH_TAGGING_URL = "https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=";

	public static final String WX_HOST = "https://mp.weixin.qq.com/";
	
	/**66元VIP价格*/
	public static final int VIP_MONEY = 6600;
	/**拼手气金额数组 */
	public static final Integer[] LUCK_MONEY_ARR = { 5, 10, 20 };
	/**拼手气次数数组 */
	public static final Integer[] LUCK_COUNT_ARR = { 210, 520, 1230 };
	/**前端金额数组*/
	public static final Integer[] MONEY_ITEM_ARR = { 0, 2000, 500, 1000, 6600, 3000 };
	/**前端次数数组*/
	public static final Integer[] COUNT_ITEM_ARR = { 0, 1200, 200, 500, 5000, 2000 };
	/**后端支付金额数组*/
	public static final Integer[] MONEY_ARR = { 300, 500, 1000, 2000, 3000, 6600 };
	/**后端支付次数数组*/
	public static final Integer[] COUNT_ARR = { 60, 200, 500, 1200, 2000, 5000 };
	
	/**置顶天数数组*/
	public static final Integer[] DAYS_TOP_ARR = { 1, 2, 3, 4, 5 };
	/**置顶金额数组*/
	public static final Integer[] MONEY_TOP_ARR = { 2000, 3000, 5000, 7000, 9000 };
	
}
