package com.wisenet.common;

/**
 * 支付常量类
 * @author fzh
 *
 */
public class PayConst {
	
	/**VIP（绿、黄、蓝钻）金额数组*/
	public static final Integer[] VIP_MONEY_ARR = { 19800, 29800, 69800 };
	/**VIP（绿、黄、蓝钻）天数数组*/
	public static final Integer[] VIP_DAY_ARR = { 365, 365, 730 };
	/**支付天数数组*/
	public static final Integer[] DAY_ARR = { 0, 1, 2, 3, 4, 5 };
	/**支付金额数组*/
	public static final Integer[] MONEY_ARR = { 1000, 2000, 3000, 5000, 7000, 9000 };
	/**商家入驻*/
	public static final Integer PAY_TYPE_JOIN = 1;
	/**信息发布*/
	public static final Integer PAY_TYPE_PUBLISH = 2;
	/**付费查看*/
	public static final Integer PAY_TYPE_PHONE = 3;
	/**支付状态：0支付中*/
	public static final Integer PAY_STATUS_ING = 0;
	/**支付状态：1支付成功*/
	public static final Integer PAY_STATUS_SUCCESS = 1;
	/**支付状态：2支付失败*/
	public static final Integer PAY_STATUS_FAIL = 2;
	/**支付状态：3取消支付*/
	public static final Integer PAY_STATUS_CANCEL = 3;
	/**支付状态：4余额不足*/
	public static final Integer PAY_STATUS_NO_MONEY = 4;
	/**5元付费查看*/
	public static final Integer PAY_PHONE_MONEY = 5000;
	
	/**30秒内重复支付*/
	public static final int PAY_REPEAT_TIME_30S = 30000;
	/**5分钟内重复支付*/
	public static final int PAY_REPEAT_TIME_5M = 60000 * 5;
	/**支付错误信息*/
	public static final String ERROR_MSG = "errorMsg";
	/**支付失败*/
	public static final String FAIL = "FAIL";
	/**支付成功*/
	public static final String SUCCESS = "SUCCESS";
	
	public static final String MINI_APP_ID = "";

}
