package com.wisenet.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 利用线程局部(thread-local)变量保存请求中servlet对象
 */
public class WebContextHelper {

	private static ThreadLocal<HttpServletRequest> requestLocal = new ThreadLocal<HttpServletRequest>();
	
	private static ThreadLocal<HttpServletResponse> responseLocal = new ThreadLocal<HttpServletResponse>();

	public static HttpServletRequest getHttpRequest() {
		return requestLocal.get();
	}

	public static void setHttpRequest(HttpServletRequest request) {
		requestLocal.set(request);
	}
	
	public static HttpServletResponse getHttpResponse() {
		return responseLocal.get();
	}
	
	public static void setHttpResponse(HttpServletResponse response) {
		responseLocal.set(response);
	}
}
