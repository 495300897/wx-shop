package com.wisenet.common;

public class Const {
	/**主图文URL*/
	public static final String MAIN_CLICK_URL = "doDetail.action?type=1&id=";
	/**子图文URL*/
	public static final String MORE_CLICK_URL = "doDetail.action?type=2&id=";
	/**用户登录属性*/
	public static final String LOGIN_USER_ATTRIBUTE = Const.class.getName() + "_USER_ATTRIBUTE";
	/**猜图关数*/
	public static final Integer GUESS_IMG_COUNT = 476;
	/**成语关数*/
	public static final Integer GUESS_CY_COUNT = 300;
	/**猜图图片*/
	public static final String GUESS_IMG_URL = "images/%s.png";
	/**猜图详情页*/
	public static final String GUESS_IMG_DETAIL_URL = "https://mmnews.net.cn/photoFrame/wx/open/toPage?page=crz/cy&type=%s&openid=%s";
	
	/**
	 * VIDEO视频链接资源
	 */
	public static final String VIDEO_HTML = "<video src=\"VIDEO_URL\" controls=\"controls\" width=\"100%\" height=\"200\" type=\"video/mp4\"></video>";
	/**
	 * IFRAME视频链接资源
	 */
	public static final String IFRAME_HTML = "<iframe src=\"IFRAME_HTML\" scrolling=\"no\" frameborder=\"0\"  id=\"mainFrame\" width=\"100%\" height=\"200\"></iframe>";
	/**
	 * 搜库资源搜索
	 */
	public static final String SOKU_SEARCH = "http://www.soku.com/m/y/video?q=";
	
	/**默认标记*/
	public static final String MENU_TYPE_DEFAULT = "1";
	/**默认游戏*/
	public static final Integer GAME_NUM_DEFAULT = 1;
	/**搜索音乐消息*/
	public static final String MENU_TYPE_SEARCH_MUSIC = "record_search_music";
	/**汉译英消息*/
	public static final String MENU_TYPE_TRANSLATE_TO_EN = "record_translate_to_en";
	/**英译汉消息*/
	public static final String MENU_TYPE_TRANSLATE_TO_CH = "record_translate_to_ch";
	/**猜图消息*/
	public static final String MENU_TYPE_GUESS_IMAGE = "record_guess_image";
	/**成语消息*/
	public static final String MENU_TYPE_GUESS_IDIOM = "record_guess_idiom";
	/**猜数消息*/
	public static final String MENU_TYPE_GUESS_NUMBER = "record_guess_number";
	/**聊天消息*/
	public static final String MENU_TYPE_LETS_TALK = "record_lets_talk";
	/**关键词或记录功能消息*/
	public static final String MENU_TYPE_RECORD = "record";
	/**百度翻译TO_EN*/
	public static final String TRANSLATE_TYPE_TO_EN = "en";
	/**百度翻译TO_ZH*/
	public static final String TRANSLATE_TYPE_TO_ZH = "zh";
	/**百度翻译FROM*/
	public static final String TRANSLATE_TYPE_FROM = "auto";
	/**猜数结束标识*/
	public static final Integer GUESS_NUMBER_IS_SEND = 1;
	/**06重新猜数*/
	public static final String GUESS_NUMBER_INPUT = "06";
	/**猜数帮助*/
	public static final String GUESS_NUMBER_HELP = "帮助";
	/**成语*/
	public static final String GUESS_IDIOM_INPUT = "成语";
	/**猜图*/
	public static final String GUESS_IMAGE_INPUT = "猜图";
	/**天气*/
	public static final String WEATHER_INPUT = "天气";
	/**猜数错误结束*/
	public static final String GUESS_NUMBER_WRONG = "wrong";
	/**文本消息*/
	public static final String MESSAGE_TYPE_TEXT = "1";
	/**图文消息*/
	public static final String MESSAGE_TYPE_IMAGE = "2";
	/**音乐消息限制数量*/
	public static final int MUSIC_LIMIT = 4;
	/**文本限制长度，一个中文等于3个字节，即1800字节，小于2048字节*/
	public static final int MESSAGE_TEXT_LIMIT = 600;
	/**合成语音文本限制长度*/
	public static final int AUDIO_TEXT_LIMIT = 512;

}
