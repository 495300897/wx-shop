package com.wisenet.common;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

/**
 * 微信常量工具类
 * @author fzh
 *
 */
public class WxConstUtil {
	
	public static Logger logger = Logger.getLogger(WxConstUtil.class);
	
	/**
	 * 转换常量
	 * @param clz
	 * @return
	 */
	public static <T> JSONObject converConst(Class<T> clz) {
		JSONObject json = new JSONObject();
		try {
			Field[] fields = clz.getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);
				json.put(f.getName(), f.get(f.getName()).toString());
			}
			return json;
		} catch (Exception e) {
			logger.info("convert error: " + e.getMessage());
		}
		return null;
	}
}
