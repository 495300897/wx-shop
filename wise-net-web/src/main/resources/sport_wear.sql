/*
SQLyog Ultimate - MySQL GUI v8.21 
MySQL - 5.5.60 : Database - sport_wear
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sport_wear` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sport_wear`;

/*Table structure for table `access_token` */

DROP TABLE IF EXISTS `access_token`;

CREATE TABLE `access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` varchar(50) DEFAULT NULL,
  `appsecret` varchar(50) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `access_token` */

/*Table structure for table `banner` */

DROP TABLE IF EXISTS `banner`;

CREATE TABLE `banner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态: 1上架，2下架',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `sort_num` int(11) DEFAULT '0' COMMENT '排序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='banner表';

/*Data for the table `banner` */

insert  into `banner`(`id`,`image_url`,`status`,`create_time`,`update_time`,`sort_num`) values (1,'https://fzh-cos-1258302607.cos.ap-guangzhou.myqcloud.com/rank/banner02.jpg',1,'2021-11-08 11:14:28','2021-11-26 23:21:54',0),(2,'https://fzh-cos-1258302607.cos.ap-guangzhou.myqcloud.com/rank/banner03.jpg',1,'2021-11-08 11:48:11','2021-11-08 11:48:11',0);

/*Table structure for table `first_join_key` */

DROP TABLE IF EXISTS `first_join_key`;

CREATE TABLE `first_join_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(255) DEFAULT NULL,
  `image_text_no` bigint(20) DEFAULT NULL,
  `operator_name` varchar(255) DEFAULT NULL,
  `re_type` varchar(255) DEFAULT NULL,
  `re_text` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vchar1` varchar(255) DEFAULT NULL,
  `vchar2` varchar(255) DEFAULT NULL,
  `vchar3` varchar(255) DEFAULT NULL,
  `vchar4` varchar(255) DEFAULT NULL,
  `weixin_public_no` varchar(255) DEFAULT NULL,
  `wx_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `first_join_key` */

/*Table structure for table `image_text_info` */

DROP TABLE IF EXISTS `image_text_info`;

CREATE TABLE `image_text_info` (
  `image_text_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `title` varchar(255) DEFAULT NULL COMMENT '图文标题',
  `click_out_url` varchar(255) DEFAULT NULL COMMENT '跳转外链接',
  `image_url` varchar(255) DEFAULT NULL COMMENT '封面图片',
  `click_url` varchar(255) DEFAULT NULL COMMENT '暂用点赞数量',
  `digest` varchar(255) DEFAULT NULL COMMENT '备注说明，暂用来源名称',
  `image_text_type` varchar(50) DEFAULT NULL COMMENT '图文类型',
  `main_text` mediumtext COMMENT '正文内容',
  `operator` varchar(255) DEFAULT NULL COMMENT '操作主体，暂用分享次数',
  `source_url` varchar(255) DEFAULT NULL COMMENT '内容来源，暂用来源头像',
  `weixin_public_no` varchar(255) DEFAULT NULL COMMENT '账号名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`image_text_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `image_text_info` */

/*Table structure for table `image_text_more` */

DROP TABLE IF EXISTS `image_text_more`;

CREATE TABLE `image_text_more` (
  `more_image_text_no` bigint(20) NOT NULL AUTO_INCREMENT,
  `click_out_url` varchar(255) DEFAULT NULL,
  `click_url` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `digest` varchar(255) DEFAULT NULL,
  `image_text_no` bigint(20) DEFAULT NULL,
  `image_text_type` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `main_text` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `source_url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `weixin_public_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`more_image_text_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `image_text_more` */

/*Table structure for table `jssdk_config` */

DROP TABLE IF EXISTS `jssdk_config`;

CREATE TABLE `jssdk_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(100) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `appid` varchar(20) DEFAULT NULL,
  `appsecret` varchar(50) DEFAULT NULL,
  `public_no` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jssdk_config` */

/*Table structure for table `keyset` */

DROP TABLE IF EXISTS `keyset`;

CREATE TABLE `keyset` (
  `key_service_no` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(255) DEFAULT NULL,
  `key_type` varchar(255) DEFAULT NULL,
  `key_word` varchar(255) DEFAULT NULL,
  `operator_name` varchar(255) DEFAULT NULL,
  `re_type` varchar(255) DEFAULT NULL,
  `ref_activity_no` varchar(255) DEFAULT NULL,
  `ref_busi_code` varchar(255) DEFAULT NULL,
  `ref_image_text_id` bigint(20) DEFAULT NULL,
  `ref_text` varchar(255) DEFAULT NULL,
  `ref_vedio_id` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `vchar1` varchar(255) DEFAULT NULL,
  `vchar2` varchar(255) DEFAULT NULL,
  `vchar3` varchar(255) DEFAULT NULL,
  `vchar4` varchar(255) DEFAULT NULL,
  `weixin_public_no` varchar(255) DEFAULT NULL,
  `wx_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key_service_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `keyset` */

/*Table structure for table `leave_word` */

DROP TABLE IF EXISTS `leave_word`;

CREATE TABLE `leave_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) DEFAULT NULL,
  `content` varchar(300) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `formid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `leave_word` */

/*Table structure for table `menu_info` */

DROP TABLE IF EXISTS `menu_info`;

CREATE TABLE `menu_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_menu_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `mkey` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `appid` varchar(50) DEFAULT NULL,
  `pagepath` varchar(100) DEFAULT NULL,
  `weixin_public_no` varchar(255) DEFAULT NULL,
  `ref_text` varchar(1000) DEFAULT NULL,
  `ref_image_text_no` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `order_no` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `menu_info` */

/*Table structure for table `menu_keyset` */

DROP TABLE IF EXISTS `menu_keyset`;

CREATE TABLE `menu_keyset` (
  `menu_keyset_no` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(255) DEFAULT NULL,
  `key_type` varchar(255) DEFAULT NULL,
  `key_word` varchar(255) DEFAULT NULL,
  `menu_id` bigint(20) DEFAULT NULL,
  `re_type` int(11) DEFAULT NULL COMMENT '1为文本、2为图文、3为链接',
  `ref_activity_no` varchar(255) DEFAULT NULL,
  `ref_busi_code` varchar(255) DEFAULT NULL,
  `ref_image_text_id` varchar(255) DEFAULT NULL,
  `ref_text` varchar(255) DEFAULT NULL,
  `ref_vedio_id` varchar(255) DEFAULT NULL,
  `vchar1` varchar(255) DEFAULT NULL,
  `vchar2` varchar(255) DEFAULT NULL,
  `vchar3` varchar(255) DEFAULT NULL,
  `vchar4` varchar(255) DEFAULT NULL,
  `weixin_public_no` varchar(255) DEFAULT NULL,
  `wx_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`menu_keyset_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `menu_keyset` */

/*Table structure for table `sport_wear` */

DROP TABLE IF EXISTS `sport_wear`;

CREATE TABLE `sport_wear` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `sizes` varchar(200) DEFAULT NULL COMMENT '尺码',
  `price` varchar(20) DEFAULT NULL COMMENT '显示价格',
  `orig_price` varchar(20) DEFAULT NULL,
  `cover_urls` varchar(2000) DEFAULT NULL COMMENT '封面图片链接',
  `info_urls` varchar(6000) DEFAULT NULL COMMENT '详情图片链接',
  `wear_type` varchar(10) DEFAULT NULL COMMENT '分类',
  `info_text` text COMMENT '内容描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `sport_wear` */

insert  into `sport_wear`(`id`,`title`,`sizes`,`price`,`orig_price`,`cover_urls`,`info_urls`,`wear_type`,`info_text`) values (1,'薄夏秋季足球训练裤男士透气速干收口运动长裤健身骑行跑步休闲裤','L,XL,XXL,XXXL,4XL','33.90','67.80','https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/2.jpg,https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/3.jpg,https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/4.jpg,https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/5.jpg','https://img.alicdn.com/imgextra/i2/154988527/TB21IW5aS0mpuFjSZPiXXbssVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2LL51aOlnpuFjSZFgXXbi7FXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB23CK4aS8mpuFjSZFMXXaxpVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2nci0aNtmpuFjSZFqXXbHFpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uNa0aSVmpuFjSZFFXXcZApXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uyafqmFjpuFjSspbXXXagVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2qMoEXDAKh1JjSZFDXXbKlFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2K.14qYJkpuFjy1zcXXa5FFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uLu3aItnpuFjSZFvXXbcTpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB21HG6arBkpuFjy1zkXXbSpFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2.zu.aB8kpuFjSspeXXc7IpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2Rxy8axXlpuFjSsphXXbJOXXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2byG1aJ0opuFjSZFxXXaDNVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2D8y5awNlpuFjy0FfXXX3CpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2v054aNhmpuFjSZFyXXcLdFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2zr15aHxmpuFjSZJiXXXauVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2gl92aUdnpuFjSZPhXXbChpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2m3y3aHlmpuFjSZFlXXbdQXXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2UH15aHxmpuFjSZJiXXXauVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2L4a1aUlnpuFjSZFjXXXTaVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2WHC4aUhnpuFjSZFEXXX0PFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2SrG4aNBmpuFjSZFDXXXD8pXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2z4m0aSVmpuFjSZFFXXcZApXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2obO6arBkpuFjy1zkXXbSpFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB20mm.arFkpuFjy1XcXXclapXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2wPa4aCXlpuFjy0FeXXcJbFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB24fe9awxlpuFjSszgXXcJdpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2qLm_arXlpuFjy1zbXXb_qpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2VY9.awJlpuFjSspjXXcT.pXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB26fC_aCxjpuFjSszeXXaeMVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2F5a.aB0kpuFjy1XaXXaFkVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2CLG3aItnpuFjSZFvXXbcTpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2rp2baCFjpuFjSszhXXaBuVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB29b95aHxmpuFjSZJiXXXauVXa_!!154988527.jpg','足球系列','颜色分类: 9011黑灰（薄款） K7013黑绿（薄款） K7013黑红（薄款） K7013黑白（薄款） K7013黑蓝薄（薄款） c9011黑灰（薄款口袋有拉链） c9011黑绿（薄款口袋有拉链） H9011黑灰（加绒加厚口袋拉链）\n款号: 9011/9033\n品牌: 亿可尔\n上市时间: 2018年\n吊牌价: 129\n裤门襟: 松紧\n面料材质: 涤纶\n里料材质: 涤纶\n服装款式细节: 徽章 图案 字母 贴袋 胶印\n运动户外项目: 运动休闲\n功能: 吸湿排汗 抗紫外线 速干 耐磨 透气 防风 超强弹性\n性别: 男\n尺码: S,M,L,XL,XXL,XXXL 更大码（成人)\n运动系列: 运动长裤'),(2,'足球服套装男成人光板足球衣DIY定制夏季印号短袖比赛训练服队服','L,XL,XXL,XXXL,4XL','38.00','76.00','http://gd3.alicdn.com/imgextra/i3/154988527/TB2MLvmvodnpuFjSZPhXXbChpXa_!!154988527.jpg_400x400.jpg,http://gd2.alicdn.com/imgextra/i2/154988527/TB2HunuvctnpuFjSZFvXXbcTpXa_!!154988527.jpg_400x400.jpg,http://gd3.alicdn.com/imgextra/i3/154988527/TB2p_BVbIPRfKJjSZFOXXbKEVXa_!!154988527.jpg_400x400.jpg,http://gd3.alicdn.com/imgextra/i3/154988527/TB2KCEduq8lpuFjy0FpXXaGrpXa_!!154988527.jpg_400x400.jpg','https://img.alicdn.com/imgextra/i4/154988527/TB2rMz3rR8kpuFjSspeXXc7IpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB29nzLrKJ8puFjy1XbXXagqVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2oPquphRDOuFjSZFzXXcIipXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2MyrxvdRopuFjSZFtXXcanpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2.wzovohnpuFjSZFEXXX0PFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2MxYsvblmpuFjSZFlXXbdQXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2VJDlvd0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2aeYdvdFopuFjSZFHXXbSlXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2Jg_drStkpuFjy0FhXXXQzFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2sUMwrSJjpuFjy0FdXXXmoFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2ofemeCB0XKJjSZFsXXaxfpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2Dg9XbSvHfKJjSZFPXXbttpXa_!!154988527.jpg_400x400.jpg','足球系列','颜色分类: 115白色套装 115橙色套装 115红色套装 115蓝色套装 115绿色套装 116橙色套装 116红色套装 116蓝色套装 116绿色套装货号: 116+115足球服版本: 球员版上下装分类: 短款套装主客场: 主场\n上市时间: 2018年\n尺码: L XL XXL XXXL 4XL\n英超: 利物浦\n西甲: 皇家马德里\n意甲: AC米兰\n德甲: 拜仁慕尼黑\n法甲: 波尔多\n中超: 广州恒大\n国家队: 中国适用\n对象: 男'),(3,'健身宽松薄秋冬季足球训练裤运动长裤男收腿小脚速干弹力跑步骑行','S,M,L,XL,XXL,XXXL','35.90','71.80','http://gd1.alicdn.com/imgextra/i1/154988527/TB2QPGTaJ0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,http://gd1.alicdn.com/imgextra/i1/154988527/TB2W8XFavSM.eBjSZFNXXbgYpXa_!!154988527.jpg_400x400.jpg,http://gd1.alicdn.com/imgextra/i8/TB1caObNVXXXXchXFXXYXGcGpXX_M2.SS2_400x400.jpg,http://gd1.alicdn.com/imgextra/i1/154988527/TB2fcjKsCFjpuFjSspbXXXagVXa_!!154988527.jpg_400x400.jpg','https://img.alicdn.com/imgextra/i1/154988527/TB2ZE5TaNxmpuFjSZFNXXXrRXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB29o5TaNxmpuFjSZFNXXXrRXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2IxGYaH4npuFjSZFmXXXl4FXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2WDWRaNtmpuFjSZFqXXbHFpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2QWylaQqvpuFjSZFhXXaOgXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB28VIPtb4npuFjSZFmXXXl4FXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2QHiRaS4mpuFjSZFOXXaUqpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2s5m4aB8lpuFjy0FnXXcZyXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2rH11aChlpuFjSspkXXa1ApXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2u5a0awJkpuFjSszcXXXfsFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB23xq5arVkpuFjSspcXXbSMVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2isS3aB4lpuFjy1zjXXcAKpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uNu5arVkpuFjSspcXXbSMVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2CcS3aB4lpuFjy1zjXXcAKpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2w4S1axXkpuFjy0FiXXbUfFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2IaKlaQqvpuFjSZFhXXaOgXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2HweYaCXlpuFjy0FeXXcJbFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB259GTaOpnpuFjSZFIXXXh2VXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2NhWYaH4npuFjSZFmXXXl4FXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2sSmWaS0mpuFjSZPiXXbssVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2PeCOaJBopuFjSZPcXXc9EpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2_X9UaJRopuFjSZFtXXcanpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uuGUaOpnpuFjSZFkXXc4ZpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2SpyLauJ8puFjy1XbXXagqVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB26MKVaHJmpuFjSZFBXXXaZXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2oXO4arXlpuFjy1zbXXb_qpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2Jdi2artlpuFjSspfXXXLUpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB25QGVaNBmpuFjSZFDXXXD8pXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2A4STaORnpuFjSZFCXXX2DXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2_ceTaUlnpuFjSZFjXXXTaVXa_!!154988527.jpg_400x400.jpg','足球系列','颜色分类: 8012黑红 8012黑白 8012黑蓝 8012黑绿 9030黑黑（螺纹弹力裤脚无拉链） 9030黑灰（螺纹弹力裤脚无拉链）\n款号: 8012-9030\n品牌: 亿可尔\n上市时间: 2018年\n吊牌价: 129\n裤门襟: 松紧\n面料材质: 涤纶\n里料材质: 涤纶\n服装款式细节: 胶印\n运动户外项目: 运动休闲\n功能: 吸湿排汗 抗紫外线 速干 耐磨 透气 防风 防水 超强弹性\n性别: 男\n尺码: L M XL XXL XXXL XXXXL\n运动系列: 运动长裤'),(4,'运动长裤男足球训练裤夏季薄款收腿小脚收口健身跑步骑行休闲速干','S,M,L,XL,XXL,XXXL','35.90-39.90','71.80-79.80','http://gd3.alicdn.com/imgextra/i3/154988527/TB2Z6c9wH4npuFjSZFmXXXl4FXa_!!154988527.jpg_400x400.jpg,http://gd1.alicdn.com/imgextra/i1/154988527/TB25GSMsxXlpuFjSsphXXbJOXXa_!!154988527.jpg_400x400.jpg,http://gd4.alicdn.com/imgextra/i4/154988527/TB2dH.ebohnpuFjSZFpXXcpuXXa_!!154988527.jpg_400x400.jpg,http://gd4.alicdn.com/imgextra/i4/154988527/TB2mgibexaK.eBjSZFwXXXjsFXa_!!154988527.jpg_400x400.jpg','https://img.alicdn.com/imgextra/i2/154988527/TB22j1GcEhnpuFjSZFpXXcpuXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2EIeRcxlmpuFjSZPfXXc9iXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2KseRcxlmpuFjSZPfXXc9iXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2p1Cmb3NlpuFjy0FfXXX3CpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2fIeNcCFmpuFjSZFrXXayOXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2DOykbYBkpuFjy1zkXXbSpFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2FJmIqYXlpuFjSszfXXcSGXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB26hKOq80kpuFjy1zdXXXuUVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2dg9IcypnpuFjSZFIXXXh2VXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2x89EcElnpuFjSZFjXXXTaVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2WLiyb3JlpuFjSspjXXcT.pXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2TBeIct0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2KkmPcr4npuFjSZFmXXXl4FXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2gFqnbYtlpuFjSspfXXXLUpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB26QmPcr4npuFjSZFmXXXl4FXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2Mpigb3JkpuFjSszcXXXfsFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2QKuMcrJmpuFjSZFBXXXaZXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2el1LcxBmpuFjSZFDXXXD8pXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2QHuPcrxmpuFjSZJiXXXauVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2sqyCcrJmpuFjSZFwXXaE4VXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2u6CPcrBmpuFjSZFAXXaQ0pXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2B0uLcxhmpuFjSZFyXXcLdFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2ROOkbYBkpuFjy1zkXXbSpFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2HQibb1J8puFjy1XbXXagqVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2Kbqkb8NkpuFjy0FaXXbRCVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2afCmb9BjpuFjSsplXXa5MVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2K2usb90jpuFjy0FlXXc0bpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2b5Skb30kpuFjSspdXXX4YXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2qi1gb9XlpuFjy0FeXXcJbFXa_!!154988527.jpg_400x400.jpg','足球系列','颜色分类: c972-黑绿（薄款口袋有拉链） c972-黑蓝（薄款口袋有拉链） c972-黑红（薄款口袋有拉链） H972黑红（加绒加厚口袋拉链）\n款号: 972\n品牌: 亿可尔\n上市时间: 2018年\n吊牌价: 129\n裤门襟: 松紧\n面料材质: 涤纶\n里料材质: 涤纶\n服装款式细节: 徽章 图案 字母 刺绣 胶印\n运动户外项目: 运动休闲\n功能: 吸湿排汗 抗紫外线 速干 耐磨 透气 防风 超强弹性\n性别: 男\n尺码: S,M,L,XL,XXL,XXXL\n运动系列: 运动长裤'),(5,'足球训练裤男士秋春夏季运动长裤速干收小腿弹力跑步健身休闲裤','S,M,L,XL,XXL','35.90','71.80','http://gd2.alicdn.com/imgextra/i2/154988527/TB2iMk.cY1K.eBjSsphXXcJOXXa_!!154988527.jpg_400x400.jpg,http://gd1.alicdn.com/imgextra/i8/TB1caObNVXXXXchXFXXYXGcGpXX_M2.SS2_400x400.jpg,http://gd1.alicdn.com/imgextra/i4/TB1ZFmLNVXXXXabXFXXYXGcGpXX_M2.SS2_400x400.jpg,http://gd4.alicdn.com/imgextra/i4/154988527/TB2mgibexaK.eBjSZFwXXXjsFXa_!!154988527.jpg_400x400.jpg','https://img.alicdn.com/imgextra/i2/154988527/TB2Fsi7awRkpuFjy1zeXXc.6FXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2A8y8aq8lpuFjy0FpXXaGrpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB22hmZaUhnpuFjSZFPXXb_4XXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2M7K.aCFjpuFjSszhXXaBuVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2xxOZaOpnpuFjSZFIXXXh2VXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2oUWZaUdnpuFjSZPhXXbChpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2.hi1aNhmpuFjSZFyXXcLdFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2e7GZaNxmpuFjSZFNXXXrRXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2bDa2aJ4opuFjSZFLXXX8mXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2UxuZaItnpuFjSZFKXXalFFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2XIGVaHBnpuFjSZFGXXX51pXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2a61VaS4mpuFjSZFOXXaUqpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2Ce52aHxmpuFjSZJiXXXauVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB297i5arXlpuFjSszfXXcSGXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2IEaOauJ8puFjy1XbXXagqVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2vE53aCBjpuFjy1XdXXaooVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2of53awNlpuFjy0FfXXX3CpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2ml14aw0kpuFjSspdXXX4YXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2G0e9aB0kpuFjy1XaXXaFkVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2Ai96aCBjpuFjSsplXXa5MVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2gF59aC0jpuFjy0FlXXc0bpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2cuG1aJXnpuFjSZFoXXXLcpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB28Si3arBkpuFjy1zkXXbSpFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2Mbq0aJFopuFjSZFHXXbSlXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2ZYq0aJFopuFjSZFHXXbSlXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB22j53aOBnpuFjSZFzXXaSrpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2V7SZaNxmpuFjSZFNXXXrRXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2DYS2aS8mpuFjSZFMXXaxpVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2Y2y1aNBmpuFjSZFDXXXD8pXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB21Va4aCBjpuFjy1XdXXaooVXa_!!154988527.jpg_400x400.jpg','足球系列','颜色分类: 982黑红 982黑蓝 982黑绿\n款号: 982\n上市时间: 2018年\n吊牌价: 129\n裤门襟: 松紧\n面料材质: 涤纶\n里料材质: 涤纶\n服装款式细节: 字母 刺绣 胶印\n运动户外项目: 运动休闲\n功能: 吸湿排汗 速干 耐磨 透气 防风 超强弹性\n性别: 男\n尺码: L XL XXL XXXL XXXXL\n运动系列: 运动长裤'),(6,'运动套装男短袖短裤训练速干健身服夏季羽毛球薄款吸汗透气跑步服','L,XL,2XL,3XL','38.00','76.00','http://gd1.alicdn.com/imgextra/i4/TB1437KRpXXXXc2XXXXYXGcGpXX_M2.SS2_400x400.jpg,http://gd1.alicdn.com/imgextra/i6/TB1R2T8RpXXXXc2apXXYXGcGpXX_M2.SS2_400x400.jpg,http://gd3.alicdn.com/imgextra/i3/154988527/TB2cb1ErRNkpuFjy0FaXXbRCVXa_!!154988527.jpg_400x400.jpg,http://gd1.alicdn.com/imgextra/i1/154988527/TB2RB9WuwRkpuFjy1zeXXc.6FXa_!!154988527.jpg_400x400.jpg','https://img.alicdn.com/imgextra/i3/154988527/TB2ozbovblmpuFjSZFlXXbdQXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2Px48bSvHfKJjSZFPXXbttpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2IEKMvmVmpuFjSZFFXXcZApXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2c5PkvohnpuFjSZFEXXX0PFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2HUz9rG8lpuFjy0FpXXaGrpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2cUWMvmVmpuFjSZFFXXcZApXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2PATQrR0kpuFjSsppXXcGTXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2NrYurHJkpuFjy1zcXXa5FFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2uMLhvd0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2zwfUrSFjpuFjSspbXXXagVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2jg6_pOC9MuFjSZFoXXbUzFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2zF4YpdhvOuFjSZFBXXcZgFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2QujevipnpuFjSZFIXXXh2VXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2Cr_AvmFmpuFjSZFrXXayOXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2.M_ivodnpuFjSZPhXXbChpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2ajPovblmpuFjSZFlXXbdQXXa_!!154988527.jpg_400x400.jpg','足球系列','颜色分类: 107白色套装 107黑色套装 107红色套装 107黄色套装 107蓝色套装 107绿色套装 白色 黑色 桔红色 红色 柠檬黄 \n蓝色款号: 106+107\n上市时间: 2018年夏季\n吊牌价: 129\n上装款式: 套头\n上衣领型: 其他\n袖长: 短袖\n裤长: 五分裤\n材质: 涤纶\n服装款式细节: 线条 撞色 光版\n功能: 吸湿排汗 抗紫外线 速干 透气\n性别: 男\n尺码: L XL 2XL 3XL\n运动系列: 运动生活'),(7,'春夏季足球训练裤透气收小腿骑行跑步运动长裤男女速干口袋有拉链','S,M,L,XL,XXL','39.90','79.80','http://gd1.alicdn.com/imgextra/i1/154988527/TB25GSMsxXlpuFjSsphXXbJOXXa_!!154988527.jpg_400x400.jpg,http://gd2.alicdn.com/imgextra/i2/154988527/TB2Ix9CswNlpuFjy0FfXXX3CpXa_!!154988527.jpg_400x400.jpg,http://gd2.alicdn.com/imgextra/i2/154988527/TB2nyeCswNlpuFjy0FfXXX3CpXa_!!154988527.jpg_400x400.jpg,http://gd4.alicdn.com/imgextra/i4/154988527/TB2mgibexaK.eBjSZFwXXXjsFXa_!!154988527.jpg_400x400.jpg','https://img.alicdn.com/imgextra/i3/154988527/TB2yBW2aS8mpuFjSZFMXXaxpVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2UYu0aItnpuFjSZFKXXalFFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2BOO9aC0jpuFjy0FlXXc0bpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2.XS2aUhnpuFjSZFEXXX0PFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2QzO8aCxjpuFjSszeXXaeMVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2Fuu1aItnpuFjSZFvXXbcTpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2pLaqaQqvpuFjSZFhXXaOgXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2ZHm3aS0mpuFjSZPiXXbssVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2989ZaORnpuFjSZFCXXX2DXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2G610aOpnpuFjSZFkXXc4ZpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2s3KZaJ0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2Xga8arJkpuFjy1zcXXa5FFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2zkK7aB4lpuFjy1zjXXcAKpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB24BC6axXlpuFjSsphXXbJOXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2l6W8aCxjpuFjSszeXXaeMVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2xMK8artlpuFjSspoXXbcDpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2lFK_arFkpuFjy1XcXXclapXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB27Li8awFkpuFjSspnXXb4qFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2HbS7aCBjpuFjSsplXXa5MVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB21S12aCXlpuFjy0FeXXcJbFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2_MOZaJ0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2SWi9aB8lpuFjy0FnXXcZyXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2Nj90aOpnpuFjSZFkXXc4ZpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2eBe7awRkpuFjy1zeXXc.6FXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2w2e1aHlmpuFjSZFlXXbdQXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2lrSYaNtmpuFjSZFqXXbHFpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2U3SZaJ0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB21ve1aHlmpuFjSZFlXXbdQXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB20bqZaNBmpuFjSZFsXXcXpFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2mhS3aJ4opuFjSZFLXXX8mXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2kVa4aHBmpuFjSZFAXXaQ0pXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2IbO0aItnpuFjSZFKXXalFFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB28Ka4aOBnpuFjSZFzXXaSrpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2hue4aOBnpuFjSZFzXXaSrpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2bce2aNhmpuFjSZFyXXcLdFXa_!!154988527.jpg_400x400.jpg','足球系列','颜色分类: 7013黑蓝（口袋无拉链） 996黑色（口袋有拉链） 7013黑红（口袋无拉链） 997黑绿（口袋有拉链） 996蓝色（口袋有拉链） 997黑白（口袋有拉链） 7013黑白（口袋无拉链） 7013黑绿（口袋无拉链）\n款号: 996/997\n上市时间: 2018年\n吊牌价: 168\n裤门襟: 松紧\n面料材质: 涤纶\n里料材质: 涤纶\n运动户外项目: 运动\n休闲功能: 吸湿排汗 抗紫外线 速干 保暖 耐磨 超轻 透气 超强弹性\n性别: 男\n尺码: L XL XXL XXXL XXXXL\n运动系列: 运动长裤'),(8,'薄夏秋季足球训练裤男士透气速干收口运动长裤健身骑行跑步休闲裤','S,M,L,XL,XXL,XXXL','35.90','71.80','https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/2.jpg,https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/3.jpg,https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/4.jpg,https://fzh-cos-1256770705.cos.ap-guangzhou.myqcloud.com/sport_wear/5.jpg','https://img.alicdn.com/imgextra/i2/154988527/TB21IW5aS0mpuFjSZPiXXbssVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2LL51aOlnpuFjSZFgXXbi7FXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB23CK4aS8mpuFjSZFMXXaxpVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2nci0aNtmpuFjSZFqXXbHFpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uNa0aSVmpuFjSZFFXXcZApXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uyafqmFjpuFjSspbXXXagVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2qMoEXDAKh1JjSZFDXXbKlFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2K.14qYJkpuFjy1zcXXa5FFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2uLu3aItnpuFjSZFvXXbcTpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB21HG6arBkpuFjy1zkXXbSpFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2.zu.aB8kpuFjSspeXXc7IpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2Rxy8axXlpuFjSsphXXbJOXXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2byG1aJ0opuFjSZFxXXaDNVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2D8y5awNlpuFjy0FfXXX3CpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2v054aNhmpuFjSZFyXXcLdFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2zr15aHxmpuFjSZJiXXXauVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2gl92aUdnpuFjSZPhXXbChpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2m3y3aHlmpuFjSZFlXXbdQXXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2UH15aHxmpuFjSZJiXXXauVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2L4a1aUlnpuFjSZFjXXXTaVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2WHC4aUhnpuFjSZFEXXX0PFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2SrG4aNBmpuFjSZFDXXXD8pXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2z4m0aSVmpuFjSZFFXXcZApXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2obO6arBkpuFjy1zkXXbSpFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB20mm.arFkpuFjy1XcXXclapXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2wPa4aCXlpuFjy0FeXXcJbFXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB24fe9awxlpuFjSszgXXcJdpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2qLm_arXlpuFjy1zbXXb_qpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2VY9.awJlpuFjSspjXXcT.pXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB26fC_aCxjpuFjSszeXXaeMVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2F5a.aB0kpuFjy1XaXXaFkVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2CLG3aItnpuFjSZFvXXbcTpXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2rp2baCFjpuFjSszhXXaBuVXa_!!154988527.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB29b95aHxmpuFjSZJiXXXauVXa_!!154988527.jpg','足球系列','颜色分类: 9011黑灰（薄款） K7013黑绿（薄款） K7013黑红（薄款） K7013黑白（薄款） K7013黑蓝薄（薄款） c9011黑灰（薄款口袋有拉链） c9011黑绿（薄款口袋有拉链） H9011黑灰（加绒加厚口袋拉链）\n款号: 9011/9033\n品牌: 亿可尔\n上市时间: 2018年\n吊牌价: 129\n裤门襟: 松紧\n面料材质: 涤纶\n里料材质: 涤纶\n服装款式细节: 徽章 图案 字母 贴袋 胶印\n运动户外项目: 运动休闲\n功能: 吸湿排汗 抗紫外线 速干 耐磨 透气 防风 超强弹性\n性别: 男\n尺码: S,M,L,XL,XXL,XXXL 更大码（成人)\n运动系列: 运动长裤'),(9,'运动套装男短袖短裤训练速干健身服夏季羽毛球薄款吸汗透气跑步服','L,XL,2XL,3XL,4XL','38.00','76.00','http://gd1.alicdn.com/imgextra/i4/TB1437KRpXXXXc2XXXXYXGcGpXX_M2.SS2_400x400.jpg,http://gd1.alicdn.com/imgextra/i6/TB1R2T8RpXXXXc2apXXYXGcGpXX_M2.SS2_400x400.jpg,http://gd3.alicdn.com/imgextra/i3/154988527/TB2cb1ErRNkpuFjy0FaXXbRCVXa_!!154988527.jpg_400x400.jpg,http://gd1.alicdn.com/imgextra/i1/154988527/TB2RB9WuwRkpuFjy1zeXXc.6FXa_!!154988527.jpg_400x400.jpg','https://img.alicdn.com/imgextra/i3/154988527/TB2ozbovblmpuFjSZFlXXbdQXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2Px48bSvHfKJjSZFPXXbttpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2IEKMvmVmpuFjSZFFXXcZApXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2c5PkvohnpuFjSZFEXXX0PFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2HUz9rG8lpuFjy0FpXXaGrpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2cUWMvmVmpuFjSZFFXXcZApXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2PATQrR0kpuFjSsppXXcGTXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2NrYurHJkpuFjy1zcXXa5FFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2uMLhvd0opuFjSZFxXXaDNVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2zwfUrSFjpuFjSspbXXXagVXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2jg6_pOC9MuFjSZFoXXbUzFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i1/154988527/TB2zF4YpdhvOuFjSZFBXXcZgFXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i3/154988527/TB2QujevipnpuFjSZFIXXXh2VXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2Cr_AvmFmpuFjSZFrXXayOXXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i2/154988527/TB2.M_ivodnpuFjSZPhXXbChpXa_!!154988527.jpg_400x400.jpg,https://img.alicdn.com/imgextra/i4/154988527/TB2ajPovblmpuFjSZFlXXbdQXXa_!!154988527.jpg_400x400.jpg','足球系列','颜色分类: 107白色套装 107黑色套装 107红色套装 107黄色套装 107蓝色套装 107绿色套装 白色 黑色 桔红色 红色 柠檬黄 \n蓝色款号: 106+107\n上市时间: 2018年夏季\n吊牌价: 129\n上装款式: 套头\n上衣领型: 其他\n袖长: 短袖\n裤长: 五分裤\n材质: 涤纶\n服装款式细节: 线条 撞色 光版\n功能: 吸湿排汗 抗紫外线 速干 透气\n性别: 男\n尺码: L XL 2XL 3XL 4XL\n运动系列: 运动生活');

/*Table structure for table `sport_wear_addr` */

DROP TABLE IF EXISTS `sport_wear_addr`;

CREATE TABLE `sport_wear_addr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `phone` char(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `flag` int(11) DEFAULT '1' COMMENT '1常用，0为否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sport_wear_addr` */

/*Table structure for table `sport_wear_info` */

DROP TABLE IF EXISTS `sport_wear_info`;

CREATE TABLE `sport_wear_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wear_id` int(11) DEFAULT NULL COMMENT '外键',
  `color` varchar(50) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `stock` int(11) DEFAULT '1' COMMENT '1为有货，0为否',
  `color_url` varchar(200) DEFAULT NULL COMMENT '颜色链接',
  `orig_price` double DEFAULT NULL COMMENT '原价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

/*Data for the table `sport_wear_info` */

insert  into `sport_wear_info`(`id`,`wear_id`,`color`,`price`,`stock`,`color_url`,`orig_price`) values (1,1,'9011黑灰（薄款）',33.9,1,'http://gd4.alicdn.com/imgextra/i4/154988527/TB2bkNMaxBmpuFjSZFDXXXD8pXa_!!154988527.jpg_100x100.jpg',67.8),(2,1,'K7013黑绿（薄款）',33.9,1,'http://gd2.alicdn.com/imgextra/i1/154988527/TB2f4RKaCVmpuFjSZFFXXcZApXa_!!154988527.jpg_100x100.jpg',67.8),(3,1,'K7013黑红（薄款）',33.9,1,'http://gd1.alicdn.com/imgextra/i3/154988527/TB2lgpLaEhnpuFjSZFPXXb_4XXa_!!154988527.jpg_100x100.jpg',67.8),(4,1,'K7013黑白（薄款）',33.9,1,'http://gd2.alicdn.com/imgextra/i3/154988527/TB2J2xHatBopuFjSZPcXXc9EpXa_!!154988527.jpg_100x100.jpg',67.8),(5,1,'K7013黑蓝薄（薄款）',33.9,1,'http://gd1.alicdn.com/imgextra/i4/154988527/TB2GApKatRopuFjSZFtXXcanpXa_!!154988527.jpg_100x100.jpg',67.8),(7,8,'c9011黑灰（薄款口袋有拉链）',35.9,1,'http://gd1.alicdn.com/imgextra/i4/154988527/TB2ez41eyGO.eBjSZFpXXb3tFXa_!!154988527.jpg_100x100.jpg',71.8),(8,8,'H9011黑灰（加绒加厚口袋拉链）',39.9,1,'http://gd1.alicdn.com/imgextra/i1/154988527/TB25GSMsxXlpuFjSsphXXbJOXXa_!!154988527.jpg_100x100.jpg',79.8),(9,2,'115白色套装',38,1,'http://gd1.alicdn.com/imgextra/i2/154988527/TB2hFK3volnpuFjSZFjXXXTaVXa_!!154988527.jpg_100x100.jpg',76),(10,2,'115橙色套装',38,1,'http://gd4.alicdn.com/imgextra/i1/154988527/TB2hpqIvbJmpuFjSZFwXXaE4VXa_!!154988527.jpg_100x100.jpg',76),(11,2,'115红色套装',38,1,'http://gd2.alicdn.com/imgextra/i2/154988527/TB2TOnxvipnpuFjSZFkXXc4ZpXa_!!154988527.jpg_100x100.jpg',76),(12,2,'115蓝色套装',38,1,'http://gd3.alicdn.com/imgextra/i4/154988527/TB2Yhb0vhlmpuFjSZPfXXc9iXXa_!!154988527.jpg_100x100.jpg',76),(13,2,'115绿色套装',38,1,'http://gd4.alicdn.com/imgextra/i1/154988527/TB2NdyGvkqvpuFjSZFhXXaOgXXa_!!154988527.jpg_100x100.jpg',76),(14,2,'116橙色套装',38,1,'http://gd1.alicdn.com/imgextra/i4/154988527/TB20AbtvctnpuFjSZFvXXbcTpXa_!!154988527.jpg_100x100.jpg',76),(15,2,'116红色套装',38,1,'http://gd3.alicdn.com/imgextra/i2/154988527/TB2AbnMvbBmpuFjSZFuXXaG_XXa_!!154988527.jpg_100x100.jpg',76),(16,2,'116蓝色套装',38,1,'http://gd2.alicdn.com/imgextra/i3/154988527/TB2hmyZvohnpuFjSZFpXXcpuXXa_!!154988527.jpg_100x100.jpg',76),(17,2,'116绿色套装',38,1,'http://gd3.alicdn.com/imgextra/i3/154988527/TB2MBmuvbBnpuFjSZFGXXX51pXa_!!154988527.jpg_100x100.jpg',76),(18,3,'8012黑红',35.9,1,'http://gd1.alicdn.com/imgextra/i1/154988527/TB26g0BaByN.eBjSZFIXXXbUVXa_!!154988527.jpg_100x100.jpg',71.8),(19,3,'8012黑白',35.9,1,'http://gd1.alicdn.com/imgextra/i4/154988527/TB2FcVFap5N.eBjSZFmXXboSXXa_!!154988527.jpg_100x100.jpg',71.8),(20,3,'8012黑蓝',35.9,1,'http://gd1.alicdn.com/imgextra/i3/154988527/TB24PNCaCiK.eBjSZFsXXbxZpXa_!!154988527.jpg_100x100.jpg',71.8),(21,3,'8012黑绿',35.9,1,'http://gd4.alicdn.com/imgextra/i4/154988527/TB2G2hBaE5O.eBjSZFxXXaaJFXa_!!154988527.jpg_100x100.jpg',71.8),(22,3,'9030黑黑（螺纹弹力裤脚无拉链）',35.9,1,'http://gd3.alicdn.com/imgextra/i1/154988527/TB2m4JxaA1M.eBjSZFOXXc0rFXa_!!154988527.jpg_100x100.jpg',71.8),(23,3,'9030黑灰（螺纹弹力裤脚无拉链）',35.9,1,'http://gd2.alicdn.com/imgextra/i3/154988527/TB2F20zazm2.eBjSZFtXXX56VXa_!!154988527.jpg_100x100.jpg',71.8),(24,4,'c972-黑绿（薄款口袋有拉链）',35.9,1,'http://gd1.alicdn.com/imgextra/i3/154988527/TB2m5x9eAWM.eBjSZFhXXbdWpXa_!!154988527.jpg_100x100.jpg',71.8),(25,4,'c972-黑蓝（薄款口袋有拉链）',35.9,1,'http://gd1.alicdn.com/imgextra/i4/154988527/TB2mgibexaK.eBjSZFwXXXjsFXa_!!154988527.jpg_100x100.jpg',71.8),(26,4,'c972-黑红（薄款口袋有拉链）',35.9,1,'http://gd3.alicdn.com/imgextra/i1/154988527/TB2DBCXepOP.eBjSZFHXXXQnpXa_!!154988527.jpg_100x100.jpg',71.8),(27,4,'H972黑红（加绒加厚口袋拉链）',39.9,1,'http://gd2.alicdn.com/imgextra/i3/154988527/TB2Z6c9wH4npuFjSZFmXXXl4FXa_!!154988527.jpg_100x100.jpg',79.8),(28,5,'982黑红',35.9,1,'http://gd4.alicdn.com/imgextra/i1/154988527/TB28p2EXY5K.eBjy0FnXXaZzVXa_!!154988527.jpg_100x100.jpg',71.8),(29,5,'982黑蓝',35.9,1,'http://gd3.alicdn.com/imgextra/i3/154988527/TB2i4nqXLTJXuFjSspeXXapipXa_!!154988527.jpg_100x100.jpg',71.8),(30,5,'982黑绿',35.9,1,'http://gd3.alicdn.com/imgextra/i3/154988527/TB29_6FX4uI.eBjy0FdXXXgbVXa_!!154988527.jpg_100x100.jpg',71.8),(31,6,'106白色',38,1,'http://gd4.alicdn.com/imgextra/i4/154988527/TB2CQMRrMxlpuFjy0FoXXa.lXXa_!!154988527.jpg_100x100.jpg',76),(32,6,'106黑色',38,1,'http://gd3.alicdn.com/imgextra/i3/154988527/TB2nvjvrMRkpuFjy1zeXXc.6FXa_!!154988527.jpg_100x100.jpg',76),(33,6,'106桔红色',38,1,'http://gd3.alicdn.com/imgextra/i2/154988527/TB2iOntrHJkpuFjy1zcXXa5FFXa_!!154988527.jpg_100x100.jpg',76),(34,6,'106红色',38,1,'http://gd3.alicdn.com/imgextra/i2/154988527/TB2a2y9rHtlpuFjSspfXXXLUpXa_!!154988527.jpg_100x100.jpg',76),(35,6,'106柠檬黄',38,1,'http://gd2.alicdn.com/imgextra/i4/154988527/TB2Mlz5rH8kpuFjy0FcXXaUhpXa_!!154988527.jpg_100x100.jpg\n',76),(36,6,'106蓝色',38,1,'http://gd2.alicdn.com/imgextra/i1/154988527/TB2BlH5rH8kpuFjy0FcXXaUhpXa_!!154988527.jpg_100x100.jpg',76),(37,9,'107白色套装',38,1,'http://gd1.alicdn.com/imgextra/i2/154988527/TB2XvYvrMRkpuFjy1zeXXc.6FXa_!!154988527.jpg_100x100.jpg',76),(38,9,'107黑色套装',38,1,'http://gd1.alicdn.com/imgextra/i4/154988527/TB2oKm5rH0kpuFjy0FjXXcBbVXa_!!154988527.jpg_100x100.jpg',76),(39,9,'107红色套装',38,1,'http://gd3.alicdn.com/imgextra/i2/154988527/TB2Yu5ZrSBjpuFjSsplXXa5MVXa_!!154988527.jpg_100x100.jpg',76),(40,9,'107黄色套装',38,1,'http://gd3.alicdn.com/imgextra/i2/154988527/TB2ITvTrSFjpuFjSspbXXXagVXa_!!154988527.jpg_100x100.jpg',76),(41,9,'107蓝色套装',38,1,'http://gd3.alicdn.com/imgextra/i4/154988527/TB2gJDHrHVkpuFjSspcXXbSMVXa_!!154988527.jpg_100x100.jpg',76),(42,9,'107绿色套装',38,1,'http://gd3.alicdn.com/imgextra/i2/154988527/TB2ern2rHFkpuFjy1XcXXclapXa_!!154988527.jpg_100x100.jpg',76),(47,7,'996黑色（口袋有拉链）',39.9,1,'http://gd3.alicdn.com/imgextra/i3/154988527/TB2TQK_abOJ.eBjy1XaXXbNupXa_!!154988527.jpg_100x100.jpg',79.8),(48,7,'996蓝色（口袋有拉链）',39.9,1,'http://gd4.alicdn.com/imgextra/i3/154988527/TB2qqi8amKI.eBjy1zcXXXIOpXa_!!154988527.jpg_100x100.jpg',79.8),(49,7,'997黑绿（口袋有拉链）',39.9,1,'http://gd4.alicdn.com/imgextra/i2/154988527/TB2Qk94ag1I.eBjSszeXXc2hpXa_!!154988527.jpg_100x100.jpg',79.8),(50,7,'997黑白（口袋有拉链）',39.9,1,'http://gd2.alicdn.com/imgextra/i1/154988527/TB2EHq4abWJ.eBjSspdXXXiXFXa_!!154988527.jpg_100x100.jpg',79.8),(51,NULL,'851黑色',59.99,1,NULL,119.98),(52,NULL,'1804黑绿',39.9,1,NULL,79.8),(53,NULL,'8071黑色',39.9,1,NULL,79.8),(54,NULL,'8071红色',39.9,1,NULL,79.8),(55,NULL,'8071蓝色',39.9,1,NULL,79.8),(56,NULL,'8072蓝色',39.9,1,NULL,79.8),(57,NULL,'8072黑色',39.9,1,NULL,79.8),(58,NULL,'8072红色',39.9,1,NULL,79.8),(59,NULL,'8072橙色',39.9,1,NULL,79.8),(60,NULL,'8072浅绿色',39.9,1,NULL,79.8),(61,NULL,'8070黑色',39.9,1,NULL,79.8),(62,NULL,'8070红色',39.9,1,NULL,79.8),(63,NULL,'8070蓝色',39.9,1,NULL,79.8),(64,NULL,'8070橙色',39.9,1,NULL,79.8),(65,NULL,'8070浅绿色',39.9,1,NULL,79.8),(66,NULL,'8070荧光绿',39.9,1,NULL,79.8),(67,NULL,'8070白色',39.9,1,NULL,79.8),(68,NULL,'164深蓝色',25.9,1,NULL,51.8),(69,NULL,'164深绿色',25.9,1,NULL,51.8),(70,NULL,'164红色',25.9,1,NULL,51.8),(71,NULL,'164?灰色',25.9,1,NULL,51.8),(72,NULL,'167白色',25.9,1,NULL,51.8),(73,NULL,'167黑色',25.9,1,NULL,51.8),(74,NULL,'167灰色',25.9,1,NULL,51.8),(75,NULL,'167红色',25.9,1,NULL,51.8),(76,NULL,'167橙色',25.9,1,NULL,51.8),(77,NULL,'167蓝色',25.9,1,NULL,51.8),(78,NULL,'167荧光绿',25.9,1,NULL,51.8),(79,NULL,'169灰色',33.9,1,NULL,67.8),(80,NULL,'171黑色',33.9,1,NULL,67.8),(81,NULL,'171白色',33.9,1,NULL,67.8),(82,NULL,'171红色',33.9,1,NULL,67.8),(83,NULL,'171灰色',33.9,1,NULL,67.8),(84,NULL,'712浅灰色',48.9,1,NULL,97.8),(85,NULL,'712深灰色',48.9,1,NULL,97.8),(86,NULL,'712浅蓝色',48.9,1,NULL,97.8),(87,NULL,'712深蓝色',48.9,1,NULL,97.8),(88,NULL,'722-822套装 浅灰色',99.9,1,NULL,199.8),(89,NULL,'722-822套装 深灰色',99.9,1,NULL,199.8),(90,NULL,'751灰色',59.9,1,NULL,119.8),(91,NULL,'751黑色',59.9,1,NULL,119.8),(92,NULL,'761黑色',59.9,1,NULL,119.8),(93,NULL,'781黑色迷彩',79.9,1,NULL,159.8),(94,NULL,'9030黑黑',35.9,1,NULL,71.8),(95,NULL,'9030黑灰',35.9,1,NULL,71.8);

/*Table structure for table `sport_wear_order` */

DROP TABLE IF EXISTS `sport_wear_order`;

CREATE TABLE `sport_wear_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(30) DEFAULT NULL,
  `wear_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `total_price` double DEFAULT NULL COMMENT '支付金额',
  `pay_status` int(11) DEFAULT '0' COMMENT '0支付中，1已支付，2支付失败，3退款中，4已退款，-1取消订单',
  `paymsg` varchar(50) DEFAULT NULL COMMENT '支付状态描述',
  `delivery` int(11) DEFAULT '0' COMMENT '0未发货，1发货中，2已收货',
  `create_time` datetime DEFAULT NULL,
  `form_id` varchar(50) DEFAULT NULL,
  `prepay_id` varchar(50) DEFAULT NULL,
  `out_trade_no` varchar(30) DEFAULT NULL,
  `post_id` varchar(20) DEFAULT NULL COMMENT '物流单号',
  `post_type` varchar(50) DEFAULT NULL COMMENT '物流公司',
  `comment` varchar(200) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sport_wear_order` */

/*Table structure for table `sport_wear_user` */

DROP TABLE IF EXISTS `sport_wear_user`;

CREATE TABLE `sport_wear_user` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(30) DEFAULT NULL,
  `nick_name` varchar(100) DEFAULT NULL,
  `avatar_url` varchar(200) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sport_wear_user` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `flag` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `wxpublicname` varchar(255) DEFAULT NULL,
  `wxpublicno` varchar(255) DEFAULT NULL,
  `openid` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`password`,`username`,`flag`,`level`,`wxpublicname`,`wxpublicno`,`openid`,`create_time`) values (1,'w3C6_E8__NeCLqeFVmvSZQ','admin',1,1,NULL,NULL,NULL,'2021-12-26 14:00:00');

/*Table structure for table `wechat_user` */

DROP TABLE IF EXISTS `wechat_user`;

CREATE TABLE `wechat_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `open_id` varchar(64) DEFAULT NULL COMMENT '用户ID',
  `nick_name` varchar(100) DEFAULT NULL COMMENT '用户昵称',
  `avatar_url` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `gender` int(11) DEFAULT NULL COMMENT '用户性别：1男，2女，0未知',
  `city` varchar(100) DEFAULT NULL COMMENT '所在城市',
  `country` varchar(255) DEFAULT NULL COMMENT '所属国籍',
  `province` varchar(50) DEFAULT NULL COMMENT '所在省份',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表';

/*Data for the table `wechat_user` */

/*Table structure for table `weixin_pubno` */

DROP TABLE IF EXISTS `weixin_pubno`;

CREATE TABLE `weixin_pubno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pubno` varchar(20) DEFAULT NULL,
  `pubno_name` varchar(20) DEFAULT NULL,
  `pubno_type` varchar(10) DEFAULT NULL,
  `appid` varchar(20) DEFAULT NULL,
  `appsecret` varchar(50) DEFAULT NULL,
  `token` varchar(32) DEFAULT NULL,
  `interface_url` varchar(255) DEFAULT NULL,
  `operator` varchar(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `mch_id` varchar(20) DEFAULT NULL,
  `mch_key` varchar(32) DEFAULT NULL,
  `notify_url` varchar(255) DEFAULT NULL,
  `spbill_ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `weixin_pubno` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
