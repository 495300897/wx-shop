DROP TABLE IF EXISTS "access_token";
CREATE TABLE access_token (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    appid TEXT DEFAULT NULL,
    appsecret TEXT DEFAULT NULL,
    token TEXT DEFAULT NULL,
    create_time TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS "banner";
CREATE TABLE banner (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    image_url TEXT NOT NULL DEFAULT '',
    status INTEGER NOT NULL DEFAULT 0,
    create_time TEXT DEFAULT NULL,
    update_time TEXT DEFAULT NULL,
    sort_num INTEGER DEFAULT 0
);

DROP TABLE IF EXISTS "first_join_key";
CREATE TABLE first_join_key (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    create_time TEXT DEFAULT NULL,
    create_user TEXT DEFAULT NULL,
    image_text_no INTEGER DEFAULT NULL,
    operator_name TEXT DEFAULT NULL,
    re_type TEXT DEFAULT NULL,
    re_text TEXT DEFAULT NULL,
    title TEXT DEFAULT NULL,
    vchar1 TEXT DEFAULT NULL,
    vchar2 TEXT DEFAULT NULL,
    vchar3 TEXT DEFAULT NULL,
    vchar4 TEXT DEFAULT NULL,
    weixin_public_no TEXT DEFAULT NULL,
    wx_name TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS "image_text_info";
CREATE TABLE image_text_info (
    image_text_no INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT DEFAULT NULL,
    click_out_url TEXT DEFAULT NULL,
    image_url TEXT DEFAULT NULL,
    click_url TEXT DEFAULT NULL,
    digest TEXT DEFAULT NULL,
    image_text_type TEXT DEFAULT NULL,
    main_text TEXT DEFAULT NULL,
    operator TEXT DEFAULT NULL,
    source_url TEXT DEFAULT NULL,
    weixin_public_no TEXT DEFAULT NULL,
    create_time TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS "sport_wear";
CREATE TABLE sport_wear (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT DEFAULT NULL,
    sizes TEXT DEFAULT NULL,
    price TEXT DEFAULT NULL,
    orig_price TEXT DEFAULT NULL,
    cover_urls TEXT DEFAULT NULL,
    info_urls TEXT DEFAULT NULL,
    wear_type TEXT DEFAULT NULL,
    info_text TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS "sport_wear_addr";
CREATE TABLE sport_wear_addr (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    uid TEXT DEFAULT NULL,
    name TEXT DEFAULT NULL,
    phone TEXT DEFAULT NULL,
    address TEXT DEFAULT NULL,
    create_time TEXT DEFAULT NULL,
    flag INTEGER DEFAULT 1
);

DROP TABLE IF EXISTS "sport_wear_info";
CREATE TABLE sport_wear_info (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    wear_id INTEGER DEFAULT NULL,
    color TEXT DEFAULT NULL,
    price REAL DEFAULT NULL,
    stock INTEGER DEFAULT 1,
    color_url TEXT DEFAULT NULL,
    orig_price REAL DEFAULT NULL
);

DROP TABLE IF EXISTS "sport_wear_order";
CREATE TABLE sport_wear_order (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    uid TEXT DEFAULT NULL,
    wear_id INTEGER DEFAULT NULL,
    address_id INTEGER DEFAULT NULL,
    title TEXT DEFAULT NULL,
    size TEXT DEFAULT NULL,
    color TEXT DEFAULT NULL,
    price REAL DEFAULT NULL,
    count INTEGER DEFAULT NULL,
    total_price REAL DEFAULT NULL,
    pay_status INTEGER DEFAULT 0,
    paymsg TEXT DEFAULT NULL,
    delivery INTEGER DEFAULT 0,
    create_time TEXT DEFAULT NULL,
    form_id TEXT DEFAULT NULL,
    prepay_id TEXT DEFAULT NULL,
    out_trade_no TEXT DEFAULT NULL,
    post_id TEXT DEFAULT NULL,
    post_type TEXT DEFAULT NULL,
    comment TEXT DEFAULT NULL,
    image_url TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS "sport_wear_user";
CREATE TABLE sport_wear_user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    uid TEXT DEFAULT NULL,
    nick_name TEXT DEFAULT NULL,
    avatar_url TEXT DEFAULT NULL,
    country TEXT DEFAULT NULL,
    province TEXT DEFAULT NULL,
    city TEXT DEFAULT NULL,
    gender INTEGER DEFAULT NULL,
    create_time TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS "user";
CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    password TEXT DEFAULT NULL,
    username TEXT DEFAULT NULL,
    flag INTEGER NOT NULL,
    level INTEGER NOT NULL,
    wxpublicname TEXT DEFAULT NULL,
    wxpublicno TEXT DEFAULT NULL,
    openid TEXT DEFAULT NULL,
    create_time TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS "weixin_pubno";
CREATE TABLE weixin_pubno (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    pubno TEXT DEFAULT NULL,
    pubno_name TEXT DEFAULT NULL,
    pubno_type TEXT DEFAULT NULL,
    appid TEXT DEFAULT NULL,
    appsecret TEXT DEFAULT NULL,
    token TEXT DEFAULT NULL,
    interface_url TEXT DEFAULT NULL,
    operator TEXT DEFAULT NULL,
    create_time TEXT DEFAULT NULL,
    mch_id TEXT DEFAULT NULL,
    mch_key TEXT DEFAULT NULL,
    notify_url TEXT DEFAULT NULL,
    spbill_ip TEXT DEFAULT NULL
);



