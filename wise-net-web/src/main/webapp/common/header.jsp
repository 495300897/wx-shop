<%@ page pageEncoding="UTF-8"%>

<div class="navbar">
	<div class="navbar-inner">
		<ul class="nav pull-right">
			<!-- <li>
				<a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">设置</a>
			</li> -->
			<li id="fat-menu" class="dropdown">
				<a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
					<i class="icon-user"></i> ${user.username} <i class="icon-caret-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li><a tabindex="-1" href="${path}/user/editUI?type=modify">修改密码</a></li>
					<li class="divider"></li>
					<li><a tabindex="-1" href="${path}/logout">退出</a></li>
				</ul>
			</li>
		</ul>
		<a class="brand" target="_blank" href="#">
			<span class="first">公众号小程序</span>
			<span class="second">管理平台</span>
		</a>
	</div>
</div>

<script type="text/javascript">
	const path = '${path}';
	document.title = '公众号小程序管理平台';
	// 获取url参数
	function getQuery(name) {
		const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
		const r = window.location.search.substr(1).match(reg);
		return r != null ? unescape(decodeURIComponent(r[2])) : null;
	}
	// 获取表单数据
	function getFormData(formId) {
		const dataArray = $('#' + formId).serializeArray(), formData = {};
		$.each(dataArray, function() {
			formData[this.name] = this.value;
		});
		return formData;
	}
	$(function() {
		// 返回上一页
		$("#cancel-btn").click(function(event) {
			history.back();
		});
	});
</script>