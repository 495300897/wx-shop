<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.wisenet.common.Const"%>
<%@ page import="com.wisenet.entity.User"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	request.setAttribute("path", path);
	request.setAttribute("basePath", basePath);
	// 登录信息
	User user = (User) request.getSession().getAttribute(Const.LOGIN_USER_ATTRIBUTE);
	request.setAttribute("user", user);
%>