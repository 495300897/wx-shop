<%@ page pageEncoding="UTF-8"%>

<div class="sidebar-nav">
	<a href="#calc-menu" class="nav-header" data-toggle="collapse">
		<i class="icon-bar-chart"></i>运动服专卖<i class="icon-chevron-up"></i>
	</a>
	<ul id="calc-menu" class="nav nav-list collapse">
		<li><a href="${path}/orderManage/list">订单管理</a></li>
		<li><a href="${path}/sportWear/goodsList">商品管理</a></li>
		<li><a href="${path}/sportWear/bannerList">BANNER管理</a></li>
		<li><a href="https://mpkf.weixin.qq.com/cgi-bin/kfloginpage" target="_blank">客服管理</a></li>
	</ul>

	<a href="#dashboard-menu" class="nav-header" data-toggle="collapse">
		<i class="icon-check"></i>公众号管理<i class="icon-chevron-up"></i>
	</a>
	<ul id="dashboard-menu" class="nav nav-list collapse">
		<li><a href="${path}/wxpubno/list">微信账号列表</a></li>
		<li><a href="${path}/menu/wxtreeUI">自定义菜单</a></li>
		<li><a href="${path}/user/editUI?type=add">添加登录用户</a></li>
		<li><a href="${path}/keyset/list">关键字回复</a></li>
		<li><a href="${path}/firstKey/firstKeyUI">首次关注回复</a></li>
		<li><a href="${path}/imagetextinfo/list">图文素材</a></li>
		<li><a href="${path}/wxpubno/JSSDKUI">添加分享配置</a></li>
		<li><a href="${path}/wxpubno/oauthUI">网页授权配置</a></li>
	</ul>
	
	<script type="text/javascript">
		let curtNavHeader;
		const parent = getQuery('parent');
		const pathname = location.pathname;

		// 二级菜单 （collapse折叠，collapse in展开）
		const list = $('.sidebar-nav ul li');
		$.each(list, function(k, v) {
			const ulNode = $(this).context.parentNode;
			const aNode = $(this).context.childNodes[0];
			curtNavHeader = '#' + ulNode.id;
			if (aNode.href.indexOf(pathname) > -1 || aNode.href.indexOf(parent) > -1) {
				// 二级展开
				$(curtNavHeader).addClass('collapse in');
				$(aNode).css('color', '#ffffff').css('text-shadow', 'none').css('background', '#6699ff');
			} else {
				// 二级折叠
				$(curtNavHeader).addClass('collapse');
			}
		});
		
		// 一级菜单（collapsed折叠）
		const nav = $('.sidebar-nav .nav-header');
		$.each(nav, function(k, v) {
			if (curtNavHeader === v.hash) {
				// 展开
				$(v).removeClass('collapsed');
			} else {
				// 折叠
				$(v).addClass('collapsed');
			}
		});
	</script>
</div>
