<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<%@ include file="/common/inc.jsp"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>fzhwx开发者-公众号管理平台</title>
    <link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${path}/css/wxmenu.css" type="text/css"/>
    <link rel="stylesheet" href="${path}/css/appmsg.css"/>
    <link rel="stylesheet" href="${path}/css/zTreeStyle/zTreeStyle.css" type="text/css"/>
    <script type="text/javascript" src="${path}/js/jquery.ztree.core-3.5.js"></script>
    <script type="text/javascript" src="${path}/js/jquery.ztree.exedit-3.5.js"></script>
    <script type="text/javascript">
      // 请求路径、获取菜单
      var path = "${path}";
      $(function() {
    	  $("#pubno").change(function() {
    		 $('#rndiv').hide();
    		 var pubno = $("#pubno").val();
    		 if (pubno != 0) {
    			 $('#pubnoBtn').show();
    			 $('input[name=weixinPublicNo]').val(pubno);
    		 	 $('.pre_nav_name').html($("#pubno option:selected").text());
    			 getMenu(pubno);
    		 } else {
    			 $('#pubnoBtn').hide();
    			 initMenu([]);
    		 }
    	  });
      });
    </script>
    <script type="text/javascript" src="${path}/js/wxtree.js"></script>
  </head>
  
  <body leftmargin="0" topmargin="0">
    <jsp:include page="/common/header.jsp" flush="true"/>
    <jsp:include page="/common/leftbar.jsp" flush="true"/>
    
    <div class="content">
      <div class="container-fluid">
        <fieldset>
          <legend>自定义菜单管理</legend>
        </fieldset>
        
        <!-- 选择公众号 -->
        <div class="control-group">
			<select id="pubno" style="width:224px;margin-left:20px;">
				<option value="0">请选择公众号</option>
				<c:forEach items="${list}" var="entry">
					<option value="${entry.pubno}">${entry.pubnoName}</option>
				</c:forEach>
			</select>
			<div id="pubnoBtn" style="display:none;margin:-40px 0 20px 300px;">
				<button type="button" class="btn" onclick="previewMenu();">预 览</button>
            	<button type="button" class="btn" style="margin-left:20px;" onclick="createMenu();">发 布</button>
			</div>
		</div>
				
        <!-- 菜单区 -->
        <div class="msg-preview" style="float:left;margin-top:10px;">
          <div class="msg-item" style="width:220px;height:400px;border:#c8c8cb 1px solid;
          	   padding-top: 1px;padding-left:1%;margin-top:-20px;margin-left:20px;">
            <ul id="treeDemo" class="ztree"></ul>
          </div>
        </div>
        
        <input id="trid" type="hidden"/>
        <!--编辑区-->
        <form method="post" action="${path}/menu/saveOrUpdateMenu" onsubmit="return checkForm();">
          <input name="id" type="hidden" value="0"/>
          <input name="weixinPublicNo" type="hidden"/>
          <input name="menuId" type="hidden" value="0"/>
          <input name="parentMenuId" type="hidden" value="0"/>
          <input name="refImageTextNo" type="hidden" value="0"/>
          <div id="rndiv" style="margin:-10px 0 0 300px;width:700px;display:none;">
            <div class="msg-editer-wrapper">
              <div class="msg-editer">
                <div style="line-height:50px">
                                               菜单名称：
                  <input name="name" type="text" style="margin-left:4px;border-radius:5px;height:30px;"/>
                  <span class="maroon">*</span>
                  <span class="help-inline">(必填，不能超过10个字)</span>
                </div>
                <div style="line-height:40px">
                                               菜单事件：
                  <select name="type" id="type" style="margin-left:4px;width:153px;" onchange="changeMenuType(this)">
                    <option value="click_text">文 本</option>
                    <option value="click_imgtxt">图 文</option>
                    <option value="view">外 链</option>
                    <option value="pic_weixin">微信相册</option>
                    <option value="pic_sysphoto">系统相机</option>
                    <option value="pic_photo_or_album">拍照/相册</option>
                    <option value="miniprogram">小程序</option>
                    <option value="scancode_push">扫码推送</option>
                    <option value="scancode_waitmsg">扫码接收</option>
                    <option value="location_select">地理位置</option>
                  </select>
                  <span class="maroon">*</span><span class="help-inline">(点击菜单回复的选项)</span>
                </div>
                <div id="click_text" class="control-group">
                  	<p style="margin-top:5px">
					  	菜单KEY：<input name="mkey" type="text" placeholder="填写数字" style="margin-left:7px;border-radius:5px;height:30px;"/>                
                    </p>                   
                                                     文本内容：<p><p/>
                    <textarea name="refText" style="width:450px;margin-left:78px;resize:none;"
                      			rows="5" id="content"></textarea>
                </div>  
                  <div id="click_imgtxt" style="display:none;line-height:40px">
                   	 图文预览：
                    <input type="button" class="btn" value="选择图文" onclick="selectImageText();"/>
                    <input type="hidden" id="imghtml"/>
                    <div id="preViewDiv" style="display:none;border:none"></div>
                  </div>
                  <div id="view" style="display:none;line-height:50px">
                                                    页面URL：
                    <input name="url" type="text" style="margin-left:4px;width:400px;border-radius:5px;height:30px;"/>
                    <span class="maroon">*</span>
                    <span class="help-inline">(必须以http://开头)</span>
                  </div>
                  <div id="miniprogram" style="display:none;line-height:50px;">
                   	 小程序配置：
                    <p>
                      appid： <input name="appid" type="text" style="margin-left:24px;width:400px;border-radius:5px;height:30px;"/>
                    </p>
                      pagepath： <input name="pagepath" type="text" style="width:400px;border-radius:5px;height:30px;"/>
                  </div>
                  <p style="margin-top:30px;">
                    <input type="submit" style="width:60px;" value="保 存" class="btn-mine"/>
                  </p>
              </div>
            </div>
          </div>
        </form>
        
        <!--自定义菜单预览-->
        <div id="preview_box" class="dialogBox" style="display:none;">
          <div class="background"></div>
          <div class="pre_wrapper">
            <div class="pre_hd">
              <h4 class="pre_nav_name"></h4>
            </div>
            <div class="pre_bd" id="previewAction"></div>
            <div class="pre_ft">
              <div id="pre_nav_wrapper" class="pre_nav_wrapper group screen1"></div>
            </div>
            <span class="pre_windows_opr">
              <i id="pre_close" class="opr_icon closed" onclick="closePreView();"></i>
            </span>
          </div>
        </div>
        
      </div>
    </div>
    <jsp:include page="/common/footer.jsp" flush="true"/>
  </body>

</html>
