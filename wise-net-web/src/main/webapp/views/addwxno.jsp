<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<jsp:include page="/common/global.jsp" flush="true" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>fzhwx开发者-公众号管理平台</title>
<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
</head>

<body>
	<jsp:include page="/common/header.jsp" flush="true" />
	<jsp:include page="/common/leftbar.jsp" flush="true" />
	<!-- <div id="bg" title="完成配置接口即可使用全部功能" class="bgcover"></div>  -->
	<div class="content">
		<div class="container-fluid">
		<fieldset>
			<legend>配置公众账号信息</legend>
		</fieldset>
		<form id="dataform" action="${path}/wxpubno/saveWxno" method="post">
			<input type="hidden" id="wxno" name="id" value="${wxpnlist.id}"/>
			<table>
				<tr>
					<td>公众号名称 <font color="red">*</font> ：</td>
					<td>
						<input required="required" style="height:30px;border-radius:5px;" type="text" name="companyName" value="${wxpnlist.companyName}"/>
					</td>
				</tr>
				<tr>
					<td>公众号原始ID <font color="red">*</font> ：</td>
					<td>
						<input required="required" style="height:30px;border-radius:5px;" type="text" name="weixinPublicNo" value="${wxpnlist.weixinPublicNo}"/>
					</td>
				</tr>
				<tr>
					<td>APPID <font color="red">*</font> ：</td>
					<td>
						<input required="required" style="height:30px;border-radius:5px;" type="text" name="appid" value="${wxpnlist.appid}"/>
					</td>
				</tr>
				<tr>
					<td>APPSECRET <font color="red">*</font> ：</td>
					<td>
						<input required="required" style="height:30px;border-radius:5px;" type="text" name="appsecret" value="${wxpnlist.appsecret}"/>
					</td>
				</tr>
				<tr>
					<td>TOKEN <font color="red">*</font> ：</td>
					<td>
						<input required="required" style="height:30px;border-radius:5px;" type="text" name="token" value="${wxpnlist.token}"/>
					</td>
				</tr>
				<tr id="tr1" style="display:none;">
					<td>URL：</td>
					<td>
						<input name="interfaceUrl" style="height:30px;width:600px;border-radius:5px;" type="text" value="${basePath}${wxpnlist.interfaceUrl}"/>
						<p>
						<span id="tip" style="display:none;color:red;font-size:16px;font-weight:bold;">
							请将URL和TOKEN配置到你的公众账号！！！
							<a href="https://mp.weixin.qq.com/" target="_blank">登录配置</a>
						</span>
					</td>
				</tr>
				<tr>
				    <td></td>
					<td>
						<button class="btn-mine" type="submit">保 存</button>&nbsp;
						<button class="btn" type="reset">重 置</button>
					</td>
				</tr>
			</table>
		</form>
		</div>
	</div>
	<jsp:include page="/common/footer.jsp" flush="true" />
</body>

<script type="text/javascript">
	$(function() {
		const wxno = $('#wxno').val();
		if (wxno != null && wxno !== "") {
			$('#tr1').show();
		}
	});
</script>
	
</html>
