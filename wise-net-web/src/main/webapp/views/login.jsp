<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>公众号小程序管理平台</title>
<link rel="shortcut icon" href="<%=path%>/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/theme.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/appmsg.css">
<link rel="stylesheet" href="<%=path%>/lib/font-awesome/css/font-awesome.css">
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="<%=path%>/js/bootstrap.js" ></script>
<style type="text/css">
    .block {border: 1px solid #ccc; border-radius: 5px;}
    .block input { border-radius: 5px; }
    .login {float: right; color: #fff; background: #6699ff; border: none; height: 30px; padding: 0 15px; }
</style>
</head>

<body>
<div class="navbar">
	<div class="navbar-inner">
		<a class="brand" target="_blank" href="#">
			<span class="first">公众号小程序</span>
			<span class="second">管理平台</span>
		</a>
	</div>
</div>
    
<!-- 登录 -->
<div class="row-fluid" style="margin-top:-50px;">
    <div class="dialog">
        <div class="block">
            <p class="block-heading">用 户 登 录</p>
            <div class="block-body">
                <form action="<%=request.getContextPath()%>/login" method="post">
                    <label>账 号</label>
                    <input id="username" name="username" type="text" class="span12" style="border-radius: 5px;">
                    <label>密 码</label>
                    <input name="pwd" type="password" class="span12" style="border-radius:5px;">
                    <input type="submit" class="login" value="登 录"/>
                    <font id="msg" color="red">${msg}</font>
                    <label class="remember-me"><input type="checkbox"> 记住账号</label>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <p class="pull-right"><a href="#">注 册</a></p>
        <p><a href="#">忘记密码?</a></p>
    </div>
</div>
</body>
</html>