<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>管理平台</title>
	<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
	<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
	<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
	<link rel="stylesheet" href="${path}/css/appmsg.css"/>
	<style type="text/css">
		label { display: inline-block; }
		.help-inline { vertical-align: top; }
	</style>
</head>
<body>
	<jsp:include page="/common/header.jsp" flush="true" />
	<jsp:include page="/common/leftbar.jsp" flush="true" />
	<div class="content">
		<div class="container-fluid">
			<fieldset>
				<legend id="actTitle">新增登录用户</legend>
			</fieldset>
			<div style="padding-top:20px;padding-bottom:20px;">
				<div class="span6">
					<div class="msg-editer-wrapper">
						<div class="msg-editer">
							<form id="appmsg-form" class="form">
								<div class="control-group">
									<label class="control-label">用户名称</label>
									<span class="maroon">*</span><span class="help-inline">(必填，不能超过20个字符)</span>
									<div class="controls">
										<input class="span5" type="text" name="username" maxlength="20" value="${user.username}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">登录密码</label>
									<span class="maroon">*</span><span class="help-inline">(必填，不能超过20个字符)</span>
									<div class="controls">
										<input class="span5" type="password" name="password" maxlength="20" value="${user.password}"/>
									</div>
								</div>
								<div class="control-group" align="center">
									<div class="controls">
										<button id="save-btn" type="button" class="btn-mine">保 存</button>&nbsp;&nbsp;
										<button id="cancel-btn" type="button" class="btn">取 消</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/common/footer.jsp" flush="true"/>
</body>
<script type="text/javascript">
const type = getQuery('type');
$(function() {
	if (type === 'add') {
		$("input[name=username]").val('');
		$("input[name=password]").val('');
	} else {
		$('#actTitle').html('修改用户密码');
		$("input[name=username]").attr('readonly', 'readonly');
	}
	// 保存
	$('#save-btn').click(function() {
		if (type === 'add') {
			saveUser();
		} else {
			updateUser();
		}
	});
	
	$('#cancel-btn').click(function() {
		history.back();
	});
});

function saveUser() {
	$.ajax({
        url: '${path}/user/saveUser',
        type: 'POST',
        data: $("#appmsg-form").serialize(),
        dataType: 'JSON',
        success: function(data) {
        	if (data.success) {
				alert("保存成功！");
				$(".btn-mine").attr("disabled", true);
				$(".btn-mine").addClass("btn");
			} else {
				alert(data.message);
			}
        },
        error: function(e) {
	        if (e.responseText === 'unlogin') {
	            location.href = '${path}/login';
	        }
	    }
	});
}

function updateUser() {
	$.ajax({
        url: '${path}/user/updatePassword',
        type: 'POST',
        data: $("#appmsg-form").serialize(),
        dataType: 'JSON',
        success: function(data) {
        	if (data.success) {
				$(".btn-mine").attr("disabled", true);
				$(".btn-mine").addClass("btn");
				alert("修改成功，请重新登录!");
				location.href = "${path}/logout";
			} else {
				alert(data.message);
			}
        },
        error: function(e) {
	        if (e.responseText === 'unlogin') {
	            location.href = '${path}/login';
	        }
	    }
	});
}
</script>
</html>