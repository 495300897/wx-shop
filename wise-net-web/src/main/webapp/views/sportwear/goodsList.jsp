<%@ page pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>fzhwx开发者-公众号管理平台</title>
<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
</head>

<body>
	<jsp:include page="/common/header.jsp" flush="true" />
	<jsp:include page="/common/leftbar.jsp" flush="true" />
	<div class="content">
		<div class="container-fluid">
			<form name="form" action="${path}/sportWear/goodsList">
				
				<fieldset>
					<legend>商品列表</legend>
				</fieldset>
				
				<div class="control-group">
					<div style="margin-top:-10px;">
						<button class="btn" style="margin-right:85px;" type="button" onclick="editUI('0');">新 增</button>
						<input name="title" value="${title}" placeHolder="输入商品名称" />
						<button class="btn" type="submit">查 询</button>
					</div>
				</div>
				
				<div align="center">
					<table class="table table-bordered table-hover table-condensed">
						<thead align="center">
							<tr>
								<th width="1%" style="text-align:center">
									<a href="javascript:void(0);">编  号</a>
								</th>
								<th width="5%" style="text-align:center">
									<a href="javascript:void(0);">商品名称</a>
								</th>
								<th width="2%" style="text-align:center">
									<a href="javascript:void(0);">商品价格</a>
								</th>
								<th width="2%" style="text-align:center">
									<a href="javascript:void(0);">商品原价</a>
								</th>
								<th width="5%" style="text-align:center">
									<a href="javascript:void(0);">商品描述</a>
								</th>
								<th width="5%" style="text-align:center">
									<a href="javascript:void(0);">操  作</a>
								</th>
							</tr>
						</thead>
						<tr>
							<c:if test="${fn:length(list) == 0}">
								<td colspan="6" style="text-align: center">未查询到相关记录！</td>
							</c:if>
						</tr>
						<c:forEach items="${list}" var="entry" varStatus="idx">
							<tr>
								<td style="text-align:center;">${idx.index + 1}</td>
								<td style="text-align:center">${entry.title}</td>
								<td style="text-align:center">${entry.price}</td>
								<td style="text-align:center">${entry.origPrice}</td>
								<td style="text-align:center">
									${fn:substring(entry.infoText, 0, 30)}...
								</td>
								<td style="text-align:center">
									<a href="javascript:editUI('${entry.id}');">编 辑</a> | 
									<a href="javascript:infoUI('${entry.id}');">参 数</a>
								</td>
							</tr>
						</c:forEach>
					</table>
					<jsp:include page="/common/page.jsp" flush="true" />
				</div>
			</form>
		</div>
	</div>
	<jsp:include page="/common/footer.jsp" flush="true" />
</body>
<script>
function editUI(id) {
	if (id != '') {
		location.href = '${path}/sportWear/goods/info?parent=sportWear/goodsList&id=' + id;
	} else {
		location.href = '${path}/sportWear/goods/info?parent=sportWear/goodsList';
	}
}
function infoUI(id) {
	location.href = '${path}/sportWear/infoList?parent=sportWear/goodsList&wearId=' + id;
}
</script>
</html>
