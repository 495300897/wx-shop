<%@ page pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>fzhwx开发者-公众号管理平台</title>
<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
<link rel="stylesheet" href="${path}/css/appmsg.css"/>
<script type="text/javascript" src="${path}/js/upload.js"></script>
<script type="text/javascript" src="${path}/js/html-helper.js"></script>
<script type="text/javascript" src="${path}/js/sportwear/goods.js"></script>
<script type="text/javascript" src="${path}/operamasks-ui-2.0/js/operamasks-ui.min.js"></script>
<style type="text/css">
	label { display: inline-block; }
	.row { padding-bottom: 20px; }
	.help-inline { vertical-align: top; }
</style>
</head>

<body>
	<jsp:include page="/common/header.jsp" flush="true" />
	<jsp:include page="/common/leftbar.jsp" flush="true" />
	
	<div class="content">
		<div class="container-fluid">
			<fieldset>
				<legend>商品管理</legend>
			</fieldset>
			
			<div class="row">
				<div class="span5 msg-preview">
					<div class="msg-item-wrapper">
						<div class="msg-item">
							<div class="cover">
								<div id="coverTip" class="default-tip">封面图片</div>
								<img class="i-img" src="${sportWear.imageUrl}"/>
							</div>
						</div>
					</div>
				</div>
	
				<div class="span6">
					<div class="msg-editer-wrapper">
						<div class="msg-editer">
							<form id="appmsg-form" class="form">
								<input type="hidden" name="folder" value="rank"/>
								<input type="hidden" name="id" value="${sportWear.id}"/>
								<input type="hidden" id="imageUrl" value="${sportWear.imageUrl}"/>
								
								<div class="control-group">
									<label class="control-label">商品标题：</label>
									<span class="maroon">*</span>
									<div class="controls">
										<input name="title" type="text" value="${sportWear.title}" class="span5" style="width:540px;"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">商品描述：</label>
									<span class="maroon">*</span>
									<div class="controls">
										<textarea name="infoText" rows="5" style="width:540px;resize:none;">${sportWear.infoText}</textarea>
									</div>
								</div>
								<div class="control-group">
									<div style="float:left;margin-right:112px;">
										<label class="control-label">出售价格：</label>
										<span class="maroon">*</span>
										<input name="price" type="number" step="1.01" value="${sportWear.price}" min="1.0" class="span5" style="width:120px;"/>
									</div>
									<div>
										<label class="control-label">商品原价：</label>
										<span class="maroon">*</span>
										<input name="origPrice" type="number" step="1.01" value="${sportWear.origPrice}" min="1.0" class="span5" style="width:120px;"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">商品尺寸：</label>
									<span class="maroon">*</span><span class="help-inline">(多个用,号分隔)</span>
									<div class="controls">
										<input name="sizes" type="text" value="${sportWear.sizes}" class="span5" style="width:540px;"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">封面图片</label>
									<span class="maroon">*</span><span class="help-inline">(必须上传一张图片)</span>
									<div class="controls">
										<input type="file" style="width:73px;" id="coverFile" name="myimage" onchange="uploadCovers('coverFile')"/>
										<div id="loading" style="display:none;position:fixed;bottom:100px;">
		                                    <img src="<%=path%>/images/load.gif"/>
		                                </div>
										<textarea name="coverUrls" rows="5" style="width:540px;resize:none;">${sportWear.coverUrls}</textarea>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">详情图片</label>
									<span class="maroon">*</span><span class="help-inline">(必须上传一张图片)</span>
									<div class="controls">
										<input type="file" style="width:73px;" id="infoFile" name="myimage" onchange="uploadInfoUrls('infoFile')"/>
										<div id="loading" style="display:none;position:fixed;bottom:100px;">
		                                    <img src="<%=path%>/images/load.gif"/>
		                                </div>
										<textarea name="infoUrls" rows="5" style="width:540px;resize:none;">${sportWear.infoUrls}</textarea>
									</div>		
								</div>
								<div class="control-group" style="margin:20px 0 -20px 0;">
									<button id="save-btn" type="submit" class="btn-mine">保 存</button>
									<button id="cancel-btn" type="button" class="btn">取 消</button>
								</div>
							</form>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
	
	<jsp:include page="/common/footer.jsp" flush="true" />
</body>
</html>