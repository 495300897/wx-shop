<%@ page pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>fzhwx开发者-公众号管理平台</title>
<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
</head>

<body>
	<jsp:include page="/common/header.jsp" flush="true" />
	<jsp:include page="/common/leftbar.jsp" flush="true" />
	<div class="content">
		<div class="container-fluid">
			<form name="form" action="${path}/orderManage/list">
				<input type="hidden" name="delivery" value="${order.delivery}"/>
				<input type="hidden" name="payStatus" value="${order.payStatus}"/>
				
				<fieldset>
					<legend>订单列表</legend>
				</fieldset>
				
				<div class="control-group">
					<select id="payStatus" style="width:100px;">
						<option value="0">支付中</option>
						<option value="1">已支付</option>
						<option value="2">支付失败</option>
						<option value="3">退款中</option>
						<option value="4">已退款</option>
						<option value="-1">删除订单</option>
					</select>
					
					<select id="delivery" style="width:100px;">
						<option value="0">未发货</option>
						<option value="1">发货中</option>
						<option value="2">已收货</option>
					</select>
					
					<div style="margin-top:-40px; margin-left:230px;">
						<input name="uid" value="${order.uid}" placeHolder="输入用户ID" />&nbsp;&nbsp;
						<input name="outTradeNo" value="${order.outTradeNo}" placeHolder="输入订单号" />&nbsp;&nbsp;
						<input name="postId" value="${order.postId}" placeHolder="输入快递单号" />
						<button class="btn" type="submit">查 询</button>
					</div>
				</div>
				
				<div align="center">
					<table class="table table-bordered table-hover table-condensed">
						<thead align="center">
							<tr>
								<th width="3%" style="text-align:center">
									<a href="javascript:void(0);">编号</a>
								</th>
								<th width="10%" style="text-align:center">
									<a href="javascript:void(0);">用户ID</a>
								</th>
								<th width="3%" style="text-align:center">
									<a href="javascript:void(0);">订单编号</a>
								</th>
								<th width="3%" style="text-align:center">
									<a href="javascript:void(0);">快递单号</a>
								</th>
								<th width="5%" style="text-align:center">
									<a href="javascript:void(0);">支付状态</a>
								</th>
								<th width="6%" style="text-align:center">
									<a href="javascript:void(0);">发货状态</a>
								</th>
								<th width="8%" style="text-align:center">
									<a href="javascript:void(0);">操作时间</a>
								</th>
							</tr>
						</thead>
						<tr>
							<c:if test="${fn:length(list) == 0}">
								<td colspan="7" style="text-align: center">未查询到相关记录！</td>
							</c:if>
						</tr>
						<c:forEach items="${list}" var="entry" varStatus="idx">
							<tr>
								<td style="text-align:center;cursor:pointer;"
									onclick="openInfo('${entry.addressId}','${entry.title}','${entry.size}','${entry.color}','${entry.count}','${entry.price}','${entry.totalPrice}','${entry.comment}');">
									<a href="javascript:void(0);">${idx.index+1}</a>
								</td>
								<td style="text-align:center">${entry.uid}</td>
								<td style="text-align:center">${entry.outTradeNo}</td>
								<td style="text-align:center">${entry.postId}</td>
								<td style="text-align:center">
									<c:if test="${entry.payStatus == 0}">未支付</c:if>
									<c:if test="${entry.payStatus == 1}">已支付</c:if>
									<c:if test="${entry.payStatus == 2}">支付失败</c:if>
									<c:if test="${entry.payStatus == 3}">退款中</c:if>
									<c:if test="${entry.payStatus == 4}">已退款</c:if>
									<c:if test="${entry.payStatus == -1}">删除订单</c:if>
								</td>
								<td style="text-align: center">
									<c:if test="${entry.delivery == 0 && entry.payStatus == 1}">
										<button onclick="setValue('${entry.id}','${entry.uid}','1','${entry.outTradeNo}');" class="btn btn-lg" data-toggle="modal" data-target="#myModal">发货</button>
									</c:if>
									<c:if test="${entry.delivery == 1 && entry.payStatus == 1}">
										<button onclick="updateDelivery('${entry.id}','${entry.uid}','2','${entry.outTradeNo}');" class="btn btn-lg">确认收货</button>
									</c:if>
									<c:if test="${entry.delivery == 2 && entry.payStatus == 1}">
										已收货
									</c:if>
									<c:if test="${entry.delivery == 0 && entry.payStatus == 0}">
										未支付
									</c:if>
								</td>
								<td style="text-align:center">${entry.createTime}</td>
							</tr>
						</c:forEach>
					</table>
					<jsp:include page="/common/page.jsp" flush="true" />
				</div>
			</form>
			<!-- 订单详情模态框 -->
			<div class="modal fade" id="infoModal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">订单详情</h4>
						</div>
						<div class="modal-body">
							<div class="control-group" id="orderInfo"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>
			</div>
			<!-- 录入快递单号模态框 -->
			<div class="modal fade" id="myModal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">录入快递单号</h4>
						</div>
						<div class="modal-body">
							<div class="control-group">
								物流公司： 
								<select id="post_type">
									<option value="yuantong">圆通</option>
									<option value="shentong">申通</option>
								</select>
							</div>
							<div class="control-group">
								<div class="controls">
									快递单号：
									<input id="postId" style="border-radius:5px" type="text" placeHolder="填写快递单号" />
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button onclick="updateDelivery('','','','');" class="btn">提交</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/common/footer.jsp" flush="true" />
</body>
<script>
	// 下拉框保值
	$(function() {
		var val = "${order.payStatus}";
		$("#payStatus").change(function() {
			val = $("#payStatus").val();
			location.href = '${path}/orderManage/list?page=${page}&payStatus=' + val;
		});
		// 设置选中值
		$("#payStatus").val(val).attr("selected", true);
	});
	
	$(function() {
		var val = "${order.delivery}";
		$("#delivery").change(function() {
			val = $("#delivery").val();
			location.href = '${path}/orderManage/list?page=${page}&delivery=' + val;
		});
		// 设置选中值
		$("#delivery").val(val).attr("selected", true);
	});
	
	// 模态框不在循环中设值
	var arr = null;
	function setValue(id, uid, delivery, outTradeNo) {
		arr = [ id, uid, delivery, outTradeNo ];
	}
	
	// 查看详情
	function openInfo(addressId,title,size,color,count,price,totalPrice,comment) {
		var html = "商品名称：" + title + "<br>商品颜色：" + color + "<br>商品尺码：" + size
		         + "<br>商品单价：" + price + "元<br>商品数量：" + count + "<br>商品总价：" + totalPrice;
		$("#orderInfo").html(html);
		$.get('${path}/sport/addr/getAddress?id=' + addressId,
			function(res) {
				var _html = "元<br></br>收货人信息<br>用户名：" + res.address.name
						  + "<br>手机号：" + res.address.phone
						  + "<br>收货地址：" + res.address.address
						  + "<br>买家留言：" + (comment == '' ? '无' : comment);
				$("#orderInfo").append(_html);
			}
		);
		$("#infoModal").modal("show");
	}
	
	// 更新物流信息
	function updateDelivery(id, uid, delivery, outTradeNo) {
		var postId = $('#postId').val();
		var postType = $('#post_type').val();
		if (delivery == 0 && postId == '') {
			alert('请填写快递单号！');
			return;
		}
		// 模态框不在循环中设值
		if (id == '' && uid == '' && delivery == '' && outTradeNo == '') {
			id = arr[0];
			uid = arr[1];
			delivery = arr[2];
			outTradeNo = arr[3];
		}
		var data = {
			id: id, uid: uid, delivery: delivery,
			outTradeNo: outTradeNo, postId: postId, postType: postType
	    };
		$.ajax({
		    url: "${path}/orderManage/updateDelivery",
		    data: JSON.stringify(data),
		    type: "POST",
		    dataType: "json",
		    contentType: "application/json",
            success: function(data) {
            	if (data.success) {
            		alert('操作成功！');
            		location.reload();
            	} else {
            		if (data.message == 'please try again') {
            			alert('请重试！');
            		} else {
            			alert('操作失败！');
            		}
            	}
		    },
		    error: function(e) {
		        if (e.responseText == 'unlogin') {
		            location.href = '${path}/login';
		        }
		    }
		 });
	}
</script>
</html>
