<%@ page pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>fzhwx开发者-公众号管理平台</title>
<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
<link rel="stylesheet" href="${path}/css/appmsg.css"/>
<script type="text/javascript" src="${path}/js/upload.js"></script>
<script type="text/javascript" src="${path}/js/html-helper.js"></script>
<script type="text/javascript" src="${path}/js/sportwear/banner.js"></script>
<script type="text/javascript" src="${path}/operamasks-ui-2.0/js/operamasks-ui.min.js"></script>
<style type="text/css">
	label { display: inline-block; }
	.help-inline { vertical-align: top; }
</style>
</head>

<body>
	<jsp:include page="/common/header.jsp" flush="true" />
	<jsp:include page="/common/leftbar.jsp" flush="true" />
	
	<div class="content">
		<div class="container-fluid">
			<fieldset>
				<legend>BANNER管理</legend>
			</fieldset>
			
			<div class="row">
				<div class="span5 msg-preview">
					<div class="msg-item-wrapper">
						<div class="msg-item">
							<div class="cover">
								<div id="coverTip" class="default-tip">封面图片</div>
								<img class="i-img" src="${banner.imageUrl}"/>
							</div>
						</div>
					</div>
				</div>
	
				<div class="span6">
					<div class="msg-editer-wrapper">
						<div class="msg-editer">
							<form id="appmsg-form" class="form">
								<input type="hidden" name="folder" value="rank"/>
								<input type="hidden" name="id" value="${banner.id}"/>
								<input type="hidden" name="imageUrl" id="imageUrl" value="${banner.imageUrl}"/>
								
								<div class="control-group">
									<label class="control-label">封面状态：</label>
									<span class="maroon">*</span>
									<div class="controls">
										<select id="status" name="status">
											<option value="1">上架</option>
											<option value="2">下架</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">封面序号：</label>
									<span class="maroon">*</span><span class="help-inline">(大值靠前)</span>
									<div class="controls">
										<input name="sortNum" type="number" value="${banner.sortNum}" class="span5" style="width:205px;"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">封面图片</label>
									<span class="maroon">*</span><span class="help-inline">(必须上传一张图片)</span>
									<div class="controls">
										<input type="file" style="width:73px;" id="file" name="myimage" onchange="uploadCovers('file');"/>
										<div id="loading" style="display:none;position:fixed;bottom:100px;">
		                                    <img src="<%=path%>/images/load.gif"/>
		                                </div>
									</div>		
								</div>
								<div class="control-group" style="margin-top:20px;">
									<div class="controls">
										<button id="save-btn" type="submit" class="btn-mine">保 存</button>
										<button id="cancel-btn" type="button" class="btn">取 消</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
	
	<jsp:include page="/common/footer.jsp" flush="true" />
</body>
</html>