<%@ page pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>fzhwx开发者-公众号管理平台</title>
<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
</head>

<body>
	<jsp:include page="/common/header.jsp" flush="true" />
	<jsp:include page="/common/leftbar.jsp" flush="true" />
	<div class="content">
		<div class="container-fluid">
			<form name="form" action="${path}/sportWear/goodsList">
				
				<fieldset>
					<legend>BANNER列表</legend>
				</fieldset>
				
				<div class="control-group">
					<div style="margin-top:-10px;">
						<button class="btn" type="button" onclick="editUI('');">新 增</button>
					</div>
				</div>
				
				<div align="center">
					<table class="table table-bordered table-hover table-condensed">
						<thead align="center">
							<tr>
								<th width="1%" style="text-align:center">
									<a href="javascript:void(0);">编  号</a>
								</th>
								<th width="5%" style="text-align:center">
									<a href="javascript:void(0);">图片链接</a>
								</th>
								<th width="3%" style="text-align:center">
									<a href="javascript:void(0);">图片状态</a>
								</th>
								<th width="3%" style="text-align:center">
									<a href="javascript:void(0);">创建时间</a>
								</th>
								<th width="5%" style="text-align:center">
									<a href="javascript:void(0);">更新时间</a>
								</th>
							</tr>
						</thead>
						<tr>
							<c:if test="${fn:length(list) == 0}">
								<td colspan="5" style="text-align: center">未查询到相关记录！</td>
							</c:if>
						</tr>
						<c:forEach items="${list}" var="entry" varStatus="idx">
							<tr>
								<td style="text-align:center;cursor:pointer;">
									<a href="javascript:editUI('${entry.id}');">${idx.index + 1}</a>
								</td>
								<td style="text-align:center">
									<img width="40" height="40" src="${entry.imageUrl}"/>
								</td>
								<td style="text-align:center">
									${entry.status == 1 ? '上架' : '下架'}
								</td>
								<td style="text-align:center">
									${entry.createTime}
								</td>
								<td style="text-align:center">
									${entry.updateTime}
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</form>
		</div>
	</div>
	<jsp:include page="/common/footer.jsp" flush="true" />
</body>
<script>
function editUI(id) {
	if (id !== '') {
		location.href = '${path}/sportWear/banner/info?parent=sportWear/banner&id=' + id;
	} else {
		location.href = '${path}/sportWear/banner/info?parent=sportWear/banner';
	}
}
</script>
</html>
