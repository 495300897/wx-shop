<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>管理平台</title>
<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${path}/css/theme.css">
<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css">
<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
</head>

<body>
	<jsp:include page="/common/header.jsp" flush="true"/>
	<jsp:include page="/common/leftbar.jsp" flush="true"/>
	<div class="content">
		<div class="container-fluid">
			<form name="form" action="${path}/wxpubno/list">
				<fieldset>
					<legend>微信账号列表</legend>
				</fieldset>
				<div class="control-group">
					<div style="margin-top:-10px;">
						<button class="btn" type="button" onclick="editUI('');">新 增</button>
					</div>
				</div>
				<div align="center">
					<table class="table table-bordered table-hover table-condensed">
						<thead align="center">
							<tr>
								<th width="1%" style="text-align:center">
									<a href="javascript:void(0);">编 号</a>
								</th>
								<th width="2%" style="text-align:center">
									<a href="javascript:void(0);">账号名称</a>
								</th>
								<th width="2%" style="text-align:center">
									<a href="javascript:void(0);">账号类型</a>
								</th>
								<th width="2%" style="text-align:center">
									<a href="javascript:void(0);">APPID</a>
								</th>
								<th width="2%" style="text-align:center">
									<a href="javascript:void(0);">创建时间</a>
								</th>
								<th width="2%" style="text-align:center">
									<a href="javascript:void(0);">操 作</a>
								</th>
							</tr>
						</thead>
						<c:if test="${fn:length(list) == 0}">
							<tr>
								<td colspan="6" style="text-align:center">未查询到相关记录！</td>
							</tr>
						</c:if>
						<c:forEach items="${list}" var="entry" varStatus="idx">
							<tr>
								<td style="text-align:center;">${idx.index + 1}</td>
								<td style="text-align:center">${entry.pubnoName}</td>
								<td style="text-align:center">
									${entry.pubnoType == 'pubno' ? '公众号' : '小程序'}
								</td>
								<td style="text-align:center">${entry.appid}</td>
								<td style="text-align:center">${entry.createTime}</td>
								<td style="text-align:center;">
									<a href="javascript:editUI('${entry.id}');">编 辑</a>
								</td>
							</tr>
						</c:forEach>
					</table>
					<jsp:include page="/common/page.jsp" flush="true"/>
				</div>
			</form>
		</div>
	</div>
	<jsp:include page="/common/footer.jsp" flush="true"/>
</body>
<script type="text/javascript">
function editUI(id) {
	if (id != '') {
		location.href = '${path}/wxpubno/editUI?id=' + id;
	} else {
		location.href = '${path}/wxpubno/editUI';
	}
}
</script>
</html>
