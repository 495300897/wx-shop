<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/global.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>管理平台</title>
	<link rel="shortcut icon" href="${path}/images/wx/wally.jpg"/>
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="${path}/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="${path}/css/theme.css"/>
	<link rel="stylesheet" href="${path}/lib/font-awesome/css/font-awesome.css"/>
	<script type="text/javascript" src="${path}/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="${path}/js/bootstrap.js"></script>
	<link rel="stylesheet" href="${path}/css/appmsg.css"/>
	<style type="text/css">
		label { display: inline-block; }
		.help-inline { vertical-align: top; }
	</style>
</head>
<body>
	<jsp:include page="/common/header.jsp" flush="true"/>
	<jsp:include page="/common/leftbar.jsp" flush="true"/>
	<div class="content">
		<div class="container-fluid">
			<fieldset>
				<legend id="actTitle">新增平台账号</legend>
			</fieldset>
			<div style="padding-top:20px;">
				<div class="span6">
					<div class="msg-editer-wrapper">
						<div class="msg-editer">
							<form id="appmsg-form" class="form" action="${path}/wxpubno/saveOrUpdate" method="post">
								<input type="hidden" name="id" value="${wxpubno.id}"/>
								<div class="control-group">
									<label class="control-label">账号类型</label>
									<span class="maroon">*</span>
									<select class="span2" id="pubnoType" name="pubnoType">
										<option value="pubno">公众号</option>
										<option value="minapp">小程序</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">账号名称</label>
									<span class="maroon">*</span><span class="help-inline">(必填，不能超过20个字符)</span>
									<div class="controls">
										<input class="span5" type="text" required="required" name="pubnoName" maxlength="20" value="${wxpubno.pubnoName}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">账号原始ID</label>
									<span class="help-inline">(不能超过20个字符)</span>
									<div class="controls">
										<input class="span5" type="text" name="pubno" maxlength="20" value="${wxpubno.pubno}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">APPID</label>
									<span class="maroon">*</span><span class="help-inline">(必填，不能超过30个字符)</span>
									<div class="controls">
										<input class="span5" type="text" required="required" name="appid" maxlength="30" value="${wxpubno.appid}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">APPSECRET</label>
									<span class="maroon">*</span><span class="help-inline">(必填，不能超过32个字符)</span>
									<div class="controls">
										<input class="span5" type="text" required="required" name="appsecret" maxlength="32" value="${wxpubno.appsecret}"/>
									</div>
								</div>
								<div class="control-group pubno">
									<label class="control-label">TOKEN</label>
									<span class="maroon">*</span><span class="help-inline">(必填，不能超过20个字符)</span>
									<div class="controls">
										<input class="span5" type="text" name="token" maxlength="20" value="${wxpubno.token}"/>
									</div>
								</div>
								<div class="control-group pubno">
									<label class="control-label">URL</label>
									<div class="controls">
										<input class="span5" type="text" name="interfaceUrl" value="${wxpubno.interfaceUrl}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">MCHID</label>
									<span class="help-inline">(不能超过20个字符)</span>
									<div class="controls">
										<input class="span5" type="text" name="mchId" maxlength="20" value="${wxpubno.mchId}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">MCHKEY</label>
									<span class="help-inline">(不能超过32个字符)</span>
									<div class="controls">
										<input class="span5" type="text" name="mchKey" maxlength="32" value="${wxpubno.mchKey}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">SPBILLIP</label>
									<span class="help-inline">(不能超过20个字符)</span>
									<div class="controls">
										<input class="span5" type="text" name="spbillIp" maxlength="20" value="${wxpubno.spbillIp}"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">NOTIFYURL</label>
									<span class="maroon">*</span><span class="help-inline">(必填，不能超过200个字符)</span>
									<div class="controls">
										<input class="span5" type="text" name="notifyUrl" maxlength="200" value="${wxpubno.notifyUrl}"/>
									</div>
								</div>
								<div class="control-group" align="center">
									<div class="controls">
										<button id="save-btn" type="submit" class="btn-mine">保 存</button>&nbsp;&nbsp;
										<button id="cancel-btn" type="button" class="btn">取 消</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/common/footer.jsp" flush="true"/>
</body>
<script type="text/javascript">
$(function() {
	if ($('input[name=id]').val() != '') {
		$('#actTitle').html('修改平台账号');
	}
	var val = "${wxpubno.pubnoType}";
	$("#pubnoType").change(function() {
		val = $("#pubnoType").val();
		if (val == 'pubno') {
			$('.pubno').show();
		} else {
			$('.pubno').hide();
		}
	});
	// 设置选中值
	$("#pubnoType").val(val).attr("selected", true);
	if (val == 'pubno' || val == '') {
		$('.pubno').show();
	} else {
		$('.pubno').hide();
	}
	// 保存
	//$('#save-btn').click(function() {});
	$('#cancel-btn').click(function() {
		history.back();
	});
});
</script>
</html>