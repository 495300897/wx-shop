<%@page import="java.util.Arrays"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@ page import="com.alibaba.fastjson.JSONObject"%>
<%@ page import="java.io.File"%>
<%@ page import="java.util.Properties"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="ueditor.Uploader"%>
<%@ page import="java.io.FileInputStream"%>

<%
    request.setCharacterEncoding(Uploader.ENCODEING);
    response.setCharacterEncoding(Uploader.ENCODEING);
    
    /* String currentPath = request.getRequestURI().replace(request.getContextPath(), "");

    File currentFile = new File(currentPath);
    currentPath = currentFile.getParent() + File.separator;

    //加载配置文件
    String propertiesPath = request.getSession().getServletContext().getRealPath(currentPath + "config.properties");
    Properties properties = new Properties();
    try {
        properties.load(new FileInputStream(propertiesPath));
    } catch ( Exception e ) {
        //加载失败的处理
        e.printStackTrace();
    } */
    
 	// 获取存储目录，并设置目录
 	if (request.getParameter("fetch") != null) {
 		response.setHeader("Content-Type", "text/javascript");
 		
 		System.out.println("fetch = " + request.getParameter("fetch"));
 		
 		// 设置目录
 		response.getWriter().print("updateSavePath(['upload']);");
 		return;
 	}
	
 	String[] fileType = { ".gif", ".png", ".jpg", ".jpeg", ".bmp" };
 	// 上传图片
 	Uploader up = new Uploader(request);
 	// 前端下拉选择目录
 	up.setSavePath("upload");
 	up.setAllowFiles(fileType);
 	up.setMaxSize(500 * 1024); //单位KB
 	up.upload();
    
    JSONObject json = new JSONObject();
    json.put("url", up.getUrl());
    json.put("fileType", up.getType());
    json.put("state", up.getState());
    json.put("original", up.getOriginalName());
    
    System.out.println("imageUp.jsp 上传成功: " + json);
    
    response.getWriter().print(json.toString());
%>
