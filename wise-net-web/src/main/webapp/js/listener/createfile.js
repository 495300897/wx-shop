function createFile(fileName) {
	var deferred = when.defer();
	
	var success=function(fs) {
		fs.root.getFile(fileName, {create: true}, function(fileEntry) {
			fileEntry.createWriter(function(fileWriter) {
				var ret = {'fileEntry':fileEntry,'fileWriter' : fileWriter};		
				deferred.resolve(ret);
			});
		});
	};
	var error=function(e)
	{
		alert("webkitRequestFileSystem error:"+e);
	};
    //On Temporary Storage, you can use up to 10% of remaining disk space per domain.
    //If you exceed quota, the browser starts clearing out your storage, starting with the oldest domain.
    //On Persistent Storage, you can request arbitrary size if the user permits. 
    window.webkitRequestFileSystem(window.TEMPORARY, 200 * 1024 * 1024, success,error);
	return deferred.promise;
};