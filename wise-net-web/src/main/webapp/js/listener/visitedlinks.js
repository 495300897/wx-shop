if (window.localStorage)
{
    window.onload = function ()
    {
        //iterate all links on this page,
        //and change the color of the visited links,
        //because "a:visited" does not work when browser is restarted,
        //we didn't use "a:visited"
        var links = document.links;
        for (var i = 0; i < links.length; i++)
        {
            var link = links[i];
            var href = link.href;          
            //use md5 of href instead of href,
            //because length of md5 is shorter,
            //so it takes up less localStorage spaces
            if (localStorage.getItem("v-" + md5(href)))
            {
                link.style.color = "cadetblue";
            }
        }
    }
    var curUrl = window.location.href;
    try
    {
        localStorage.setItem("v-" + md5(curUrl), 'v');//mark current url as visited
    }
    catch (err)//if exceeded quote,A QUOTA_EXCEEDED_ERR exception is thrown.
    {
        //Delete the first 100 records
        var keysToBeRemoved = new Array();
        for (var i = 0; i < 100; i++)
        {
            var key = localStorage.key(i);
            if (key&&key.indexOf("v-") == 0)
            {
                keysToBeRemoved.push(key);
            }
        }
        for (var i = 0; i < keysToBeRemoved.length; i++)
        {
            var key = keysToBeRemoved[i];
            localStorage.removeItem(key);
        }
    }
}