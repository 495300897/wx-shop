function Downloader(url, onProgressCallback)
{
	var deferred = when.defer();
    var xhr = new XMLHttpRequest();    
    xhr.open('GET', url, true);
    //if url is from other domains,
    //Cors settings of that domain is required
    //the settings is as following
    //Access-Control-Allow-Origin *.youzack.com;
    //Access - Control - Allow - Methods GET;
    //Access - Control - Expose - Headers Content - Type, Content - Length;
    xhr.onreadystatechange = function ()
    {
        if (this.readyState == 4) {
			if (this.response != null) {
                var blob = new Blob([new Uint8Array(this.response)]);
                var ret = {'blob': blob };
				deferred.resolve(ret);
			}
		}
	};
	xhr.onprogress = function(e) {
		if(onProgressCallback)
		{
			onProgressCallback(e);
		}		
	};
	xhr.responseType = 'arraybuffer'; 
	xhr.send();
	return deferred.promise;
};