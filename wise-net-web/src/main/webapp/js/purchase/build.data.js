
function buildData(list) {
	var html = '';
	setUserInfo(list);
	$.each(list, function(idx) {
		var photoHtml = '';
		if (this.photo != null && this.photo.length > 0) {
			photoHtml = `<input type="hidden" name="photo_list" class="photo_list" value='${this.photo}'/>`;
			var picArr = JSON.parse(this.photo);
			for (var i = 0; i < picArr.length; i++) {
				photoHtml += `<a href="javascript:void(0);" onclick="showPicList($(this),${i});">
							      <img src="${picArr[i]}"/>
								  ${picArr.length > 3 && i == 2 ? '<span class="more-pic">更多<br>图片</span>': ''}
							  </a>`;
				if (picArr.length > 3 && i == 2) {
					break;
				}
			}
		}
		html += `<div class="tcline-item noAllowCopy">
					<div class="avatar-label">
						<a href="#">
							<img src="${this.headurl}" class="avatar"/>
						</a>
					</div>
					<div class="tcline-detail" data-id="7081">
						<div class="tcline-detail__hd dislay-flex">
							<div class="detail-hd__lt">
								<a href="#" class="typename tc-template__bg">
									${this.firstCategory || this.secondCategory}
								</a>
								&nbsp;
								<a href="#" class="username">
									${this.nickName || this.liaison}
								</a>
							</div>
							<div class="detail-hd__rt">
								<a href="javascript:toPage('purchase/goods_info&id=${this.id}');" class="ext-act">
								<i class="tciconfont tcicon-yanjing"></i>详情
								</a>
							</div>
						</div>
						<article style="max-height:none;">
							<a href="javascript:toPage('purchase/goods_info&id=${this.id}');">
								<div class="detail-attr" style="max-height:63px;overflow:hidden;">
									<p style="display:${this.sourceId == 55 ? 'none' : ''}">
										<font class="tc-template__color" color="#f60">产品分类 : </font>
										${this.secondCategory}
									</p>
									<p style="display:${this.sourceId == 55 ? 'none' : ''}">
										<font class="tc-template__color" color="#f60">公司名称 : </font>
										${this.company}
									</p>
									<p>
										<font class="tc-template__color" color="#f60">所属地区 : </font>
										${this.area}
									</p>
								</div>
								<p class="detail-content detail-content__line2">${this.description}</p>
							</a>
						</article>
						<div class="detail-toggle">全文</div>
						<div class="detail-toggle2" style="display:none;">收起</div>
						<div class="detail-pics clearfix">
							${photoHtml}
						</div>
						<div class="detail-dingwei">
							<i class="tciconfont tcicon-dingwei_shi"></i>
							${this.area}
						</div>
						<div class="detail-time">
							<span>${this.visitCount}次浏览</span>
						</div>
					</div>
				 </div>`;
	});
	$('#index-list').html(html);
}

function companyData(list) {
	var html = '';
	$.each(list, function(idx) {
		html += `<div class="item-box clearfix">
				    <div class="item-pic">
					    <a href="javascript:toPage('purchase/company_info&id=${this.id}');">
					        <img src="${this.logo}">
					    </a>
				 	</div>
					<div class="item-content">
					    <div class="content">
					        <h5>
					            <a href="javascript:toPage('purchase/company_info&id=${this.id}');">
					                ${this.name}&nbsp;
					                <span class="icon vip" style="background:url(https://www.tobo1688.com/data/attachment/tomwx/202006/09/145109s9dsk70yvssph5l9.png) no-repeat; background-size:90%;"></span>
					            </a>
					        </h5>
					        <a href="javascript:toPage('purchase/company_info&id=${this.id}');">
					            <p class="xinxi" style="color:#dea44e;">
					                <i class="tciconfont tcicon-business-hours" style="margin-right:3px;"></i>
					                ${this.openTime}
					            </p>
					        </a>
					        <a href="#">
					            <p class="xinxi shop_list-tags">
					                <span class="span0">${this.category}</span>
					            </p>
					        </a>
					        <p class="address">
					            ${this.address}
					        </p>
					        <p class="nr">
					            <span class="zan"></span>${this.description}
					        </p>
					    </div>
					    <div class="details">
					        <div class="tel">
					            <a href="tel:${this.tel}"></a>
					        </div>
					        <div class="dist">5138浏览</div>
					    </div>
				    </div>
			     </div>`;
	});
	$('#index-list').html(html);
}

function companyList() {
	var html = '';
	$.ajax({
        url: path + '/purchase/company/list',
        type: 'GET',
        data: { page: 1 },
        dataType: 'JSON',
        success: function (data) {
        	if (data.success) {
        		$.each(data.list, function(idx) {
        			html += `<a class="swiper-slide index_shop_list_item" href="javascript:toPage('purchase/company_info&id=${this.id}');">
								<div class="index_shop_list_item_img tc-template__color">
									<img style="border-radius:5px;" src="${this.logo}"/>
								</div>
								<div class="index_shop_list_item_txt">${this.name}</div>
							 </a> `;
        		});
        		$('.swiper-container-shoplist .swiper-wrapper').html(html);
        	} else {
        		tusi(data.message);
        	}
        }
    });
}

// 上传显示图片
function showPhoto(photo) {
	var photoHtml = '';
	if (photo != null && photo.length > 0) {
		photoHtml = `<input type="hidden" name="photo_list" class="photo_list" value='${photo}'/>`;
		var picArr = JSON.parse(photo);
		for (var i = 0; i < picArr.length; i++) {
			photoHtml += `<img src="${picArr[i]}" class="picture" onclick="showPicList($(this),${i});"/>`;
		}
		$('.mation-photo').html(photoHtml);
	}
}

var userArr = [
	{"nickName":"东海鼠", headurl:"https://wx.qlogo.cn/mmhead/qk2IaLRaphRv1TgSVZ7JruibxBCYJOuA12CWKshXwCxw/132"},
	{"nickName":"晴天", headurl:"https://wx.qlogo.cn/mmhead/AV7HfW2bsiauQBVOicrlfrXdWlegDsGNtAqvMnEic3CPia4/132"},
	{"nickName":"雅洁哥哥", headurl:"https://wx.qlogo.cn/mmhead/ThUa79IWhic7Ag3GNLBGuTEia7pWnlicOhBzft5bxZBhXE/132"},
	{"nickName":"军", headurl:"https://wx.qlogo.cn/mmhead/ndiboYMXoszJ762tXW8ac8rsEM6KVM2r3AqmtWvGmjiaY/132"},
	{"nickName":"知非", headurl:"https://wx.qlogo.cn/mmhead/ykn76iaG5WXBgmChLicT9u9hl4IuaGtfb5zS6iaOiaCTA2U/132"},
	{"nickName":"清漪", headurl:"https://wx.qlogo.cn/mmhead/r6UlBX94vTTia1j3RTe3MSDusb6NVrFiaCOo5guRGcLT0/132"},
	{"nickName":"输赢一场梦", headurl:"https://wx.qlogo.cn/mmhead/VqyHpPVvGia2pzp5iby7cFyvTx3phbYXZo9A3jONxb1gg/132"},
	{"nickName":"洋洋得意", headurl:"https://wx.qlogo.cn/mmhead/RILlOftibdwBduOLzWibjmfrT2flQM9g9pibMqrqoeic4s4/132"},
	{"nickName":"林呆呆呀", headurl:"https://wx.qlogo.cn/mmhead/a4gKxuDCUEwOXbTDAZyjCUSAB0AW3dB9Odh7NfOib3AQ/132"},
	{"nickName":"A土豆", headurl:"https://wx.qlogo.cn/mmhead/Ichib5hYyuKHqIJhbleK5N66aNUSFdgKxicyJgwVW1pz0/132"},
	{"nickName":"林平琴", headurl:"https://wx.qlogo.cn/mmhead/teCC2ju9Vyiar9jjnib03t74IvHicicaicHmRj53IrlY4rO4/132"},
	{"nickName":"静怡", headurl:"https://wx.qlogo.cn/mmhead/0TicUpQ4jCxeFicUUbY1BgCKGZYibbdR0VCoZjHvCBB20w/132"},
	{"nickName":"泰云", headurl:"https://wx.qlogo.cn/mmhead/8pXUcZ8PuooVfnCTBYcfE525SGlvM9icMdUicu3kiaND5E/132"},
	{"nickName":"念", headurl:"https://wx.qlogo.cn/mmhead/hlWiaH7hoNuklTibXkACmMyHsBH7vXPcnxZV2R0z3A2Q0/132"},
	{"nickName":"孟涵", headurl:"https://wx.qlogo.cn/mmhead/dD2fJIFctKFxjJVwHNWMNVr3B1j7iccrSPzpRluUuMtg/132"},
];

function setUserInfo(list) {
	$.each(list, function(idx) {
		if (this.openId == 'oTv570nP2GkqZpEZ2bCPo2Ngg3oI' || this.openId == 'oVivD5o1k9dkjcOWyyWP1WK97aBA') {
			var user = userArr[idx];
			list[idx].headurl = user.headurl;
			list[idx].nickName = user.nickName;
		}
	});
}
