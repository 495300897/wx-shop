$(function() {
	// 显示封面图片
	if ($('#imageUrl').val() !== '') {
		$('#coverTip').removeClass('default-tip');
	}
	
	$("#appmsg-form").validate({
		rules: {
			color: {
				required: true
			},
			price: {
				required: true
			},
			origPrice: {
				required: true
			},
			colorUrl: {
				required: true
			}
		},
		messages: {
			color: {
				required: "必须填写颜色型号"
			},
			price: {
				required: "必须填写出售价格"
			},
			origPrice: {
				required: "必须填写商品原价"
			},
			colorUrl: {
				required: "必须上传封面图片"
			}
		},
		showErrors: function(errorMap, errorList) {
			console.log('errorMap', errorMap);
			if (errorList && errorList.length > 0) {
				$.each(errorList, function(index, obj) {
					const item = $(obj.element);
					item.closest(".control-group").addClass("error");
					item.attr("title", obj.message);
				});
			} else {
				const item = $(this.currentElements);
				item.removeAttr("title");
				item.closest(".control-group").removeClass("error");
			}
		},
		submitHandler: function() {
			const imageUrl = $('#imageUrl').val();
			if (imageUrl === '' || imageUrl.indexOf('http') === -1) {
				alert("必须上传一个封面图片。");
				return false;
			}

			const $btn = $("#save-btn");
			if ($btn.hasClass("disabled")) {
				return false;
			}

			const formData = getFormData("appmsg-form");
			//console.log('formData', formData);
			
			$btn.addClass("disabled");
			$.post(path + "/sportWear/info/save",
				formData,
				function(data) {
					$btn.removeClass("disabled");
					if (data.success) {
						alert("保存成功");
						location.href = path + "/sportWear/infoList?parent=sportWear/goodsList&wearId=" + formData.wearId;
					} else {
						alert("保存失败");
					}
				},
			"JSON");
		}
	});
});

function uploadCovers(id) {
	uploadImage(id, function(res) {
		console.log('uploadCovers', res);
		if (res.success) {
			$('.default-tip').hide();
			$('#imageUrl').val(res.url);
        	$('#coverTip').removeClass('default-tip');
        	$(".cover .i-img").attr("src", $('#imageUrl').val()).show();
        	alert('上传成功！');
        	return;
		}
		alert(res.message);
	});
}