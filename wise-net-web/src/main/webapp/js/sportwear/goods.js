$(function() {
	// 显示封面图片
	if ($('#imageUrl').val() !== '') {
		$('#coverTip').removeClass('default-tip');
	}
	
	$("#appmsg-form").validate({
		rules: {
			title: {
				required: true,
				maxlength: 100
			},
			infoText: {
				required: true,
				maxlength: 1000
			},
			price: {
				required: true
			},
			origPrice: {
				required: true
			},
			sizes: {
				required: true
			},
			coverUrls: {
				required: true
			},
			infoUrls: {
				required: true
			}
		},
		messages: {
			title: {
				required: "必须填写商品标题",
				maxlength: "商品标题不能超过100个字"
			},
			infoText: {
				required: "必须填写商品描述",
				maxlength: "商品描述不能超过1000个字"
			},
			price: {
				required: "必须填写出售价格"
			},
			origPrice: {
				required: "必须填写商品原价"
			},
			sizes: {
				required: "必须填写商品尺寸"
			},
			coverUrls: {
				required: "必须上传封面图片"
			},
			infoUrls: {
				required: "必须上传详情图片"
			}
		},
		showErrors: function(errorMap, errorList) {
			console.log('errorMap', errorMap);
			if (errorList && errorList.length > 0) {
				$.each(errorList, function(index, obj) {
					const item = $(obj.element);
					item.closest(".control-group").addClass("error");
					item.attr("title", obj.message);
				});
			} else {
				const item = $(this.currentElements);
				item.removeAttr("title");
				item.closest(".control-group").removeClass("error");
			}
		},
		submitHandler: function() {
			const covers = [], infos = [];
			const infoUrls = $("textarea[name=infoUrls]").val();
			const coverUrls = $("textarea[name=coverUrls]").val();

			$.each(coverUrls.split(','), function(index, url) {
				if (url.indexOf('http') > -1) covers.push(url);
			});
			if (covers.length === 0) {
				alert("必须上传一个封面图片。");
				return false;
			}
			$.each(infoUrls.split(','), function(index, url) {
				if (url.indexOf('http') > -1) infos.push(url);
			});
			if (infos.length === 0) {
				alert("必须上传一个详情图片。");
				return false;
			}
			$("textarea[name=infoUrls]").val(infos.join(','));
			$("textarea[name=coverUrls]").val(covers.join(','));

			const $btn = $("#save-btn");
			if ($btn.hasClass("disabled")) {
				return false;
			}

			const formData = getFormData("appmsg-form");
			//console.log('formData', formData);
			
			$btn.addClass("disabled");
			$.post(path + "/sportWear/goods/save",
				formData,
				function(data) {
					$btn.removeClass("disabled");
					if (data.success) {
						alert("保存成功");
						location.href = path + "/sportWear/goodsList";
					} else {
						alert("保存失败");
					}
				},
			"JSON");
		}
	});
});

function uploadCovers(id) {
	const coverUrls = $("textarea[name=coverUrls]").val();
	uploadImage(id, function(res) {
		console.log('uploadCovers', res);
		if (res.success) {
			$('#coverTip').removeClass('default-tip');
			$(".cover .i-img").attr("src", res.url).show();
			$("textarea[name=coverUrls]").val(coverUrls + ',' + res.url);
		}
	});
}

function uploadInfoUrls(id) {
	const infoUrls = $("textarea[name=infoUrls]").val();
	uploadImage(id, function(res) {
		console.log('uploadInfoUrls', res);
		if (res.success) {
			$("textarea[name=infoUrls]").val(infoUrls + ',' + res.url);
		}
	});
}
