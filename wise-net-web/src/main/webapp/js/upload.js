// 上传云图片
function uploadImage(fileId, callback) {
    if (!checkImage(fileId)) return;
    $('#loading').show();
    $.ajax({
        url: path + '/file/ossUpload',
        type: 'POST',
        cache: false,
        data: new FormData($('#appmsg-form')[0]),
        timeout: 10000,
        // 必须false才会避开jQuery对 formdata 的默认处理 
        // XMLHttpRequest会对 formdata 进行正确的处理
        processData: false,
        //必须false才会自动加上正确的Content-Type 
        contentType: false,
        xhrFields: { withCredentials: true },
        success: function(data) {
            return typeof callback == 'function' && callback(data);
        },
        error: function(XMLHttpRequest, status, error) {
            console.log('error', error);
            return typeof callback == 'function' && callback(false);
        },
        complete: function(res) {
        	$('#loading').hide();
        	$("#" + fileId).val('');
            console.log('complete', res);
        }
    });
}

function checkImage(fileId) {
    // 获取上传的图片名 带//
    const animateimg = $('#' + fileId).val();
    // 分割
    const imgarr = animateimg.split('\\');
    // 去掉 // 获取图片名
    const myimg = imgarr[imgarr.length - 1];
    // 获取 . 出现的位置
    const houzui = myimg.lastIndexOf('.');
    // 切割 . 获取文件后缀
    const ext = myimg.substring(houzui, myimg.length).toUpperCase();
    // 获取上传的文件
    const file = $('#' + fileId).get(0).files[0];
    // 获取上传的文件大小
    const fileSize = file.size;
    // 最大6MB
    const maxSize = 6291456;
    console.log('fileId = ' + fileId + ', fileSize = ' + fileSize);
    if (ext !== '.PNG' && ext !== '.JPG' && ext !== '.JPEG' && ext !== '.BMP') {
        alert('文件类型错误，请上传图片类型！');
        return false;
    }
    if (parseInt(fileSize) >= parseInt(maxSize)) {
        alert('上传的文件不能超过6MB！');
        return false;
    }
    return true;
}