$(function() {
	//文件上传
	$("#file_upload").omFileUpload({
		action : window.allBasePath + "file/ossUpload",
		fileExt :"*.jpg;*.png;*.gif;*.jpeg;*.bmp",
		fileDesc :"Image Files",
		fileDataName :"myFile",
		sizeLimit :200 * 1024,
		onError : function(ID, fileObj, errorObj, event) {
			if (errorObj.type === "File Size") {
				alert("上传图片的大小不能超过200KB！");
			}
		},
		autoUpload :true,
		method :"POST",
		actionData : {
			uid :$("#uid").val()
		},
		onComplete : function(ID, fileObj, response, data, event) {
			const jsonData = JSON.parse(response);
			console.log(jsonData)
			if (jsonData.error === "fileType") {
				alert("上传图片的格式只支持：jpg、jpeg、png、bmp！");
				return;
			}
			$(".cover .i-img").attr("src", window.allBasePath + jsonData.fileUrl).show();
			$("#imgArea").show().find("#img").attr("src", window.allBasePath + jsonData.fileUrl);
			$("input[name='imageUrl']").val(jsonData.fileName);
			$(".default-tip").hide();
		}
	});

	$(".msg-editer #title").bind("keyup", function() {
		$(".i-title").text($(this).val());
	});

	$(".msg-editer #digest").bind("keyup", function() {
		$(".msg-text").html($.htmlEncode($(this).val()));
	});

	$("#desc-block-link").click(function() {
		$("#desc-block").show();
		$(this).hide();
	});

	$("#url-block-link").click(function() {
		$("#url-block").show();
		$(this).hide();
	});

	$("#chain-block-link").click(function() {
		$("#chain-block").show();
		$(this).hide();
	});

	$("#delImg").click( function() {
		$(".default-tip").show();
		$("#imgArea").hide();
		$("#imageUrl").val("");
		$(".cover .i-img").hide();
	});

	$("#cancel-btn").click(function(event) {
		event.stopPropagation();
		history.back();
	});
	
	$("#imageUrl").blur(function() {
		showImage();
	});

	$("#appmsg-form").validate({
		rules : {
			title : {
				required : true,
				maxlength : 64
			},
			digest : {
				required : true,
				maxlength : 120
			},
			imageUrl : {
				required : true
			},
			sourceUrl : {
				required : true
			},
			clickOutUrl : {
				required : true
			}
		},
		messages : {
			title : {
				required :"请输入标题",
				maxlength :"标题不能超过64个字"
			},
			imageUrl : {
				required :"请填写封面URL"
			},
			digest : {
				maxlength :"摘要不能超过120个字"
			},
			sourceUrl : {
				required :"必须输入正确的url格式"
			},
			clickOutUrl : {
				required :"必须输入正确的url格式"
			}
		},

		showErrors : function(errorMap, errorList) {
			console.log('errorMap', errorMap);
			if (errorList && errorList.length > 0) {
				$.each(errorList, function(index, obj) {
					const item = $(obj.element);
					item.closest(".control-group").addClass("error");
					item.attr("title", obj.message);
				});
			} else {
				const item = $(this.currentElements);
				item.closest(".control-group").removeClass("error");
				item.removeAttr("title");
			}
		},

		submitHandler : function() {
			const editorContent = msg_editor.getContent();
			const clickOutUrl = $("#clickOutUrl").val();
			const len = editorContent.length;
			if (clickOutUrl === "" && len === 0) {
				alert("正文内容或者外链至少要填写一个。");
				msg_editor.focus();
				return false;
			}

			if (len > charLimit) {
				alert("正文的内容不能超过" + charLimit + "个字");
				msg_editor.focus();
				return false;
			}

			const $btn = $("#save-btn");
			const $form = $("#appmsg-form");
			if ($btn.hasClass("disabled")) {
				return false;
			}

			const submitData = {
				title: $("input[name='title']", $form).val(),
				digest: $("textarea[name='digest']", $form).val(),
				imageUrl: $("#imageUrl", $form).val(),
				sourceUrl: $("input[name='sourceUrl']", $form).val(),
				clickOutUrl: $("#clickOutUrl", $form).val(),
				imageTextNo: $("#imageTextNo", $form).val(),
				mainText: editorContent,
				flag: $("#flag", $form).val() // 标识新增或修改
			};

			$btn.addClass("disabled");

			$.post(
				window.allBasePath + "imagetextinfo/saveOrUpdate",
				submitData,
				function(data) {
					$btn.removeClass("disabled");
					if (data != null && data === true) {
						location.href = window.allBasePath + "imagetextinfo/list";
					} else {
						alert("保存失败");
					}
				}, "json");
			return false;
		}
	});

	window.msg_editor = new UE.ui.Editor({
		initialFrameWidth : 530
	});
	window.msg_editor.render("editor");

	function computeChar() {
		const len = msg_editor.getContent().length;
		if (len > charLimit) {
			$(".editor-bottom-bar").html(
					"<span style='color:red;'>你输入的字符个数（" + len
							+ "）已经超出最大允许值！</span>");
		} else {
			$(".editor-bottom-bar").html(
					"当前已输入<span class='char_count'>" + len
							+ "</span>个字符, 您还可以输入<span class='char_remain'>"
							+ (charLimit - len) + "</span>个字符。");
		}
	}

	window.msg_editor.addListener("keyup", function(type, evt) {
		computeChar();
	});

});

// 上传云图片
function uploadImage() {
    if (!checkImage()) return;
    $('#loading').show();
    $.ajax({
        url: window.allBasePath + 'file/ossUpload',
        type: 'POST',
        cache: false,
        data: new FormData($('#appmsg-form')[0]),
        timeout: 30000,
        // 必须false才会避开jQuery对 formdata 的默认处理 
        // XMLHttpRequest会对 formdata 进行正确的处理
        processData: false,
        //必须false才会自动加上正确的Content-Type 
        contentType: false,
        xhrFields: { withCredentials: true },
        success: function(data) {
            $('#loading').hide();
            if (data.success && data.url != null) {
            	$('#imageUrl').val(data.url);
            	$('#coverTip').removeClass('default-tip');
            	// 显示图片
            	showImage();
            	alert('上传成功！');
            	return;
            }
            alert('上传失败！');
        },
        error: function(XMLHttpRequest, status, error) {
            console.log('error', error);
        },
        complete: function(res) {
        	$("#file").val('');
        	$('#loading').hide();
            console.log('complete', res);
        }
    });
}

function checkImage() {
    // 获取上传的图片名 带//
	const animateimg = $("#file").val();
	// 分割
	const imgarr = animateimg.split('\\');
	// 去掉 // 获取图片名
	const myimg = imgarr[imgarr.length - 1];
	// 获取 . 出现的位置
	const houzui = myimg.lastIndexOf('.');
	// 切割 . 获取文件后缀
	const ext = myimg.substring(houzui, myimg.length).toUpperCase();
	// 获取上传的文件
	const file = $('#file').get(0).files[0];
	if (ext !== '.PNG' && ext !== '.JPG' && ext !== '.JPEG' && ext !== '.BMP') {
        alert('文件类型错误，请上传图片类型！');
        return false;
    }
    if (parseInt(file.size) >= 6291456) {
        alert('上传的文件不能超过6MB！');
        return false;
    }
    return true;
}

function showImage() {
	$('.default-tip').hide();
	$(".cover .i-img").attr("src", $('#imageUrl').val()).show();
}