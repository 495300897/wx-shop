var path = '', openid = null, appid = 'wx16fd1fc8b0018190';
var liIdx = 0, submitPayStatus = 0, photoArr = [];
var shareLink = `https://mmnews.net.cn/photoFrame/wx/open/redirect?page=purchase/index&app=${appid}&scope=snsapi_userinfo`;

$(function() {
	// 获取项目路径
	path = getProjectPath();
	console.log('getProjectPath -> path', path);
	
	// 真实环境开启userAgent验证
	//if (path != '') { userAgentWx(); }
	
	openid = getQuery('openid');
	
	/*config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，
	    所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，
	    则可以直接调用，不需要放在ready函数中。
	*/
	wx.ready(function(res) {
		console.log('ready', res);
		// 分享给朋友
		wx.updateAppMessageShareData({
	        title: '竹影-采购平台', // 分享标题
	        desc: '全国采购平台', // 分享描述
	        link: shareLink, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
	        imgUrl: 'https://mmnews.net.cn/photoFrame/images/purchase/142803i9a91o855p5eg851.png', // 分享图标
	        success: function () {
	          // 设置成功
	        }
	    });
		// 分享到朋友圈
		wx.updateTimelineShareData({
			title: '全国采购平台-竹影', // 分享标题
	        link: shareLink, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
	        imgUrl: 'https://mmnews.net.cn/photoFrame/images/purchase/142803i9a91o855p5eg851.png', // 分享图标
	        success: function () {
	          // 设置成功
	        }
	    });
	});
	/*config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，
	    也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
	*/
	wx.error(function(res) {
		console.log('error', res);
	});
});

// 获取jssdk配置
function getConfig(appid, cb) {
	var url = location.href.split('#')[0];
	$.ajax({
        url: path + '/wx/open/getConfig',
        type: 'GET',
        data: { url: url, app: appid },
        dataType: 'JSON',
        success: function (data) {
        	if (!data.success) {
        		alert(data.message);
        		return typeof cb == 'function' && cb(false);
        	}
        	wx.config({
        	    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        	    appId: data.result.appId, // 必填，公众号的唯一标识
        	    timestamp: data.result.timestamp, // 必填，生成签名的时间戳
        	    nonceStr: data.result.nonceStr,   // 必填，生成签名的随机串
        	    signature: data.result.signature, // 必填，签名
        	    jsApiList: [
        	    	'updateAppMessageShareData',
                    'updateTimelineShareData',
                    'previewImage',
                    'uploadImage',
                    'chooseImage',
                    'getLocation',
                    'openLocation',
                    'openAddress',
                    'scanQRCode'
        	    ], // 必填，需要使用的JS接口列表
        	    openTagList: ['wx-open-launch-app'] // 可选，需要使用的开放标签列表
        	});
        	return typeof cb == 'function' && cb(true);
        }
    });
}

// 获取url参数
function getQuery(name) {
	var reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`, 'i');
	var r = window.location.search.substr(1).match(reg);
	return r != null ? unescape(decodeURIComponent(r[2])) : null;
}

// 获取项目路径
function getProjectPath() {
	var curWwwPath = window.document.location.href;
	// 本地测试环境
	if (curWwwPath.indexOf('localhost') > 0 || curWwwPath.indexOf('127.0.0.1') > 0) {
		return '';
	}
	// 获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
	var pathName = window.document.location.pathname;
	var pos = curWwwPath.indexOf(pathName);
	// 获取主机地址，如： http://localhost:8080
	var localhostPaht = curWwwPath.substring(0, pos);
	// 获取带'/'的项目名，如：/uimcardprj
	return localhostPaht + pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
}

// 微信客户端验证
function userAgentWx() {
	var useragent = navigator.userAgent;
	if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
		// 这里警告框会阻塞当前页面继续加载
		alert('请在微信客户端访问本页面');
		// 以下代码是用javascript强行关闭当前页面
		var opened = window.open('about:blank', '_self');
		opened.opener = null;
		opened.close();
	}
}

function queryCategory(type) {
	var html = '';
	$.ajax({
        type: 'GET',
        url: path + '/purchase/goodscategory/list',
        data: {type: type},
        success: function(res) {
            if (!res.success) { return; }
        	
        	$.each(res.list, function(idx) {
        		var html_ = '<option value="' + this.id + '">' + this.name + '</option>';
        		html += html_;
        	});
        	$('#categoryId').append(html);
        }
    });
}

function getUserInfo(cb) {
	$.ajax({
        type: 'GET',
        url: path + '/purchase/user/get',
        data: {openid: openid},
        success: function(data) {
        	if (!data.success) {
        		alert(data.message);
        		return typeof cb == 'function' && cb(false);
        	}
        	return typeof cb == 'function' && cb(data.result);
        }
    });
}

function showPhoto(liIdx, picUrl) {
	var photoHtml = `<li class="li_${liIdx}">
			            <section class="img"><img src="${picUrl}"/></section>
			            <div class="close pic-delete-btn pointer" onclick="picRemove('${liIdx}', '${picUrl}');">&nbsp;X&nbsp;</div>
			         </li>`;
	$('#photoList').append(photoHtml);
}

function picRemove(i, picUrl) {
    $('.li_' + i).remove();
    if (photoArr.length > 0) {
    	var pos = $.inArray(picUrl, photoArr);
    	photoArr.splice(pos, 1);
    	$('#photo').val(JSON.stringify(photoArr));
    }
}

$("input[name=wxSamePhone]").click(function() {
    var phone = $('#phone').val();
    if ($('input[name=wxSamePhone]').attr('checked')) {
        $('#wechat').val(phone);
    } else {
        $('#wechat').val('');
    }
});

function checkMobile(phone) {
    var regu = /^[1][0-9]{10}$/;
	var re = new RegExp(regu);
	return re.test(phone) ? true : false;
}

function toPage(page) {
	if (page != '' && page != 'undefined') {
		location.href = path + `/wx/open/toPage?page=${page}&openid=${openid}`;
	}
}

// 预支付
function prepay(totalFee, type, recordId, cb) {
	$.ajax({
        url: path + '/wxpay/unifiedOrder/' + appid,
        type: 'POST',
        data: {
        	openId: openid, totalFee: totalFee * 100, type: type, recordId: recordId
        },
        dataType: 'JSON',
        success: function(data) {
        	if (data.success) {
        		var result = data.result;
        		console.log('result', result);
        		wx.chooseWXPay({
        			timestamp: result.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
        			nonceStr: result.nonceStr, // 支付签名随机串，不长于 32 位
        			package: result.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=\*\*\*）
        			signType: result.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
        			paySign: result.paySign, // 支付签名
        			success: function (res) {
        				tusi('支付成功');
        				return typeof cb == "function" && cb(result);
        			},
        			complete: function(res) {
        				console.log('res', res);
        				// 取消支付
        				if (res.errMsg == 'chooseWXPay:cancel') {
        					// 取消支付
        					cancelPay();
        					return typeof cb == 'function' && cb(false);
        				}
        			}
        		});
        	} else {
        		tusi(data.message);
        		return typeof cb == 'function' && cb(false);
        	}
        },
        complete: function() {
        	$('#loading').hide();
        }
	});
}

// 取消支付
function cancelPay() {
	$.ajax({
        url: path + '/wxpay/cancelpay',
        type: 'POST',
        data: { openid: openid },
        dataType: 'JSON',
        success: function(data) {
        	console.log('data', data);
        }
	});
}

// 上传图片
function upload() {
	if (photoArr.length > 6) {
		tusi('最多上传6张图片');
		return;
	}
	wx.chooseImage({
        count: 1, // 默认9
        sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
            var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
            wx.uploadImage({
                localId: localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
                isShowProgressTips: 1, // 默认为1，显示进度提示
                success: function (res) {
                    var serverId = res.serverId; // 返回图片的服务器端ID
                    // 把微信服务器图片上传到自己服务器
                    $.get(path + '/wx/open/savePicture', { mediaId: serverId, app: appid },
                    	function(data) {
                    		if (!data.success) {
                    			tusi(data.message);
                    			return;
                    		}
                    			
                        	if (data.success && data.picUrl != null) {
                        		showPhoto(liIdx, data.picUrl);
                        		photoArr.push(data.picUrl);
                        		$('#photo').val(JSON.stringify(photoArr));
                        		liIdx++;
                        	}
                    });
                }
            });
        }
    });
}

function saveGoods() {
	$.ajax({
        type: 'POST',
        url: path + '/purchase/goods/save',
        dataType: 'JSON',
        data: $('#payForm').serialize(),
        success: function(data) {
            loading(false);
            tusi('发布成功');
            toPage('purchase/index');
        }
    });
}

function qqMap(categoryId) {
	var page = categoryId == 0 ? 'company_joinin' : 'supply';
	var backurl = path + `/wx/open/toPage?page=purchase/${page}%26openid=${openid}%26categoryId=${categoryId}`;
	var url = `https://apis.map.qq.com/tools/locpicker?search=1&type=0&policy=1&backurl=${backurl}&key=NUHBZ-GXXWW-D2HR6-OC63T-SXVSJ-T7FUP&referer=weixins`;
	location.href = url;
}

function getTime2Show() {
	//var futureDay = new Date('2021-01-08 12:37:00').getTime();
    //console.log(futureDay);
    var TS = new Date().getTime() - 1610080620000;
    return TS > 0 ? false : true;
}

function iOS() {
	var u = navigator.userAgent;  
	return !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
}

window.alert = function(name) {
    var iframe = document.createElement('IFRAME');
    iframe.style.display = 'none';
    iframe.setAttribute('src', 'data:text/plain,');
    document.documentElement.appendChild(iframe);
    window.frames[0].window.alert(name);
    iframe.parentNode.removeChild(iframe);
};

window.confirm = function (message) {
    var iframe = document.createElement('IFRAME');
    iframe.style.display = 'none';
    iframe.setAttribute('src', 'data:text/plain,');
    document.documentElement.appendChild(iframe);
    var alertFrame = window.frames[0];
    var result = alertFrame.window.confirm(message);
    iframe.parentNode.removeChild(iframe);
    return result;
};

/**
 * 回调函数
 * @param 'id=' + id，也可以直接在url拼接
 */
var callbackAjax = function (param, url, method, okCallback, failCallback) {
    $.ajax({
        type: method,
        url: url,
        dataType: 'JSON',
        data: param,
        error: function (error) {
            console.log('error', error);
            alert(JSON.stringify(error));
        },
        success: function (data) {
        	if (data.success) {
        		okCallback(data);
            } else {
                if (failCallback) {
                    failCallback(data);
                } else {
                	alert(data.message);
                }
            }
        }
    });
};

var retry = function (callback) {
    var openId = sessionStorage.getItem('openId');
    if (openId && openId.length != 0) {
        'function' === typeof callback && callback();
    } else {
        setTimeout(function () {
            retry(callback);
        }, 500);
    }
};
