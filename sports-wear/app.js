//app.js
App({
  onLaunch: function (options) {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || [];
    logs.unshift(Date.now());
    wx.setStorageSync('logs', logs);
  },
  
  onShow: function (options) {
    console.log('options', options);
  },
  
  globalData: {
    extraData: null
  }
})