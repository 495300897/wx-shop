var util = require('../../utils/util.js');
Page({
  data: {
    title: '',
    list: []
  },

  onLoad: function(e) {
    var title = e.title;
    this.setData({ title: title });
    this.searchWearList();
  },

  searchinput: function (e) {
    var title = e.detail.value;
    this.setData({ title: title });
  },

  detail: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({ url: '/pages/details/details?id=' + id });
  },

  searchWearList: function () {
    wx.showLoading({ title: '数据加载中...' });
    wx.request({
      url: util.requestUrl.searchList,
      data: { "title": this.data.title },
      method: 'GET',
      header: { 'content-type': 'application/json' },
      success: res => {
        var result = res.data;
        console.log(result)
        if (result.success) {
          var list = util.buildData(result.list);
          this.setData({ list: list });
        } else {
          wx.showModal({ content: '数据加载失败，请重试！', showCancel: false });
        }
      },
      complete: function (e) {
        wx.hideLoading();
      }
    });
  },

});