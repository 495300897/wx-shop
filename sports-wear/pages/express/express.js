var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: null,
    postid: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var postid = options.postid, type = options.type;
    this.setData({ postid: postid });
    wx.showLoading({ title: '数据加载中...' });
    wx.request({
      url: util.requestUrl.queryExpress,
      method: 'GET',
      data: { postid: postid, type: type },
      header: { 'content-type': 'application/json' },
      success: res => {
        //console.log('res', res.data);
        if (res.data.success) {
          var express = JSON.parse(res.data.express);
          if (express.message == 'ok') {
            this.setData({ list: express.data });
          } else {
            wx.showModal({ content: express.message, showCancel: false });
          }
        }
      },
      fail: function (res) {
        console.log('fail', res);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    });
  },

})