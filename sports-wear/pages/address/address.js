var util = require('../../utils/util.js');
Page({
  data: {
    area: ''
  },

  saveAddress: function (e) {
    var formData = e.detail.value;
    console.log(formData)
    var phone = formData.phone;
    var name = formData.name;
    var address = formData.address;
    var reg = /^1(3|4|5|6|7|8)\d{9}$/;
    var flag = reg.test(phone);
    if (name == '') {
      wx.showModal({ content: '请输入收货人姓名！', showCancel: false, });
      return;
    }
    if (!flag) {
      wx.showModal({ content: '请输入正确的手机号！', showCancel: false, });
      return;
    }
    if (address == '') {
      wx.showModal({ content: '请输入收货人地址！', showCancel: false, });
      return;
    }
    wx.request({
      url: util.requestUrl.addressSave,
      method: 'POST',
      data: {
        "uid": "o_uEm0XlBxSFNxpKBK0v_og1dj0Y",
        "phone": phone,
        "name": name,
        "address": address,
        "flag": 1
      },
      header: { 'content-type': 'application/json' },
      success: function (res) {
        console.log(res);
      }
    })
  },
  
})
