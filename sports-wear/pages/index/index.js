var util = require('../../utils/util.js');
Page({
  data: {
    title: '',
    banner: [],
    football: null
  },

  searchinput: function (e) {
    var title = e.detail.value;
    this.setData({ title: title });
  },

  searchWear: function (e) {
    var title = e.detail.value;
    if (title == '') {
      wx.showModal({ content: '请输入搜索关键字！' });
      return;
    }
    wx.navigateTo({ url: '/pages/search/search?title=' + title });
  },

  onLoad: function () {
    this.getWearList();
    this.getBannerList();
    // 获取用户缓存信息
    var user = wx.getStorageSync('user');
    // 用户登录
    if (user == '') {
      this.userLogn();
    }
  },

  getWearList: function () {
    wx.showLoading({ title: '数据加载中...' });
    wx.request({
      url: util.requestUrl.wearList,
      data: { "wearType": "足球系列" },
      method: 'GET',
      header: { 'content-type': 'application/json' },
      success: res => {
        var result = res.data;
        //console.log(result)
        if (result.success) {
          var list = util.buildData(result.list);
          this.setData({ football: list });
        } else {
          wx.showModal({ content: '数据加载失败，请重试！', showCancel: false });
        }
      },
      complete: function (e) {
        wx.hideLoading();
      }
    });
  },

  getBannerList: function () {
    wx.request({
      url: util.requestUrl.bannerList,
      data: { status: 1 },
      method: 'GET',
      header: { 'content-type': 'application/json' },
      success: res => {
        var banner = res.data;
        console.log(banner)
        this.setData({ banner: banner });
      },
      complete: function (e) {
        wx.hideLoading();
      }
    });
  },

  detail: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({ url: '/pages/details/details?id=' + id });
  },

  // 登录
  userLogn: function () {
    wx.login({
      success: function (res) {
        if (res.code.length) {
          wx.request({
            url: util.requestUrl.login,
            data: { code: res.code, appid: 'wx0b236a02a79e42f7' },
            method: 'GET',
            header: { 'content-type': 'application/json' },
            success: function (res) {
              var result = res.data;
              if (result.success) {
                wx.setStorageSync('user', result.user);
              } else {
                wx.showModal({ content: '数据加载失败，请重试！', showCancel: false });
              }
            }
          });
        }
      }
    });
  },

  /**
 * 用户点击右上角分享
 */
  onShareAppMessage: function (res) {
    // 来自页面内转发按钮 
    //if (res.from === 'button') { console.log(res.target) }
    return {
      title: '运动服专营',
      path: '/pages/index/index',
      imageUrl: '',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }

  }

})
