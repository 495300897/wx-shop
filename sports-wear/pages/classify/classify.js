var util = require('../../utils/util.js');
Page({
    data: {
        content: '这是分类页面',
        navLeftItems: [],
        navRightItems: [],
        isActive: false,
        curNav: 1,
		    curIndex: 0
    },

    onLoad: function() {
      console.log('getCategories....')
      var that = this;
      util.loginGetToken(function (res) {
        if (typeof res == 'object') {
          that.getCategories(res);
          console.log('getCategories', res);
        } else {
          console.log('error', res)
        }
      });
    },

    getCategories: function (loginInfo) {
      wx.request({
        url: util.requestUrl.categories,
        method: 'GET',
        header: { 'content-type': 'application/json', 'authorization': loginInfo.token },
        success: function (res) {
          console.log('categories', res.data);
        }
      });
    },

})