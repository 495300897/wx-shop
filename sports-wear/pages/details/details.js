var util = require('../../utils/util.js');
Page({
  data: {
    wear: null,
    infoList: [],
    buyNum: 1,
    totalPrice: 0,
    isBuy: false,
    showText: false,
    sizes: [],
    infoImg: [],
    coverImg: [],
    size: '',
    color: '',
    sizeImg: '',
    wearId: 0,
    info_text: ''
  },

  onLoad: function(options) {
    var id = options.id;
    this.getWearInfo(id);
  },

  preBuy: function() {
    this.setData({ isBuy: true, sizeImg: this.data.infoList[0].colorUrl });
  },

  closePanel: function() {
    this.setData({ isBuy: false });
  },

  selectColor: function(e) {
    var dataset = e.currentTarget.dataset;
    var wear = this.data.wear;
    wear.price = dataset.price;
    var totalPrice = wear.price * this.data.buyNum;
    this.setData({
      wear: wear,
      wearId: dataset.id,
      color: dataset.color,
      sizeImg: dataset.logo,
      totalPrice: totalPrice.toFixed(2)
    });
  },

  selectSize: function(e) {
    this.setData({ size: e.currentTarget.dataset.size });
  },

  showSize: function() {
    var list = this.data.wear.sizes.split(','), _list = [];
    var length = list.length;
    for (var i = 0; i < length; i++) {
      if ((i + 1) % 3 == 0) {
        _list.push([list[i - 2], list[i - 1], list[i]]);
      }
    }
    var len = length % 3;
    if (len == 1) {
      _list.push([list[length - 1]]);
    }
    if (len == 2) {
      _list.push([list[length - 2], list[length - 1]]);
    }
    this.setData({ sizes: _list });
  },

  showCoverImage: function() {
    this.setData({ coverImg: this.data.wear.coverUrls.split(',') });
  },

  showImage: function() {
    this.setData({ infoImg: this.data.wear.infoUrls.split(',') });
  },

  previewImage: function(e) {
    var idx = e.currentTarget.dataset.idx;
    var flag = e.currentTarget.dataset.flag;
    var urls = flag == 'info' ? this.data.infoImg : this.data.coverImg;
    wx.previewImage({ current: urls[idx], urls: urls });
  },

  showText: function() {
    this.setData({ showText: !this.data.showText });
  },

  isBuy: function() {
    this.setData({ isBuy: true });
  },

  confirmBuy: function() {
    var that = this;
    var color = that.data.color, size = that.data.size;
    if (color == '' || size == '') {
      wx.showModal({ content: '请选择颜色和尺码！', showCancel: false });
      return;
    }
    var buyNum = that.data.buyNum;
    if (isNaN(buyNum) || buyNum < 1) return;

    that.setData({ isBuy: false });

    // 页面传参
    var param = {};
    param['size'] = size;
    param['color'] = color;
    param['count'] = buyNum;
    param['wearId'] = that.data.wearId;
    param['imageUrl'] = that.data.sizeImg;
    param['title'] = that.data.wear.title;
    param['price'] = that.data.wear.price;
    wx.navigateTo({ url: '/pages/pay/pay?data=' + JSON.stringify(param) });
  },

  inputNum: function(e) {
    var num = e.detail.value;
    var price = this.data.wear.price;
    // 数量不能小于1
    if (isNaN(num) || num <= 0) {
      this.setData({ buyNum: 1 });
    } else {
      this.setData({ buyNum: num });
    }
    var totalPrice = price * num;
    this.setData({ totalPrice: totalPrice.toFixed(2) });
  },

  /**
   * 购买数量加减操作
   */
  buyNum: function(e) {
    var type = e.currentTarget.dataset.type;
    var price = this.data.wear.price;
    var buyNum = parseInt(this.data.buyNum);
    if (isNaN(buyNum) || (type == -1 && buyNum == 1)) {
      this.setData({ buyNum: 1 });
      return;
    }
    buyNum = (type == -1 && buyNum > 1) ? (buyNum - 1) : (buyNum + 1);
    var totalPrice = price * buyNum;
    this.setData({
      buyNum: buyNum,
      totalPrice: totalPrice.toFixed(2)
    });
  },

  getWearInfo: function(id) {
    var that = this;
    wx.showLoading({ title: '数据加载中...' });
    wx.request({
      url: util.requestUrl.wearInfo,
      data: { "id": id },
      method: 'GET',
      header: { 'content-type': 'application/json' },
      success: function(res) {
        var result = res.data;
        if (result.success) {
          that.setData({ wear: result.wear, infoList: result.infoList });
          // 显示尺码
          that.showSize();
          // 显示详情图片
          that.showImage();
          // 显示封面图片
          that.showCoverImage();
        } else {
          wx.showModal({ content: '数据加载失败，请重试！', showCancel: false });
        }
      },
      complete: function(e) {
        wx.hideLoading();
      }
    })
  },


})