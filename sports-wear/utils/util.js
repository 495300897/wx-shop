var host = 'http://localhost/';
var requestUrl = {
  login: host + 'sport/user/login',
  wearList: host + 'sport/wearList',
  bannerList: host + 'sportWear/banner/list',
  searchList: host + 'sport/searchList',
  wearInfo: host + 'sport/wearInfo',
  updateUser: host + 'sport/user/update',
  toPay: host + 'sport/order/toPay',
  wxpayUrl: host + 'sport/order/wxpay',
  checkOrderUrl: host + 'ocrWxpay/checkOrder',
  payAgain: host + 'sport/order/payAgain',
  orderCancle: host + 'sport/order/cancel',
  cancelPayUrl: host + 'ocrWxpay/cancelpay',
  orderList: host + 'sport/order/list',
  addressList: host + 'sport/addr/list',
  addressSave: host + 'sport/addr/save',
  queryExpress: host + 'orderManage/queryExpress',
}

function buildData (list) {
  var _list = [];
  for (var i = 0; i < list.length; i++) {
    var obj = list[i];
    // 将字符串转为数组
    obj.imageUrl = obj.coverUrls.split(',')[0];
    if (obj.price.indexOf('-') > -1) {
      obj.price = obj.price.split('-')[0];
    }
    _list.push(obj);
  }
  return _list;
}

function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

module.exports = {
  buildData: buildData,
  formatTime: formatTime,
  requestUrl: requestUrl
}
